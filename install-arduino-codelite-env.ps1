#!/usr/bin/env pwsh
###  #!"C:\Program Files\PowerShell\7\pwsh.exe" -ExecutionPolicy Bypass
#--------------------------------------------------------------
#    Desktop Shortcut Example
#    W:\ProgramFiles\PowerShell\7\pwsh.exe -ExecutionPolicy Bypass -File W:\DinRDuino\PwrShell\arduino_ide.ps1 W DinRDuino 1.8.15 Eicon BluePill F103C8T6 SerLed V1.0.1 stm32 swd
#
#    Launching the Arduino IDE in a fully configured way.
#    This Pwershell Script uses forward declarations as follows.
#
#    #---------------------------------------------------------
#    # Main Function
#    #---------------------------------------------------------
#    function main
#    {
#        function_1        # Calling function_1
#        function_2        # Calling function_2
#    }
#    #---------------------------------------------------------
#    # Function 1
#    #---------------------------------------------------------
#    function function_1
#    {
#        do something
#    }
#    #---------------------------------------------------------
#    # Function 2
#    #---------------------------------------------------------
#    function function_1
#    {
#        do something else
#    }
#    #---------------------------------------------------------
#    # Main      The script entry point
#    #---------------------------------------------------------
#    main                      # Call function_main
#--------------------------------------------------------------

#---------------------------------------------------------
# Global Parameters
#--------------------------------------------------------

#param([string]$PROJ_NAME       = "PROJ_NAME",
#      [string]$BRK_LINE_NO     = "BRK_LINE_NO");
if ($IsLinux)
{
    $OS_UC                = "LIN"
    $OS_LC                = "lin"
    $OS_C                 = "Lin"
    $DRIVE                = "/home/eicon"
    $SL                   = "/"
    $ARDUINO_TB_TYPE      = "linux64.tar.xz"
    $OPENOCD_TB_TYPE      = "linux-x64.tar.gz"
    $ARDUINO_CLI_TB_TYPE  = "Linux_64bit.tar.gz"
}
elseif ($IsWindows)
{
    $OS_UC                = "WIN"
    $OS_LC                = "win"
    $OS_C                 = "Win"
    $DRIVE                = "C:"
    $SL                   = "\"
    $ARDUINO_TB_TYPE      = "windows.zip"
    $OPENOCD_TB_TYPE      = "win32-x64.zip" 
    $ARDUINO_CLI_TB_TYPE  = "Windows_64bit.zip"
 }


#$OS_UC                         = "LIN"
#$OS_LC                         = "lin"
#$OS_C                          = "Lin"
#$OS_LC                         = ${OS}.ToLower()
#$OS_C                          = $OS_UC.SubString(0, 1)+$OS_LC.SubString(1, 2)
#$DRIVE                         = "/home/eicon"
$PROJECT_NAME_ST               = "F411CEU6-BlackPill-HelloW"
$PROJECT_NAME_GD               = "F303ZIT6-GDDuino-HelloW"
$BREAK_LINE_NO                 = "42"
$INSTALL_FOLDER                = "J-Tagit"
$WORKSPACE_FOLDER              = "CodeLite"
#$PROJECT_NAME_ST              = "${PROJ_NAME}"
$CPU_NAME                      = $PROJECT_NAME_ST.SubString(0, 8)
$PROJECT_STM32_RELEASE         = "2.2.0"
$PROJECT_GD32_RELEASE          = "1.0.0"
$CORE_STM32_NAME               = "Core_STM32"
$CORE_GD32_NAME                = "Core_GD32"
$PLATFORM_STM32_NAME           = "stm32"
$PLATFORM_GD32_NAME            = "gd32"
$ARDUINO_VERSION               = "1.8.18"
$ARDUINO_LIBRARIES_RELEASE     = "1.0.1"
$ARDUINO_CLI_RELEASE           = "0.20.2"
$BOARD_VENDOR_JT               = "J-Tagit"
$BOARD_VENDOR_JT_LC            = "j-tagit"
$BOARD_VENDOR_ST               = "STMicroelectronics"
$SKETCHBOOK_RELEASE            = "0.1.0"
$OPENOCD_RELEASE               = "0.11.0-1"
$OPENOCD_CFG_RELEASE           = "0.1.0"
$ARM_GCC_VERSION               = "10.1.0"
$TEENSY_GCC_VERSION            = "5.4.1"
$CMSIS_VERSION                 = "5.7.0"
$TEMPL_PROJ_NAME               = "F103CBT6-BluePill-HelloW"
$PROBE                         = "jtagit-2"
$CPU_TARGET                    = "stm32f1x.cfg"
$BUILD_SYS                     = "cmake"
$PROTOCOL                      = "swd"
$OPENOCD_STM32_CFG_FILENAME    = "${PROJECT_NAME_ST}-${BUILD_SYS}-${PROTOCOL}.cfg"
$OPENOCD_GD32_CFG_FILENAME     = "${PROJECT_NAME_JD}-${BUILD_SYS}-${PROTOCOL}.cfg"

$PROJECT_STM32_URI             = "http://192.168.0.14/noeldiviney/${PROJECT_NAME_ST}/-/archive/${PROJECT_STM32_RELEASE}/${PROJECT_NAME_ST}-${PROJECT_STM32_RELEASE}.zip"
$PROJECT_GD32_URI              = "http://192.168.0.14/noeldiviney/${PROJECT_NAME_GD}/-/archive/${PROJECT_GD32_RELEASE}/${PROJECT_NAME_GD}-${PROJECT_GD32_RELEASE}.zip"
$VARIANTS_STM32_URI            = "http://192.168.0.14/noeldiviney/${BOARD_VENDOR_JT_LC}-${PLATFORM_STM32_NAME}-variants-${OS_LC}/-/archive/${PROJECT_STM32_RELEASE}/${BOARD_VENDOR_JT_LC}-${PLATFORM_STM32_NAME}-variants-${OS_LC}-${PROJECT_STM32_RELEASE}.zip"
$VARIANTS_GD32_URI             = "http://192.168.0.14/noeldiviney/${BOARD_VENDOR_JT_LC}-${PLATFORM_GD32_NAME}-variants-${OS_LC}/-/archive/${PROJECT_GD32_RELEASE}/${BOARD_VENDOR_JT_LC}-${PLATFORM_GD32_NAME}-variants-${OS_LC}-${PROJECT_GD32_RELEASE}.zip"
$CORE_STM32_ZIP_URI            = "https://github.com/stm32duino/Arduino_${CORE_STM32_NAME}/archive/refs/tags/${PROJECT_STM32_RELEASE}.zip"
$CORE_GD32_ZIP_URI             = "http://192.168.0.14/noeldiviney/${BOARD_VENDOR_JT_LC}-${PLATFORM_GD32_NAME}-core-${OS_LC}/-/archive/${PROJECT_GD32_RELEASE}/${BOARD_VENDOR_JT_LC}-${PLATFORM_GD32_NAME}-core-${OS_LC}-${PROJECT_GD32_RELEASE}.zip"
##                                http://192.168.0.14/noeldiviney/F303ZIT6-GDDuino-HelloW/-/archive/1.0.0/F303ZIT6-GDDuino-HelloW-1.0.0.zip
##                                http://192.168.0.14/noeldiviney/j-tagit-gd32-core-lin/-/archive/1.0.0/j-tagit-gd32-core-lin-1.0.0.zip
##                                https://gitlab.com/noeldiviney/F103CBT6_BluePill_HelloW_Lin/-/archive/2.2.0/F103CBT6_BluePill_HelloW_Lin-2.2.0.zip
##                                https://github.com/stm32duino/Arduino_Core_STM32/archive/refs/tags/2.2.0.zip
$PROJECT_STM32_ZIP_NAME        = "${PROJECT_NAME_ST}-${PROJECT_STM32_RELEASE}.zip"
$PROJECT_STM32_UNZIPPED_NAME   = "${PROJECT_NAME_ST}-${PROJECT_STM32_RELEASE}"
$PROJECT_GD32_ZIP_NAME         = "${PROJECT_NAME_GD}-${PROJECT_GD32_RELEASE}.zip"
$PROJECT_GD32_UNZIPPED_NAME    = "${PROJECT_NAME_GD}-${PROJECT_GD32_RELEASE}"
$ARDUINO_CORE_STM32_ZIP_NAME   = "Arduino_${CORE_STM32_NAME}-${PROJECT_STM32_RELEASE}.zip"
$ARDUINO_CORE_STM32_NAME       = "Arduino_${CORE_STM32_NAME}-${PROJECT_STM32_RELEASE}"
$ARDUINO_CORE_GD32_ZIP_NAME    = "${BOARD_VENDOR_JT_LC}-${PLATFORM_GD32_NAME}-core-${OS_LC}-${PROJECT_GD32_RELEASE}.zip"
$ARDUINO_CORE_GD32_NAME        = "${BOARD_VENDOR_JT_LC}-${PLATFORM_GD32_NAME}-core-${OS_LC}-${PROJECT_GD32_RELEASE}"
$VARIANTS_STM32_ZIP_NAME       = "${BOARD_VENDOR_JT_LC}-${PLATFORM_STM32_NAME}-variants-${OS_LC}-${PROJECT_STM32_RELEASE}.zip"
$VARIANTS_STM32_NAME           = "${BOARD_VENDOR_JT_LC}-${PLATFORM_STM32_NAME}-variants-${OS_LC}-${PROJECT_STM32_RELEASE}"
$VARIANTS_GD32_ZIP_NAME        = "${BOARD_VENDOR_JT_LC}-${PLATFORM_GD32_NAME}-variants-${OS_LC}-${PROJECT_GD32_RELEASE}.zip"
$VARIANTS_GD32_NAME            = "${BOARD_VENDOR_JT_LC}-${PLATFORM_GD32_NAME}-variants-${OS_LC}-${PROJECT_GD32_RELEASE}"


$OPENOCD_TB_TYPE               = "win32-x64.zip"
$ARDUINO_LIBRARIES_PREFIX      = "${BOARD_VENDOR_JT_LC}-win-arduino-libraries"
$SKETCHBOOK_NAME_PREFIX        = "${BOARD_VENDOR_JT_LC}-${OS_LC}-sketchbook"
$XPACK_GCC_NAME                = "xpack-arm-none-eabi-gcc"
$EICON_GCC_NAME                = "${BOARD_VENDOR_JT_LC}-arm-none-eabi-gcc"
$ARDUINO_CLI_TB_TYPE           = "Windows_64bit.zip"
$ARDUINO_CLI_TB_NAME           = "arduino-cli_${ARDUINO_CLI_RELEASE}_${ARDUINO_CLI_TB_TYPE}"
$ARDUINO_TB_TYPE               = "windows.zip"
$OPENOCD_CFG_SCRIPT_NAME       = "${BOARD_VENDOR_JT_LC}-${OS_LC}-openocd-cfg"

$BASE_PATH                     = "${DRIVE}/"
$INSTALL_FOLDER_PATH           = "${BASE_PATH}${INSTALL_FOLDER}"
$CORE_STM32_PATH               = "${INSTALL_FOLDER_PATH}/${CORE_STM32_NAME}"
$CORE_GD32_PATH                = "${INSTALL_FOLDER_PATH}/${CORE_GD32_NAME}"
$DLOAD_PATH                    = "${INSTALL_FOLDER_PATH}/dload" 
$WORKSPACE_PATH                = "${INSTALL_FOLDER_PATH}/${WORKSPACE_FOLDER}"
$BUILD_STM32_PATH              = "${WORKSPACE_PATH}/${PROJECT_NAME_ST}/build"
$BUILD_GD32_PATH               = "${WORKSPACE_PATH}/${PROJECT_NAME_GD}/build"
$APP_PATH                      = "${WORKSPACE_PATH}/${PROJECT_NAME_ST}/App"
$ARDUINO_PATH                  = "${INSTALL_FOLDER_PATH}/arduino-${ARDUINO_VERSION}"
$PORTABLE_PATH                 = "${ARDUINO_PATH}/portable"
$HARDWARE_PATH_JT              = "${PORTABLE_PATH}/packages/${BOARD_VENDOR_JT}/hardware"
$HARDWARE_PATH_ST              = "${PORTABLE_PATH}/packages/${BOARD_VENDOR_ST}/hardware"
$TOOLS_PATH_JT                 = "${PORTABLE_PATH}/packages/${BOARD_VENDOR_JT}/tools"
$TOOLS_PATH_ST                 = "${PORTABLE_PATH}/packages/${BOARD_VENDOR_ST}/tools"
$OPENOCD_PATH                  = "${INSTALL_FOLDER_PATH}/openocd"
$OPENOCD_CFG_PATH              = "${OPENOCD_PATH}/scripts/board/${BOARD_VENDOR_JT}/${OS_LC}/j-tagit"
$PREFS_PATH                    = "${PORTABLE_PATH}"
$SKETCH_PATH                   = "${PORTABLE_PATH}/sketchbook/arduino"
##  https://gitlab.com/noeldiviney/eicon_lin_stm32_variants/-/archive/2.2.0/eicon_lin_stm32_variants-2.2.0.zip
##  https://gitlab.com/noeldiviney/eicon_lin_stm32_variants/-/archive/2.2.0/eicon_lin_stm32_variants-2.2.0.zip
$ARDUINO_CLI_URI               = "https://downloads.arduino.cc/arduino-cli/arduino-cli_${ARDUINO_CLI_RELEASE}_${ARDUINO_CLI_TB_TYPE}"
$OPENOCD_CFG_SCRIPT_URI        = "https://gitlab.com/noeldiviney/${OPENOCD_CFG_SCRIPT_NAME}/-/archive/${OPENOCD_CFG_RELEASE}/${OPENOCD_CFG_SCRIPT_NAME}-${OPENOCD_CFG_RELEASE}.tar.gz"
$ARDUINO_LIBRARIES_ZIP_URI     = "https://gitlab.com/noeldiviney/${BOARD_VENDOR_JT_LC}_${OS_LC}_arduino_libraries/-/archive/${ARDUINO_LIBRARIES_RELEASE}/${ARDUINO_LIBRARIES_PREFIX}-${ARDUINO_LIBRARIES_RELEASE}.zip"
$ARDUINO_ZIP_URI               = "https://downloads.arduino.cc/arduino-${ARDUINO_VERSION}-${ARDUINO_TB_TYPE}"
$SKETCHBOOK_ZIP_URI            = "https://gitlab.com/noeldiviney/${BOARD_VENDOR_JT_LC}_${OS_LC}_sketchbook/-/archive/${SKETCHBOOK_RELEASE}/${BOARD_VENDOR_JT_LC}_${OS_LC}_sketchbook-${SKETCHBOOK_RELEASE}.zip"
$OPENOCD_TB_NAME               = "xpack-openocd-${OPENOCD_RELEASE}-${OPENOCD_TB_TYPE}"
$OPENOCD_TB_URI                = "https://github.com/xpack-dev-tools/openocd-xpack/releases/download/v${OPENOCD_RELEASE}/xpack-openocd-${OPENOCD_RELEASE}-${OPENOCD_TB_TYPE}"
$GD32_INDEX                    = "        http://192.168.0.14/noeldiviney/j-tagit-packages/-/raw/master/package_j-tagit_gd32_index.json,"
$STM32_INDEX                   = "        http://192.168.0.14/noeldiviney/j-tagit-packages/-/raw/master/package_j-tagit_stm32_index.json,"
$KINETIS_INDEX                 = "        http://192.168.0.14/noeldiviney/j-tagit-packages/-/raw/master/package_j-tagit_kinetis_index.json,"
$STMicroelectronics_INDEX      = "        https://github.com/stm32duino/BoardManagerFiles/raw/main/package_stmicroelectronics_index.json"

$ARDUINO_ZIP_FILE              = "arduino-${ARDUINO_VERSION}.zip"
$ARDUINO_LIBRARIES_ZIP_FILE    = "${BOARD_VENDOR_JT_LC}_${OS_LC}_arduino_libraries-${ARDUINO_LIBRARIES_RELEASE}.zip"
$SKETCH_NAME                   = "${BOARD_VENDOR}-${BOARD}-${CPU}-${SKETCH}-${SKETCH_VER}"
$USER                          = "$env:USER"                                 # $env:VAR_NAME="VALUE"
$PROFILE_PATH                  = "/home/$env:USER"
$SCRIPT_PATH                   = "${BASE_ROOT}bin"
$OPENOCD_ZIP_FILE              = "${BOARD_VENDOR_JT_LC}-openocd-${OPENOCD_RELEASE}.zip"
$ARDUINO_CLI_ZIP_FILE          = "arduino_cli.zip"
$SKETCHBOOK_ZIP_FILE           = "sketchbook.zip"
$SourceFileLocation            = "${BASE_ROOT}Program Files/Notepad++/notepad++.exe"

#Read-Host -Prompt "Pausing:  Press any key to continue"

#---------------------------------------------------------
# Echo Args
#---------------------------------------------------------
function echo_args
{
$MyVariable = 1

    Write-Host "Line $(CurrentLine)  Entering                        echo_args";
    Write-Host "Line $(CurrentLine)  OS_UC                         = ${OS_UC}";
    Write-Host "Line $(CurrentLine)  OS_LC                         = ${OS_LC}";
    Write-Host "Line $(CurrentLine)  OS_C                          = ${OS_C}";
    Write-Host "Line $(CurrentLine)  DRIVEk                         = ${DRIVE}";
    Write-Host "Line $(CurrentLine)  PROJECT_NAME_ST               = ${PROJECT_NAME_ST}";
    Write-Host "Line $(CurrentLine)  PROJECT_NAME_GD               = ${PROJECT_NAME_GD}";
    Write-Host "Line $(CurrentLine)  BREAK_LINE_NO                 = ${BREAK_LINE_NO}";
    Write-Host "Line $(CurrentLine)  SLASH                         = /";
    Write-Host "Line $(CurrentLine)  INSTALL_FOLDER                = ${INSTALL_FOLDER}";	
    Write-Host "Line $(CurrentLine)  WORKSPACE_FOLDER              = ${WORKSPACE_FOLDER}";	
    Write-Host "Line $(CurrentLine)  PROJECT_NAME_ST               = ${PROJECT_NAME_ST}";	
    Write-Host "Line $(CurrentLine)  PROJECT_NAME_GD               = ${PROJECT_NAME_GD}";	
    Write-Host "Line $(CurrentLine)  CPU_NAME                      = ${CPU_NAME}";    
    Write-Host "Line $(CurrentLine)  PROJECT_STM32_RELEASE         = ${PROJECT_STM32_RELEASE}";	
    Write-Host "Line $(CurrentLine)  PROJECT_GD32_RELEASE          = ${PROJECT_GD32_RELEASE}";	
    Write-Host "Line $(CurrentLine)  CORE_STM32_NAME               = ${CORE_STM32_NAME}";    
    Write-Host "Line $(CurrentLine)  CORE_GD32_NAME                = ${CORE_GD32_NAME}";    
    Write-Host "Line $(CurrentLine)  PLATFORM_STM32_NAME           = ${PLATFORM_STM32_NAME}";    
    Write-Host "Line $(CurrentLine)  PLATFORM_GD32_NAME            = ${PLATFORM_GD32_NAME}";    
    Write-Host "Line $(CurrentLine)  ARDUINO_VERSION               = ${ARDUINO_VERSION}";
    Write-Host "Line $(CurrentLine)  ARDUINO_LIBRARIES_RELEASE     = $ARDUINO_LIBRARIES_RELEASE";	
    Write-Host "Line $(CurrentLine)  ARDUINO_CLI_RELEASE           = ${ARDUINO_CLI_RELEASE}";            
    Write-Host "Line $(CurrentLine)  BOARD_VENDOR_JT               = ${BOARD_VENDOR_JT}";            
    Write-Host "Line $(CurrentLine)  BOARD_VENDOR_JT_LC            = ${BOARD_VENDOR_JT_LC}";            
    Write-Host "Line $(CurrentLine)  BOARD_VENDOR_ST               = ${BOARD_VENDOR_ST}";            
    Write-Host "Line $(CurrentLine)  SKETCHBOOK_RELEASE            = ${SKETCHBOOK_RELEASE}";            
    Write-Host "Line $(CurrentLine)  OPENOCD_RELEASE               = ${OPENOCD_RELEASE}";            
    Write-Host "Line $(CurrentLine)  OPENOCD_CFG_RELEASE           = ${OPENOCD_CFG_RELEASE}";            
    Write-Host "Line $(CurrentLine)  ARM_GCC_VERSION               = ${ARM_GCC_VERSION}";            
    Write-Host "Line $(CurrentLine)  TEENSY_GCC_VERSION            = ${TEENSY_GCC_VERSION}";            
    Write-Host "Line $(CurrentLine)  CMSIS_VERSION                 = ${CMSIS_VERSION}";            
    Write-Host "Line $(CurrentLine)  BREAK_LINE_NO                 = ${BREAK_LINE_NO}";            
    Write-Host "Line $(CurrentLine)  TEMPL_PROJ_NAME               = ${TEMPL_PROJ_NAME}";            
    Write-Host "Line $(CurrentLine)  PROBE                         = ${PROBE}";            
    Write-Host "Line $(CurrentLine)  CPU_TARGET                    = ${CPU_TARGET}";            
    Write-Host "Line $(CurrentLine)  BUILD_SYS                     = ${BUILD_SYS}";            
    Write-Host "Line $(CurrentLine)  PROTOCOL                      = ${PROTOCOL}";            
    Write-Host "Line $(CurrentLine)  OPENOCD_STM32_CFG_FILENAME          = ${OPENOCD_STM32_CFG_FILENAME}";            
    
	Write-Host "Line $(CurrentLine)  PROJECT_STM32_URI             = ${PROJECT_STM32_URI}";
	Write-Host "Line $(CurrentLine)  VARIANTS_STM32_URI            = ${VARIANTS_STM32_URI}";
    Write-Host "Line $(CurrentLine)  CORE_STM32_ZIP_URI            = ${CORE_STM32_ZIP_URI}";            
    Write-Host "Line $(CurrentLine)  PROJECT_STM32_ZIP_NAME        = ${PROJECT_STM32_ZIP_NAME}";
    Write-Host "Line $(CurrentLine)  PROJECT_STM32_UNZIPPED_NAME   = ${PROJECT_STM32_UNZIPPED_NAME}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_CORE_STM32_ZIP_NAME   = ${ARDUINO_CORE_STM32_ZIP_NAME}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_CORE_STM32_NAME       = ${ARDUINO_CORE_STM32_NAME}";            
	Write-Host "Line $(CurrentLine)  VARIANTS_STM32_ZIP_NAME       = ${VARIANTS_STM32_ZIP_NAME}";
	Write-Host "Line $(CurrentLine)  VARIANTS_STM32_NAME           = ${VARIANTS_STM32_NAME}";
	Write-Host "Line $(CurrentLine)  VARIANTS_GD32_ZIP_NAME        = ${VARIANTS_GD32_ZIP_NAME}";
	Write-Host "Line $(CurrentLine)  VARIANTS_GD32_NAME            = ${VARIANTS_GD32_NAME}";
    Write-Host "Line $(CurrentLine)";	
    Write-Host "Line $(CurrentLine)  OPENOCD_TB_TYPE               = ${OPENOCD_TB_TYPE}";            
    Write-Host "Line $(CurrentLine)  XPACK_GCC_NAME                = ${XPACK_GCC_NAME}";            
    Write-Host "Line $(CurrentLine)  EICON_GCC_NAME                = ${EICON_GCC_NAME}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_CLI_TB_TYPE           = ${ARDUINO_CLI_TB_TYPE}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_CLI_TB_NAME           = ${ARDUINO_CLI_TB_NAME}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_TB_TYPE               = ${ARDUINO_TB_TYPE}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_ZIP_FILE              = ${ARDUINO_ZIP_FILE}";            
    Write-Host "Line $(CurrentLine)  OPENOCD_TB_NANE               = ${OPENOCD_TB_NAME}";                    
    Write-Host "Line $(CurrentLine)  OPENOCD_CFG_SCRIPT_NAME       = ${OPENOCD_CFG_SCRIPT_NAME}";

    Write-Host "Line $(CurrentLine)";
    Write-Host "Line $(CurrentLine)  BASE_PATH                     = ${BASE_PATH}";    
    Write-Host "Line $(CurrentLine)  INSTALL_FOLDER_PATH           = ${INSTALL_FOLDER_PATH}";    
    Write-Host "Line $(CurrentLine)  DLOAD_PATH                    = ${DLOAD_PATH}";	
    Write-Host "Line $(CurrentLine)  CORE_STM32_PATH               = ${CORE_STM32_PATH}";	
    Write-Host "Line $(CurrentLine)  CORE_GD32_PATH                = ${CORE_GD32_PATH}";	
    Write-Host "Line $(CurrentLine)  WORKSPACE_PATH                = ${WORKSPACE_PATH}";	
    Write-Host "Line $(CurrentLine)  BUILD_STM32_PATH              = ${BUILD_STM32_PATH}";	
    Write-Host "Line $(CurrentLine)  BUILD_GD32_PATH               = ${BUILD_32_PATH}";	
    Write-Host "Line $(CurrentLine)  APP_PATH                      = ${APP_PATH}";	
    Write-Host "Line $(CurrentLine)  ARDUINO_PATH                  = ${ARDUINO_PATH}";	
    Write-Host "Line $(CurrentLine)  PORTABLE_PATH                 = ${PORTABLE_PATH}";	
    Write-Host "Line $(CurrentLine)  HARDWARE_PATH_JT              = ${HARDWARE_PATH_JT}";            
    Write-Host "Line $(CurrentLine)  HARDWARE_PATH_ST              = ${HARDWARE_PATH_ST}";            
    Write-Host "Line $(CurrentLine)  TOOLS_PATH_JT                 = ${TOOLS_PATH_JT}";            
    Write-Host "Line $(CurrentLine)  TOOLS_PATH_ST                 = ${TOOLS_PATH_ST}";            
    Write-Host "Line $(CurrentLine)  OPENOCD_PATH                  = ${OPENOCD_PATH}";            
    Write-Host "Line $(CurrentLine)  OPENOCD_CFG_PATH              = ${OPENOCD_CFG_PATH}";            
    Write-Host "Line $(CurrentLine)  PREFS_PATH                    = ${PREFS_PATH}";            
    Write-Host "Line $(CurrentLine)  SKETCH_PATH                   = ${SKETCH_PATH}";            
    Write-Host "Line $(CurrentLine)";
    Write-Host "Line $(CurrentLine)  ARDUINO_CLI_URI               = ${ARDUINO_CLI_URI}";            
    Write-Host "Line $(CurrentLine)  OPENOCD_CFG_SCRIPT_URI        = ${OPENOCD_CFG_SCRIPT_URI}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_LIBRARIES_ZIP_URI     = ${ARDUINO_LIBRARIES_ZIP_URI}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_ZIP_URI               = ${ARDUINO_ZIP_URI}";            
    Write-Host "Line $(CurrentLine)  SKETCHBOOK_ZIP_URI            = ${SKETCHBOOK_ZIP_URI}";            
    Write-Host "Line $(CurrentLine)  OPENOCD_TB_NAME               = ${OPENOCD_TB_NAME}";            
    Write-Host "Line $(CurrentLine)  OPENOCD_TB_URI                = ${OPENOCD_TB_URI}";            
    Write-Host "Line $(CurrentLine)  STM32_INDEX                   = ${STM32_INDEX}";            
    Write-Host "Line $(CurrentLine)  KINETIS_INDEX                 = ${KINETIS_INDEX}";            
    Write-Host "Line $(CurrentLine)  STMicroelectronics_INDEX      = ${STMicroelectronics_INDEX}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_ZIP_FILE              = ${ARDUINO_ZIP_FILE}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_LIBRARIES_ZIP_FILE    = ${ARDUINO_LIBRARIES_ZIP_FILE}";            
    Write-Host "Line $(CurrentLine)  SKETCH_NAME                   = ${SKETCH_NAME}";            
    Write-Host "Line $(CurrentLine)  USER                          = ${USER}";            
    Write-Host "Line $(CurrentLine)  PROFILE_PATH                  = ${PROFILE_PATH}";            
    Write-Host "Line $(CurrentLine)  SCRIPT_PATH                   = ${SCRIPT_PATH}";            
    Write-Host "Line $(CurrentLine)  OPENOCD_ZIP_FILE              = ${OPENOCD_ZIP_FILE}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_CLI_ZIP_FILE          = ${ARDUINO_CLI_ZIP_FILE}";            
    Write-Host "Line $(CurrentLine)  SKETCHBOOK_ZIP_FILE           = ${SKETCHBOOK_ZIP_FILE}";            
    Write-Host "Line $(CurrentLine)  SourceFileLocation            = ${SourceFileLocation}";            
    Write-Host "Line $(CurrentLine)  USER                          = ${USER}";            
    Write-Host "Line $(CurrentLine)  Leaving                         echo_args";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# Main Function
#---------------------------------------------------------
function main
{
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------";
    Write-Host "Line $(CurrentLine)  Entering                        main"; 

    Write-Host "Line $(CurrentLine)  Calling                         echo_args";
    echo_args                                                                        # Calling echo_args
Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                         check_environment";
    check_environment
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                         install_arduino_stm32_core";
    install_arduino_stm32_core
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                         install_arduino_gd32_core";
    install_arduino_gd32_core
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                         install_arduino_stm32_cmake_project";
    install_arduino_stm32_cmake_project
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                         install_arduino_gd32_cmake_project";
    install_arduino_gd32_cmake_project
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    
    Write-Host "Line $(CurrentLine)  Calling                         install_j-tagit_stm32_variants-lin";
    install_j-tagit_stm32_variants-lin
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                         iinstall_j-tagit_gd32_variants-lin";
    iinstall_j-tagit_gd32_variants-lin
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                         install_codelite_stm32_config";
    install_codelite_stm32_config
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                         install_codelite_gd32_config";
    install_codelite_gd32_config
Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

##    Write-Host "Line $(CurrentLine)  Calling                         rename_app_files";
##    rename_app_files
##Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                         create_openocd_stm32_config";
    create_openocd_stm32_config
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                         create_openocd_gd32_config";
    create_openocd_gd32_config
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                         build_stm32_project";
    build_stm32_project
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                         build_gd32_project";
    build_gd32_project
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
}

#---------------------------------------------------------
# build_gd32_project
#---------------------------------------------------------
function build_gd32_project
{
    Write-Host "Line $(CurrentLine)  Entering                        build_gd32_project";
    cd ${BUILD_GD32_PATH}
    Write-Host "Line $(CurrentLine)  PWD                           = ${PWD}";
    Write-Host "Line $(CurrentLine)  Executing                       rm -rf *";
    if ($IsLinux)
    {
        rm -rf *
    }
    elseif ($IsWindows)
    {
        rm *
    }
    Write-Host "Line $(CurrentLine)  Executing                       cmake .. -G "CodeLite - Unix Makefiles"";
    cmake .. -G "CodeLite - Unix Makefiles"
    Write-Host "Line $(CurrentLine)  Executing                       make";
    make        
    Write-Host "Line $(CurrentLine)  Leaving                         build_gd32_project";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# build_stm32_project
#---------------------------------------------------------
function build_stm32_project
{
    Write-Host "Line $(CurrentLine)  Entering                        build_stm32_project";
    cd ${BUILD_STM32_PATH}
    Write-Host "Line $(CurrentLine)  PWD                           = ${PWD}";
    Write-Host "Line $(CurrentLine)  Executing                       rm -rf *";
    if ($IsLinux)
    {
        rm -rf *
    }
    elseif ($IsWindows)
    {
        rm *
    }
    Write-Host "Line $(CurrentLine)  Executing                       cmake .. -G "CodeLite - Unix Makefiles"";
    cmake .. -G "CodeLite - Unix Makefiles"
    Write-Host "Line $(CurrentLine)  Executing                       make";
    make        
    Write-Host "Line $(CurrentLine)  Leaving                         build_stm32_project";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}


#---------------------------------------------------------
# rename_app_files
#---------------------------------------------------------
function rename_app_files
{
    Write-Host "Line $(CurrentLine)  Entering                        rename_app_files";
    Write-Host "Line $(CurrentLine)  Executing                       Rename-Item  ${APP_PATH}/${PROJ_NAME_ST}.cpp  ${APP_PATH}/${PROJECT_NAME_ST}.cpp";
    Rename-Item  ${APP_PATH}/${PROJ_NAME_ST}.cpp  ${APP_PATH}/${PROJECT_NAME_ST}.cpp

    Write-Host "Line $(CurrentLine)  Leaving                         rename_app_files";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# create_openocd_gd32_config
#---------------------------------------------------------
function create_openocd_gd32_config
{
    Write-Host "Line $(CurrentLine)  Entering                        create_openocd_gd32_config";
    Write-Host "Line $(CurrentLine)  Executing                       if (Test-Path ${OPENOCD_CFG_PATH}/${OPENOCD_GD32_CFG_FILENAME})";
    if (Test-Path ${OPENOCD_CFG_PATH}/${OPENOCD_GD32_CFG_FILENAME})    
    {
        Write-Host "Line $(CurrentLine)  OpenOCD CFG File                exists  ...  deleting" ; 
        Remove-Item -Recurse -Force ${OPENOCD_CFG_PATH}/${OPENOCD_GD32_CFG_FILENAME}
    }
	else
    {
        Write-Host "Line $(CurrentLine)  OpenOCD Cfg File                does not exist  ...  continuing" ;  
    }
    Write-Host "Line $(CurrentLine)  Adding                          ${OPENOCD_CFG_PATH}/${OPENOCD_GD32_CFG_FILENAME} Contents";
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_GD32_CFG_FILENAME} "# ${BOARD_VENDOR_JT} ${PROJECT_NAME_GD}"
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_GD32_CFG_FILENAME} ""
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_GD32_CFG_FILENAME} "source [find interface/${BOARD_VENDOR_JT}/${PROBE}-${PROTOCOL}.cfg]"
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_GD32_CFG_FILENAME} ""
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_GD32_CFG_FILENAME} "source [find target/${BOARD_VENDOR_JT}/${CPU_TARGET}]"
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_GD32_CFG_FILENAME} ""
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_GD32_CFG_FILENAME} "init"
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_GD32_CFG_FILENAME} ""
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_GD32_CFG_FILENAME} "program ${BUILD_GD32_PATH}/${PROJECT_NAME_ST}.hex verify reset"
    Write-Host "Line $(CurrentLine)  Editing                         ${OPENOCD_CFG_PATH}/${OPENOCD_GD32_CFG_FILENAME} Completed";
    Write-Host "Line $(CurrentLine)  Leaving                         create_openocd_gd32_config";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# create_openocd_stm32_config
#---------------------------------------------------------
function create_openocd_stm32_config
{
    Write-Host "Line $(CurrentLine)  Entering                        create_openocd_stm32_config";
    Write-Host "Line $(CurrentLine)  Executing                       if (Test-Path ${OPENOCD_CFG_PATH}/${OPENOCD_STM32_CFG_FILENAME})";
    if (Test-Path ${OPENOCD_CFG_PATH}/${OPENOCD_STM32_CFG_FILENAME})    
    {
        Write-Host "Line $(CurrentLine)  OpenOCD CFG File                exists  ...  deleting" ; 
        Remove-Item -Recurse -Force ${OPENOCD_CFG_PATH}/${OPENOCD_STM32_CFG_FILENAME}
    }
	else
    {
        Write-Host "Line $(CurrentLine)  OpenOCD Cfg File                does not exist  ...  continuing" ;  
    }
    Write-Host "Line $(CurrentLine)  Adding                          ${OPENOCD_CFG_PATH}/${OPENOCD_STM32_CFG_FILENAME} Contents";
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_STM32_CFG_FILENAME} "# ${BOARD_VENDOR_JT} ${PROJECT_NAME_ST}"
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_STM32_CFG_FILENAME} ""
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_STM32_CFG_FILENAME} "source [find interface/${BOARD_VENDOR_JT}/${PROBE}-${PROTOCOL}.cfg]"
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_STM32_CFG_FILENAME} ""
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_STM32_CFG_FILENAME} "source [find target/${BOARD_VENDOR_JT}/${CPU_TARGET}]"
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_STM32_CFG_FILENAME} ""
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_STM32_CFG_FILENAME} "init"
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_STM32_CFG_FILENAME} ""
    Add-Content ${OPENOCD_CFG_PATH}/${OPENOCD_STM32_CFG_FILENAME} "program ${BUILD_STM32_PATH}/${PROJECT_NAME_ST}.hex verify reset"
    Write-Host "Line $(CurrentLine)  Editing                         ${OPENOCD_CFG_PATH}/${OPENOCD_STM32_CFG_FILENAME} Completed";
    Write-Host "Line $(CurrentLine)  Leaving                         create_openocd_stm32_config";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# install_codelite_gd32_config
#---------------------------------------------------------
function install_codelite_gd32_config
{
    Write-Host "Line $(CurrentLine)  Entering                        install_codelite_gd32_config";
    Write-Host "Line $(CurrentLine)  Executing                       if (Test-Path ${DRIVE}/.gdbinit)";
    if (Test-Path ${DRIVE}/.gdbinit)    
    {
        Write-Host "Line $(CurrentLine)  .gdbinit                        exists  ...  continuing" ; 
#        Remove-Item -Recurse -Force ${DRIVE}/.gdbinit
    }
	else
    {
        Write-Host "Line $(CurrentLine)  .gdbinit                        does not exist  ...  continuing" ;  
    }
    Write-Host "Line $(CurrentLine)  Adding                          ${DRIVE}/.gdbinit Contents";
    Add-Content $DRIVE/.gdbinit "set auto-load safe-path ${BUILD_STM32_PATH}/.gdbinit"
    Write-Host "Line $(CurrentLine)  Adding                          ${BUILD_GD32_PATH}/.gdbinit Contents";
    Add-Content ${BUILD_GD32_PATH}/.gdbinit "set history save on"
    Add-Content ${BUILD_GD32_PATH}/.gdbinit "set pagination off"
    Add-Content ${BUILD_GD32_PATH}/.gdbinit "set print pretty on"
    Add-Content ${BUILD_GD32_PATH}/.gdbinit "set confirm off"
    Add-Content ${BUILD_GD32_PATH}/.gdbinit "target extended-remote:3333"
    Add-Content ${BUILD_GD32_PATH}/.gdbinit "b ${APP_PATH}/${PROJECT_NAME_GD}.cpp:${BREAK_LINE_NO}"
    Write-Host "Line $(CurrentLine)  Editing                         .gdbinit Completed";
    Write-Host "Line $(CurrentLine)  Leaving                         install_codelite_gd32_config";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# install_codelite_stm32_config
#---------------------------------------------------------
function install_codelite_stm32_config
{
    Write-Host "Line $(CurrentLine)  Entering                        install_codelite_stm32_config";
    Write-Host "Line $(CurrentLine)  Executing                       if (Test-Path ${DRIVE}/.gdbinit)";
    if (Test-Path ${DRIVE}/.gdbinit)    
    {
        Write-Host "Line $(CurrentLine)  .gdbinit                        exists  ...  deleting" ; 
        Remove-Item -Recurse -Force ${DRIVE}/.gdbinit
    }
	else
    {
        Write-Host "Line $(CurrentLine)  .gdbinit                        does not exist  ...  continuing" ;  
    }
    Write-Host "Line $(CurrentLine)  Adding                          ${DRIVE}/.gdbinit Contents";
    Add-Content $DRIVE/.gdbinit "set auto-load safe-path ${BUILD_STM32_PATH}/.gdbinit"
    Write-Host "Line $(CurrentLine)  Adding                          ${BUILD_STM32_PATH}/.gdbinit Contents";
    Add-Content ${BUILD_STM32_PATH}/.gdbinit "set history save on"
    Add-Content ${BUILD_STM32_PATH}/.gdbinit "set pagination off"
    Add-Content ${BUILD_STM32_PATH}/.gdbinit "set print pretty on"
    Add-Content ${BUILD_STM32_PATH}/.gdbinit "set confirm off"
    Add-Content ${BUILD_STM32_PATH}/.gdbinit "target extended-remote:3333"
    Add-Content ${BUILD_STM32_PATH}/.gdbinit "b ${APP_PATH}/${PROJECT_NAME_ST}.cpp:${BREAK_LINE_NO}"
    Write-Host "Line $(CurrentLine)  Editing                         .gdbinit Completed";
    Write-Host "Line $(CurrentLine)  Leaving                         install_codelite_stm32_config";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# install_arduino_gd32_cmake_project
#---------------------------------------------------------
function install_arduino_gd32_cmake_project
{
    Write-Host "Line $(CurrentLine)  Entering                        install_arduino_gd32_cmake_project";

    Write-Host "Line $(CurrentLine)  Executing                       if (Test-Path ${WORKSPACE_PATH}/${PROJECT_NAME_GD})";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${WORKSPACE_PATH}/${PROJECT_NAME_GD})
    {
        Write-Host "Line $(CurrentLine)  Project                         Exists ... deleting" ;  
        Write-Host "Line $(CurrentLine)  Executing                       Remove-Item -Recurse -Force ${WORKSPACE_PATH}/${PROJECT_NAME_GD}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
        Remove-Item -Recurse -Force ${WORKSPACE_PATH}/${PROJECT_NAME_GD}
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Project                         does not exist ... continuing" ;  
    }
    Write-Host "Line $(CurrentLine)  Installing                      ${WORKSPACE_PATH}/${PROJECT_NAME_GD}";
    Write-Host "Line $(CurrentLine)  Executing                       Invoke-WebRequest -Uri ${PROJECT_GD32_URI} -Outfile ${DLOAD_PATH}/${PROJECT_GD32_ZIP_NAME}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Invoke-WebRequest -Uri ${PROJECT_GD32_URI} -Outfile ${DLOAD_PATH}/${PROJECT_GD32_ZIP_NAME}
    Write-Host "Line $(CurrentLine)  Executing                       Expand-Archive -Force -Path ${DLOAD_PATH}/${PROJECT_GD32_ZIP_NAME} -DestinationPath ${WORKSPACE_PATH}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Expand-Archive -Force -Path ${DLOAD_PATH}/${PROJECT_GD32_ZIP_NAME} -DestinationPath ${WORKSPACE_PATH}

    Write-Host "Line $(CurrentLine)  Renaming                        ${WORKSPACE_PATH}/${PROJECT_GD32_UNZIPPED_NAME}";
    Write-Host "Line $(CurrentLine)  To                              ${WORKSPACE_PATH}/${PROJECT_NAME_GD}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Rename-Item  ${WORKSPACE_PATH}/${PROJECT_GD32_UNZIPPED_NAME}  ${WORKSPACE_PATH}/${PROJECT_NAME_GD}

    Write-Host "Line $(CurrentLine)  Leaving                         install_arduino_gd32_cmake_project";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# install_arduino_stm32_cmake_project
#---------------------------------------------------------
function install_arduino_stm32_cmake_project
{
    Write-Host "Line $(CurrentLine)  Entering                        install_arduino_stm32_cmake_project";

    Write-Host "Line $(CurrentLine)  Executing                       if (Test-Path ${WORKSPACE_PATH}/${PROJECT_NAME_ST})";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${WORKSPACE_PATH}/${PROJECT_NAME_ST})
    {
        Write-Host "Line $(CurrentLine)  Project                         Exists ... deleting" ;  
        Write-Host "Line $(CurrentLine)  Executing                       Remove-Item -Recurse -Force ${WORKSPACE_PATH}/${PROJECT_NAME_ST}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
        Remove-Item -Recurse -Force ${WORKSPACE_PATH}/${PROJECT_NAME_ST}
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Project                         does not exist ... continuing" ;  
    }
    Write-Host "Line $(CurrentLine)  Installing                      ${WORKSPACE_PATH}/${PROJECT_NAME_ST}";
    Write-Host "Line $(CurrentLine)  Executing                       Invoke-WebRequest -Uri ${PROJECT_STM32_URI} -Outfile ${DLOAD_PATH}/${PROJECT_STM32_ZIP_NAME}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Invoke-WebRequest -Uri ${PROJECT_STM32_URI} -Outfile ${DLOAD_PATH}/${PROJECT_STM32_ZIP_NAME}
    Write-Host "Line $(CurrentLine)  Executing                       Expand-Archive -Force -Path ${DLOAD_PATH}/${PROJECT_STM32_ZIP_NAME} -DestinationPath ${WORKSPACE_PATH}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Expand-Archive -Force -Path ${DLOAD_PATH}/${PROJECT_STM32_ZIP_NAME} -DestinationPath ${WORKSPACE_PATH}

    Write-Host "Line $(CurrentLine)  Renaming                        ${WORKSPACE_PATH}/${PROJECT_STM32_UNZIPPED_NAME}";
    Write-Host "Line $(CurrentLine)  To                              ${WORKSPACE_PATH}/${PROJECT_NAME_ST}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Rename-Item  ${WORKSPACE_PATH}/${PROJECT_STM32_UNZIPPED_NAME}  ${WORKSPACE_PATH}/${PROJECT_NAME_ST}

    Write-Host "Line $(CurrentLine)  Leaving                         install_arduino_stm32_cmake_project";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# iinstall_j-tagit_gd32_variants-lin
#---------------------------------------------------------
function iinstall_j-tagit_gd32_variants-lin
{
    Write-Host "Line $(CurrentLine)  Entering                        iinstall_j-tagit_gd32_variants-lin";
    Write-Host "Line $(CurrentLine)  Executing                       if (Test-Path ${WORKSPACE_PATH}/${PROJECT_NAME_GD}/${CORE_GD32_NAME}/variants)";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${WORKSPACE_PATH}/${PROJECT_NAME_GD}/${CORE_GD32_NAME}/variants)
    {
        Write-Host "Line $(CurrentLine)  variants                        exist  ...  deleting" ;  
        Write-Host "Line $(CurrentLine)  Executing                       Remove-Item -Recurse -Force ${WORKSPACE_PATH}/${PROJECT_NAME_GD}/${CORE_GD32_NAME}/variants" ;  
        Remove-Item -Recurse -Force ${WORKSPACE_PATH}/${PROJECT_NAME_GD}/${CORE_GD32_NAME}/variants
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    }
	else
    {
        Write-Host "Line $(CurrentLine)  variants                        do not exist  ...  continuing" ;  
    }


#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"


    if (Test-Path ${WORKSPACE_PATH}/${PROJECT_NAME_GD}/${CORE_GD32_NAME}/boards.txt)
    {
        Write-Host "Line $(CurrentLine)  boards.txt                      exist  ...  deleting" ;  
        Write-Host "Line $(CurrentLine)  Executing                       Remove-Item -Recurse -Force ${WORKSPACE_PATH}/${PROJECT_NAME_GD}/${CORE_GD32_NAME}/boards.txt" ;  
        Remove-Item -Recurse -Force ${WORKSPACE_PATH}/${PROJECT_NAME_GD}/${CORE_GD32_NAME}/boards.txt
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    }
	else
    {
        Write-Host "Line $(CurrentLine)  boards.txt                      does not exist  ...  continuing" ;  
    }

    if (Test-Path ${WORKSPACE_PATH}/${PROJECT_NAME_GD}/${CORE_GD32_NAME}/platform.txt)
    {
        Write-Host "Line $(CurrentLine)  platform.txt                    exist  ...  deleting" ;  
        Write-Host "Line $(CurrentLine)  Executing                       Remove-Item -Recurse -Force ${WORKSPACE_PATH}/${PROJECT_NAME_ST}/${CORE_STM32_NAME}/platform.txt" ;  
        Remove-Item -Recurse -Force ${WORKSPACE_PATH}/${PROJECT_NAME_GD}/${CORE_GD32_NAME}/platform.txt
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    }
	else
    {
        Write-Host "Line $(CurrentLine)  platform.txt                    does not exist  ...  continuing" ;  
    }

    Write-Host "Line $(CurrentLine)  Executing                       if (Test-Path ${DLOAD_PATH}/${VARIANTS_GD32_ZIP_NAME})";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${DLOAD_PATH}/${VARIANTS_GD32_ZIP_NAME})
    {
        Write-Host "Line $(CurrentLine)  variants zip                    exist  ...  deleting" ;  
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
        Write-Host "Line $(CurrentLine)  Executing                       Remove-Item -Recurse -Force ${DLOAD_PATH}/${VARIANTS_GD32_ZIP_NAME}" ;  
        Remove-Item -Recurse -Force ${DLOAD_PATH}/${VARIANTS_GD32_ZIP_NAME}        
    }

    Write-Host "Line $(CurrentLine)  Executing                       if (Test-Path ${DLOAD_PATH}/${VARIANTS_GD32_NAME})";
    if (Test-Path ${DLOAD_PATH}/${VARIANTS_GD32_NAME})
    {
        Write-Host "Line $(CurrentLine)  variants unzipped               exists  ...  deleting" ;  
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
        Write-Host "Line $(CurrentLine)  Executing                       Remove-Item -Recurse -Force ${DLOAD_PATH}/${VARIANTS_GD32_NAME}" ;  
        Remove-Item -Recurse -Force ${DLOAD_PATH}/${VARIANTS_GD32_NAME}        
    }
		
    Write-Host "Line $(CurrentLine)  Downloading                     ${DLOAD_PATH}/${VARIANTS_GD32_ZIP_NAME}";
    Write-Host "Line $(CurrentLine)  Executing                       Invoke-WebRequest -Uri ${VARIANTS_GD32_URI} -Outfile ${DLOAD_PATH}/${VARIANTS_GD32_ZIP_NAME}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Invoke-WebRequest -Uri ${VARIANTS_GD32_URI} -Outfile ${DLOAD_PATH}/${VARIANTS_GD32_ZIP_NAME}

    Write-Host "Line $(CurrentLine)  Executing                       Expand-Archive -Path ${DLOAD_PATH}/${VARIANTS_GD32_ZIP_NAME} -DestinationPath ${DLOAD_PATH}";
    Expand-Archive -Path ${DLOAD_PATH}/${VARIANTS_GD32_ZIP_NAME} -DestinationPath ${DLOAD_PATH}
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
	
    Write-Host "Line $(CurrentLine)  Executing                       Copy-Item -Recurse -Force -path: ${DLOAD_PATH}/${VARIANTS_GD32_NAME}/* -destination: ${INSTALL_FOLDER_PATH}/${CORE_GD32_NAME}";
    Copy-Item -Recurse -Force -path: ${DLOAD_PATH}/${VARIANTS_GD32_NAME}/* -destination: ${INSTALL_FOLDER_PATH}/${CORE_GD32_NAME}
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
	
    Write-Host "Line $(CurrentLine)  Leaving                         iinstall_j-tagit_gd32_variants-lin";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# install_j-tagit_stm32_variants-lin
#---------------------------------------------------------
function install_j-tagit_stm32_variants-lin
{
    Write-Host "Line $(CurrentLine)  Entering                        install_j-tagit_stm32_variants-lin";
    Write-Host "Line $(CurrentLine)  Executing                       if (Test-Path ${WORKSPACE_PATH}/${PROJECT_NAME_ST}/${CORE_STM32_NAME}/variants)";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${WORKSPACE_PATH}/${PROJECT_NAME_ST}/${CORE_STM32_NAME}/variants)
    {
        Write-Host "Line $(CurrentLine)  variants                        exist  ...  deleting" ;  
        Write-Host "Line $(CurrentLine)  Executing                       Remove-Item -Recurse -Force ${WORKSPACE_PATH}/${PROJECT_NAME_ST}/${CORE_STM32_NAME}/variants" ;  
        Remove-Item -Recurse -Force ${WORKSPACE_PATH}/${PROJECT_NAME_ST}/${CORE_STM32_NAME}/variants
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    }
	else
    {
        Write-Host "Line $(CurrentLine)  variants                        do not exist  ...  continuing" ;  
    }


#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"


    if (Test-Path ${WORKSPACE_PATH}/${PROJECT_NAME_ST}/${CORE_STM32_NAME}/boards.txt)
    {
        Write-Host "Line $(CurrentLine)  boards.txt                      exist  ...  deleting" ;  
        Write-Host "Line $(CurrentLine)  Executing                       Remove-Item -Recurse -Force ${WORKSPACE_PATH}/${PROJECT_NAME_ST}/${CORE_STM32_NAME}/boards.txt" ;  
        Remove-Item -Recurse -Force ${WORKSPACE_PATH}/${PROJECT_NAME_ST}/${CORE_STM32_NAME}/boards.txt
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    }
	else
    {
        Write-Host "Line $(CurrentLine)  boards.txt                      does not exist  ...  continuing" ;  
    }

    if (Test-Path ${WORKSPACE_PATH}/${PROJECT_NAME_ST}/${CORE_STM32_NAME}/platform.txt)
    {
        Write-Host "Line $(CurrentLine)  platform.txt                    exist  ...  deleting" ;  
        Write-Host "Line $(CurrentLine)  Executing                       Remove-Item -Recurse -Force ${WORKSPACE_PATH}/${PROJECT_NAME_ST}/${CORE_STM32_NAME}/platform.txt" ;  
        Remove-Item -Recurse -Force ${WORKSPACE_PATH}/${PROJECT_NAME_ST}/${CORE_STM32_NAME}/platform.txt
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    }
	else
    {
        Write-Host "Line $(CurrentLine)  platform.txt                    does not exist  ...  continuing" ;  
    }

    Write-Host "Line $(CurrentLine)  Executing                       if (Test-Path ${DLOAD_PATH}/${VARIANTS_STM32_ZIP_NAME})";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${DLOAD_PATH}/${VARIANTS_STM32_ZIP_NAME})
    {
        Write-Host "Line $(CurrentLine)  variants zip                    exist  ...  deleting" ;  
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
        Write-Host "Line $(CurrentLine)  Executing                       Remove-Item -Recurse -Force ${DLOAD_PATH}/${VARIANTS_ZIP+NAME}" ;  
        Remove-Item -Recurse -Force ${DLOAD_PATH}/${VARIANTS_STM32_ZIP_NAME}        
    }

    Write-Host "Line $(CurrentLine)  Executing                       if (Test-Path ${DLOAD_PATH}/${VARIANTS_STM32_NAME})";
    if (Test-Path ${DLOAD_PATH}/${VARIANTS_STM32_NAME})
    {
        Write-Host "Line $(CurrentLine)  variants unzipped               exists  ...  deleting" ;  
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
        Write-Host "Line $(CurrentLine)  Executing                       Remove-Item -Recurse -Force ${DLOAD_PATH}/${VARIANTS_STM32_NAME}" ;  
        Remove-Item -Recurse -Force ${DLOAD_PATH}/${VARIANTS_STM32_NAME}        
    }
		
    Write-Host "Line $(CurrentLine)  Downloading                     ${DLOAD_PATH}/${VARIANTS_STM32_ZIP_NAME}";
    Write-Host "Line $(CurrentLine)  Executing                       Invoke-WebRequest -Uri ${VARIANTS_STM32_URI} -Outfile ${DLOAD_PATH}/${VARIANTS_STM32_ZIP_NAME}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Invoke-WebRequest -Uri ${VARIANTS_STM32_URI} -Outfile ${DLOAD_PATH}/${VARIANTS_STM32_ZIP_NAME}

    Write-Host "Line $(CurrentLine)  Executing                       Expand-Archive -Path ${DLOAD_PATH}/${VARIANTS_STM32_ZIP_NAME} -DestinationPath ${DLOAD_PATH}";
    Expand-Archive -Path ${DLOAD_PATH}/${VARIANTS_STM32_ZIP_NAME} -DestinationPath ${DLOAD_PATH}
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
	
    Write-Host "Line $(CurrentLine)  Executing                       Copy-Item -Recurse -Force -path: ${DLOAD_PATH}/${VARIANTS_STM32_NAME}/* -destination: ${INSTALL_FOLDER_PATH}/${CORE_STM32_NAME}";
    Copy-Item -Recurse -Force -path: ${DLOAD_PATH}/${VARIANTS_STM32_NAME}/* -destination: ${INSTALL_FOLDER_PATH}/${CORE_STM32_NAME}
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
	
    Write-Host "Line $(CurrentLine)  Leaving                         install_j-tagit_stm32_variants-lin";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# install_arduino_gd32_core
#---------------------------------------------------------
function install_arduino_gd32_core
{
    Write-Host "Line $(CurrentLine)  Entering                        install_arduino_gd32_core";


    Write-Host "Line $(CurrentLine)  Executing                       if (Test-Path ${DLOAD_PATH}/${ARDUINO_CORE_GD32_ZIP_NAME})";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${DLOAD_PATH}/${ARDUINO_CORE_GD32_ZIP_NAME})
    {
        Write-Host "Line $(CurrentLine)  Arduino Core                    Exists" ;  
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Arduino Core                    does not exist  ...   downloading" ;  
        Write-Host "Line $(CurrentLine)  Downloading                     ${DLOAD_PATH}/${ARDUINO_CORE_GD32_ZIP_NAME}";
        Write-Host "Line $(CurrentLine)  Executing                       Invoke-WebRequest -Uri ${CORE_GD32_ZIP_URI} -Outfile ${DLOAD_PATH}/${ARDUINO_CORE_GD32_ZIP_NAME}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
        Invoke-WebRequest -Uri ${CORE_GD32_ZIP_URI} -Outfile ${DLOAD_PATH}/${ARDUINO_CORE_GD32_ZIP_NAME}
    }
	
    Write-Host "Line $(CurrentLine)  Executing                       if (Test-Path ${CORE_GD32_PATH})";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${CORE_GD32_PATH})
    {
        Write-Host "Line $(CurrentLine)  Arduino Core                   is installed  ...  deleting" ;  
        Write-Host "Line $(CurrentLine)  Executing                       Remove-Item -Recurse -Force ${CORE_GD32_PATH}" ;  
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
        Remove-Item -Recurse -Force ${CORE_GD32_PATH}
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Arduino Core                    is not installed ...  continuing" ;  
    }

    Write-Host "Line $(CurrentLine)  Executing                       Expand-Archive -Path ${DLOAD_PATH}/${ARDUINO_CORE_GD32_ZIP_NAME} -DestinationPath ${INSTALL_FOLDER_PATH}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Expand-Archive -Path ${DLOAD_PATH}/${ARDUINO_CORE_GD32_ZIP_NAME} -DestinationPath ${INSTALL_FOLDER_PATH}

#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Renaming                        ${INSTALL_FOLDER_PATH}/${ARDUINO_CORE_GD32_NAME}";
    Write-Host "Line $(CurrentLine)  To                              ${CORE_GD32_PATH}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Rename-Item  ${INSTALL_FOLDER_PATH}/${ARDUINO_CORE_GD32_NAME}  ${CORE_GD32_PATH}
		
    Write-Host "Line $(CurrentLine)  Leaving                         install_arduino_gd32_core";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# install_arduino_stm32_core
#---------------------------------------------------------
function install_arduino_stm32_core
{
    Write-Host "Line $(CurrentLine)  Entering                        install_arduino_stm32_core";


    Write-Host "Line $(CurrentLine)  Executing                       if (Test-Path ${DLOAD_PATH}/${ARDUINO_CORE_STM32_ZIP_NAME})";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${DLOAD_PATH}/${ARDUINO_CORE_STM32_ZIP_NAME})
    {
        Write-Host "Line $(CurrentLine)  Arduino Core                    Exists" ;  
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Arduino Core                    does not exist  ...   downloading" ;  
        Write-Host "Line $(CurrentLine)  Downloading                     ${DLOAD_PATH}/${ARDUINO_CORE_STM32_ZIP_NAME}";
        Write-Host "Line $(CurrentLine)  Executing                       Invoke-WebRequest -Uri ${CORE_STM32_ZIP_URI} -Outfile ${DLOAD_PATH}/${ARDUINO_CORE_STM32_ZIP_NAME}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
        Invoke-WebRequest -Uri ${CORE_STM32_ZIP_URI} -Outfile ${DLOAD_PATH}/${ARDUINO_CORE_STM32_ZIP_NAME}
    }
	
    Write-Host "Line $(CurrentLine)  Executing                       if (Test-Path ${CORE_STM32_PATH})";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${CORE_STM32_PATH})
    {
        Write-Host "Line $(CurrentLine)  Arduino Core                   is installed  ...  deleting" ;  
        Write-Host "Line $(CurrentLine)  Executing                       Remove-Item -Recurse -Force ${CORE_STM32_PATH}" ;  
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
        Remove-Item -Recurse -Force ${CORE_STM32_PATH}
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Arduino Core                    is not installed ...  continuing" ;  
    }

    Write-Host "Line $(CurrentLine)  Executing                       Expand-Archive -Path ${DLOAD_PATH}/${ARDUINO_CORE_STM32_ZIP_NAME} -DestinationPath ${INSTALL_FOLDER_PATH}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Expand-Archive -Path ${DLOAD_PATH}/${ARDUINO_CORE_STM32_ZIP_NAME} -DestinationPath ${INSTALL_FOLDER_PATH}

#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Renaming                        ${INSTALL_FOLDER_PATH}/${ARDUINO_CORE_STM32_NAME}";
    Write-Host "Line $(CurrentLine)  To                              ${CORE_STM32_PATH}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Rename-Item  ${INSTALL_FOLDER_PATH}/${ARDUINO_CORE_STM32_NAME}  ${CORE_STM32_PATH}
		
    Write-Host "Line $(CurrentLine)  Leaving                         install_arduino_stm32_core";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}


#---------------------------------------------------------
# check_environment
#---------------------------------------------------------
function check_environment
{
    Write-Host "Line $(CurrentLine)  Entering                        check_environment";

    Write-Host "Line $(CurrentLine)  Executing                       if (Test-Path ${INSTALL_FOLDER_PATH})";
    if (Test-Path ${INSTALL_FOLDER_PATH})
    {
        Write-Host "Line $(CurrentLine)  INSTALL Folder Path             Exists ... continuing" ;  
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Creating                        ${INSTALL_FOLDER_PATH}";
        Write-Host "Line $(CurrentLine)  Executing                       md ${INSTALL_FOLDER_PATH}";
        md ${INSTALL_FOLDER_PATH}
    }

    Write-Host "Line $(CurrentLine)  Executing                       if (Test-Path ${DLOAD_PATH})";
    if (Test-Path ${DLOAD_PATH})
    {
        Write-Host "Line $(CurrentLine)  dload folser                    Exists ... continuing" ;  
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Creating                        ${DLOAD_PATH}";
        Write-Host "Line $(CurrentLine)  Executing                       md $DLOAD_PATH}";
        md ${DLOAD_PATH}
    }

    Write-Host "Line $(CurrentLine)  Executing                       if (Test-Path ${WORKSPACE_PATH})";
    if (Test-Path ${WORKSPACE_PATH})
    {
        Write-Host "Line $(CurrentLine)  WORKSPACE Folder                 Exists ... continuing" ;  
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Creating                        ${WORKSPACE_PATH}";
        Write-Host "Line $(CurrentLine)  Executing                       md ${WORKSPACE_PATH}";
        md ${WORKSPACE_PATH}
    }

    Write-Host "Line $(CurrentLine)  Leaving                         check_environment";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}


#---------------------------------------------------------
# Setup OS Stuff
#---------------------------------------------------------
function setup_os_stuff
{
    Write-Host "Line $(CurrentLine)  Entering                        setup_os_stuff"
	if(${OS} -eq "WIN")
    { 
        Write-Host "Line $(CurrentLine)  OS                            = Windows";            
        Write-Host "Line $(CurrentLine)  SLASH                         = /";
        Write-Host "Line $(CurrentLine)  ARDUINO_VERSION               = ${ARDUINO_VERSION}";
        Write-Host "Line $(CurrentLine)  SLASH                         = /";
        / = "\";
        Write-Host "Line $(CurrentLine)  SLASH                         = /";
        ${BASE_ROOT} = "${DRIVE}:/"	
        Write-Host "Line $(CurrentLine)  BASE_ROOT                     = ${BASE_ROOT}";
    }
    elseIf(${OS} -eq "LIN")
    {
        Write-Host "Line $(CurrentLine)  OS                            = Linux";            
    }
    ElseIf(${OS} -eq "WSL" )
    {
        Write-Host "Line $(CurrentLine)  OS                            = WSL";            
    }
    Else
    {
        Write-Host "Line $(CurrentLine)  OS                            = Not Defined";            
    }
    Write-Host "Line $(CurrentLine)  Leaving                         setup_os_stuff"
}

#---------------------------------------------------------
# Launch_Arduino_IDE
#---------------------------------------------------------
function Launch_Arduino_IDE
{
    Write-Host "Line $(CurrentLine)  Entering                        Launch_Arduino_IDE"

    Write-Host "Line $(CurrentLine)  Executing                       $ARDUINO_PATH/arduino "
#Read-Host -Prompt "Pausing:  Press any key to continue"
    & $ARDUINO_PATH/arduino
    Write-Host "Line $(CurrentLine)  Leaving                         Launch_Arduino_IDE"
}

#---------------------------------------------------------
# CurrentLine
#---------------------------------------------------------
function CurrentLine
{
    $MyInvocation.ScriptLineNumber
}


#---------------------------------------------------------
# Main Entry point
#---------------------------------------------------------
Write-Host "Line $(CurrentLine)  Calling                         main()"
main
Write-Host "Line $(CurrentLine)  All Done  To exit Press Continue"
Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"



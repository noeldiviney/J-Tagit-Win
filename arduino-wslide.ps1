#!C:\PgmFiles\PowerShell\7\pwsh.exe -ExecutionPolicy Bypass
#--------------------------------------------------------------
#    Desktop Shortcut Example
#    W:\ProgramFiles\PowerShell\7\pwsh.exe -ExecutionPolicy Bypass -File W:\DinRDuino\PwrShell\arduino_ide.ps1 W DinRDuino 1.8.15 Eicon BluePill F103C8T6 SerLed V1.0.1 stm32 swd
#
#    Launching the Arduino IDE in a fully configured way.
#    This Pwershell Script uses forward declarations as follows.
#
#    #---------------------------------------------------------
#    # Main Function
#    #---------------------------------------------------------
#    function main
#    {
#        function_1        # Calling function_1
#        function_2        # Calling function_2
#    }
#    #---------------------------------------------------------
#    # Function 1
#    #---------------------------------------------------------
#    function function_1
#    {
#        do something
#    }
#    #---------------------------------------------------------
#    # Function 2
#    #---------------------------------------------------------
#    function function_1
#    {
#        do something else
#    }
#    #---------------------------------------------------------
#    # Main      The script entry point
#    #---------------------------------------------------------
#    main                      # Call function_main
#--------------------------------------------------------------

#---------------------------------------------------------
# Global Parameters
#---------------------------------------------------------
#param([string]$DRIVE = "DRIVE", [string]$FOLDER = "FOLDER", [string]$VERSION = "VERSION", [string]$VENDOR = "VENDOR", [string]$BOARD = "BOARD",
#      [string]$CPU = "CPU", [string]$SKETCH = "SKETCH", [string]$SKETCH_VER = "SKETCH_VER", [string]$ARCHITECTURE = "ARCHITECTURE",
#	  [string]$PROTOCOL = "PROTOCOL" ) ;
Read-Host -Prompt "Pausing:  Press any key to continue"

$DRIVE             = "//wsl$"
$USER              = "$env:UserName"
$UBUNTU_DISTRO     = "Ubuntu-20.04-C1"
$FOLDER            = "DinRDuino"
$VERSION           = "1.8.15"
$VENDOR            = "Eicon"
$BOARD             = "DGPreamp"
$CPU               = "MK22FX512"
$SKETCH            = "DGPSnooze"
$SKETCH_VER        = "V2.0.1"
$ARCHITECTURE      = "nxp"
$PROTOCOL          = "swd"
$COMPUTER_NAME     = "$env:ComputerName"
$UBUNTU_DISTRO     = "Ubuntu-20.04-C1"
$BASE_PATH         = "${DRIVE}/${UBUNTU_DISTRO}/home/${USER}/${FOLDER}"
$ARDUINO_PATH      = "${BASE_PATH}/arduino-$VERSION"
$PORTABLE_PATH     = "${ARDUINO_PATH}/portable"
$PREFS_PATH        = "${PORTABLE_PATH}"
$SKETCH_PATH       = "${PORTABLE_PATH}/sketchbook/arduino"
$SKETCH_NAME       = "${VENDOR}-${BOARD}-${CPU}-${SKETCH}-${SKETCH_VER}"
$SKETCH_PREFS_PATH = "${SKETCH_PATH}/${SKETCH_NAME}/preferences"
$PROFILE_PATH      = "/home/$env:UserName"
$SCRIPT_PATH       = "${DRIVE}:\bin\pwshell"
$OPENOCD_PATH      ="${DRIVE}:\bin\openocd\bin"
#Read-Host -Prompt "Pausing:  Press any key to continue"

#---------------------------------------------------------
# Main Function
#---------------------------------------------------------
function main
{
    Write-Host "Line $(CurrentLine)   Entering               main"
#Read-Host -Prompt 'Input your server  name'

    Write-Host "Line $(CurrentLine)   ----------------------------------------------------------------------------------------"
    Write-Host "Line $(CurrentLine)   Calling                echo_args"
    echo_args                                                                        # Calling echo_args
#Read-Host -Prompt "Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)   Returning from         echo_args"
    Write-Host "Line $(CurrentLine)   ----------------------------------------------------------------------------------------"

    Write-Host "Line $(CurrentLine)   Copy                   ${SKETCH_PREFS_PATH}\${SKETCH_NAME}.txt"
    Write-Host "Line $(CurrentLine)   To                     $PREFS_PATH/preferences.txt"
    Copy-Item ${SKETCH_PREFS_PATH}\${SKETCH_NAME}.txt $PREFS_PATH\preferences.txt
#Read-Host -Prompt "Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)   ----------------------------------------------------------------------------------------"
    Write-Host "Line $(CurrentLine)   Calling                Launch_Arduino_IDE"
    Launch_Arduino_IDE
Read-Host -Prompt "Pausing:  Press any key to continue"
    Write-Host "Line $(CurrentLine)   ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# Echo Args
#---------------------------------------------------------
function echo_args
{
$MyVariable = 1

    Write-Host "Line $(CurrentLine)  Entering               echo_args"
    Write-Host "Line $(CurrentLine)  PWD                  = ${PWD} "
    Write-Host "Line $(CurrentLine)  DRIVE                = ${DRIVE} "
    Write-Host "Line $(CurrentLine)  FOLDER               = ${FOLDER} "
    Write-Host "Line $(CurrentLine)  UBUNTU_DISTRO        = ${UBUNTU_DISTRO} "
    Write-Host "Line $(CurrentLine)  VERSION              = ${VERSION} "
    Write-Host "Line $(CurrentLine)  VENDOR               = ${VENDOR} "
    Write-Host "Line $(CurrentLine)  BOARD                = ${BOARD} "
    Write-Host "Line $(CurrentLine)  CPU                  = ${CPU} "
    Write-Host "Line $(CurrentLine)  SKETCH               = ${SKETCH} "
    Write-Host "Line $(CurrentLine)  SKETCH_VER           = ${SKETCH_VER} "
    Write-Host "Line $(CurrentLine)  ARCHITECTURE         = ${ARCHITECTURE} "
    Write-Host "Line $(CurrentLine)  PROTOCOL             = ${PROTOCOL} "
    Write-Host "Line $(CurrentLine)  USER                 = ${USER} "
    Write-Host "Line $(CurrentLine)  COMPUTER_NAME        = ${COMPUTER_NAME} "
    Write-Host "Line $(CurrentLine)  DRIVE                = $DRIVE "
    Write-Host "Line $(CurrentLine)  FOLDER               = $FOLDER "
    Write-Host "Line $(CurrentLine)  VERSION              = $VERSION "
    Write-Host "Line $(CurrentLine)  VENDOR               = $VENDOR "
    Write-Host "Line $(CurrentLine)  BASE_PATH            = ${BASE_PATH}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_PATH         = ${ARDUINO_PATH}";            
    Write-Host "Line $(CurrentLine)  PREFS_PATH           = ${PREFS_PATH}";            
    Write-Host "Line $(CurrentLine)  SKETCH_NAME          = ${SKETCH_NAME}";            
    Write-Host "Line $(CurrentLine)  SKETCH_PATH          = ${SKETCH_PATH}";            
    Write-Host "Line $(CurrentLine)  SKETCH_PREFS_PATH    = ${SKETCH_PREFS_PATH}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_PATH         = ${ARDUINO_PATH}";            
    Write-Host "Line $(CurrentLine)  SKETCH               = ${SKETCH}";            
    Write-Host "Line $(CurrentLine)  Leaving                echo_args"
}

#---------------------------------------------------------
# Launch_Arduino_IDE
#---------------------------------------------------------
function Launch_Arduino_IDE
{
    Write-Host "Line $(CurrentLine)  Entering               Launch_Arduino_IDE"

    Write-Host "Line $(CurrentLine)  Executing              //wsl$/${UBUNTU_DISTRO}/home/eicon/${FOLDER}/bin/arduino_wslide.ps1"
    & //wsl$/${UBUNTU_DISTRO}/home/eicon/${FOLDER}/bin/arduino_wslide.ps1
#    Write-Host "Line $(CurrentLine)  Executing              $ARDUINO_PATH/arduino "
#Read-Host -Prompt "Pausing:  Press any key to continue"
#    & $ARDUINO_PATH/arduino
#    $ARDUINO_PATH/arduino
    Write-Host "Line $(CurrentLine)  Leaving                Launch_Arduino_IDE"
}

#---------------------------------------------------------
# CurrentLine
#---------------------------------------------------------
function CurrentLine
{
    $MyInvocation.ScriptLineNumber
}


#---------------------------------------------------------
# Main Entry point
#---------------------------------------------------------
Write-Host "Line $(CurrentLine)  Calling                main()"
main

#!C:\Program Files\PowerShell\7\pwsh.exe -ExecutionPolicy Bypass
#--------------------------------------------------------------
#    Desktop Shortcut Example
#    C:\PgmFiles\PowerShell\7\pwsh.exe -ExecutionPolicy Bypass -File W:\DinRDuino\PwrShell\arduino_ide.ps1 W DinRDuino 1.8.15 Eicon BluePill F103C8T6 SerLed V1.0.1 stm32 swd
#
#    Launching the Arduino IDE in a fully configured way.
#    This Pwershell Script uses forward declarations as follows.
#
#    #---------------------------------------------------------
#    # Main Function
#    #---------------------------------------------------------
#    function main
#    {
#        function_1        # Calling function_1
#        function_2        # Calling function_2
#    }
#    #---------------------------------------------------------
#    # Function 1
#    #---------------------------------------------------------
#    function function_1
#    {
#        do something
#    }
#    #---------------------------------------------------------
#    # Function 2
#    #---------------------------------------------------------
#    function function_1
#    {
#        do something else
#    }
#    #---------------------------------------------------------
#    # Main      The script entry point
#    #---------------------------------------------------------
#    main                      # Call function_main
#--------------------------------------------------------------

#---------------------------------------------------------
# Global Parameters
#---------------------------------------------------------

## param([string]$DRIVE  = "DRIVE", 
##       [string]$FOLDER = "FOLDER",
##       [string]$DISTRO = "DISTRO",
##       [string]$VENDOR = "VENDOR");

#Read-Host -Prompt "Pausing:  Press any key to continue"

$OS                        = "WIN"
$DRIVE                     = "C"
$INSTALL_FOLDER            = "DinRDuino"
$SLASH                     = "\"
$BASE_ROOT                 = "${DRIVE}:${SLASH}"
$BASE_PATH                 = "${DRIVE}:${SLASH}${INSTALL_FOLDER}"
$DLOAD_PATH                = "${DRIVE}:${SLASH}${INSTALL_FOLDER}${SLASH}dload"
$INSTALL_FOLDER            = "DinRDuino"
$ARDUINO_VERSION           = "1.8.18"
$USER_NAME                 = "eicon"
$7ZIP_URI                  = "https://www.7-zip.org/a/7z2106-x64.exe"
$7ZIP_EXE                  = "7z2106-x64.exe"
#$VCXSRV_URI                = "https://sourceforge.net/projects/vcxsrv/files/vcxsrv/1.20.14.0/vcxsrv-64.1.20.14.0.installer.exe/download"
$VCXSRV_URI                = "https://sourceforge.net/projects/vcxsrv/files/vcxsrv/1.20.14.0/vcxsrv-64.1.20.14.0.installer.exe"
$VCXSRV_EXE                = "vcxsrv-64.1.20.14.0.installer.exe"
$POWERSHELL_URI            = "https://github.com/PowerShell/PowerShell/releases/download/v7.2.1/PowerShell-7.2.1-win-x64.msi"
$POWERSHELL_MSI            = "PowerShell-7.2.1-win-x64.msi"
$SHASUM_URI                = "http://www.labtestproject.com/files/sha256sum/sha256sum.exe"
$SHASUM_EXE                = "sha256sum.exe"
$CDM_SETUP_URI             = "https://ftdichip.com/wp-content/uploads/2021/08/CDM212364_Setup.zip"
$CDM_SETUP_ZIP             = "CDM212364_Setup.zip"
$ZADIG_URI                 = "https://github.com/pbatard/libwdi/releases/download/v1.4.1/zadig-2.7.exe"
$ZADIG_EXE                 = "zadig-2.7.exe"
$DESKTOPOK_URI             = "https://www.softwareok.com/Download/DesktopOK_x64.zip"
$DESKTOPOK_ZIP             = "DesktopOK_x64.zip"
$USER                      = "$env:UserName"                                 # $env:VAR_NAME="VALUE"
$PROFILE_PATH              = "/home/$env:USER"
$NOTEPAD_EDITOR            = "C:\Program Files\Notepad++\notepad++.exe"
#Read-Host -Prompt "Pausing:  Press any key to continue"
#---------------------------------------------------------
# Main Function
#---------------------------------------------------------
function main
{
    Write-Host "Line $(CurrentLine)   Entering                     main";    

    Write-Host "Line $(CurrentLine)   Calling                      echo_args";
    echo_args                                                                        # Calling echo_args
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)   Calling                      create_base_path";
    create_base_path
#Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)   Calling                      install_7-Zip";
    install_7-Zip
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)   Calling                      install_windows_terminal";
    install_windows_terminal
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)   Calling                      install_VcXsrv";
    install_VcXsrv
#Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      install_notepad++";
    install_notepad++
#Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      install_cmake";
    install_cmake
#Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      install_make";
    install_make
#Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      install_sha256sum";
    install_sha256sum
#Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      install_cdm_setup";
    install_cdm_setup
#Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      install_zadig";
    install_zadig
#Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      install_desktopOK_x64";
    install_desktopOK_x64
#Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Updating                     PATH Env Variable";
    changePath "remove" "C:\Program Files\Powershell\7\"
    changePath "remove" "C:\Program Files\Git\cmd\"
    changePath "remove" "C:\Program Files\Notepad++\"
    changePath "remove" "C:\Program Files\\ShaSUM\"
    changePath "remove" "C:\Program Files\DinRDuino\bin\"
    changePath "remove" "C:\bin"
    changePath "add" "C:\Program Files\Powershell\7\"
    changePath "add" "C:\Program Files\Git\cmd\"
    changePath "add" "C:\Program Files\Notepad++\"
    changePath "add" "C:\Program Files\\ShaSUM\"
    changePath "add" "C:\Program Files\DinRDuino\bin\"
    changePath "add" "C:\bin"

    Write-Host "Line $(CurrentLine)  Calling                      install_desktop";
    install_desktop
#Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)   ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# install_desktop
#---------------------------------------------------------
function install_desktop
{
    Write-Host "Line $(CurrentLine)  Entering                     install_desktop";

    Write-Host "Line $(CurrentLine)  Executing                    Copy-Item -Path C:\bin\desktop\* -Destination   C:\Users\eicon\Desktop" 
    Copy-Item -Path C:\bin\desktop\* -Destination   C:\Users\eicon\Desktop
    Write-Host "Line $(CurrentLine)  Executing                    Copy-Item -Path C:\bin\StartMenu\* -Destination   "C:\Users\eicon\AppData\Roaming\Microsoft\Windows\Start Menu\Programs"" 
    Copy-Item -Path C:\bin\StartMenu\* -Destination   "C:\Users\eicon\AppData\Roaming\Microsoft\Windows\Start Menu\Programs"
    Write-Host "Line $(CurrentLine)  Executing                    Copy-Item -Path C:\bin\DesktopOK\DesktopOK.ini -Destination  ( New-Item -Path  "C:\Users\eicon\AppData\Roaming\DesktopOK\DesktopOK.ini" -Force )" 
    Copy-Item -Path C:\bin\DesktopOK\DesktopOK.ini -Destination  ( New-Item -Path  "C:\Users\eicon\AppData\Roaming\DesktopOK\DesktopOK.ini" -Force )
    Write-Host "Line $(CurrentLine)  Executing                    "C:\Program Files\DesktopOK_x64\DesktopOK_x64.exe"" 
    & "C:\Program Files\DesktopOK_x64\DesktopOK_x64.exe"

    cd ${BASE_PATH}
    Write-Host "Line $(CurrentLine)  Current Directory          = $PWD";

    Write-Host "Line $(CurrentLine)  Leaving                      install_desktop";
}


#[System.Environment]::GetEnvironmentVariable('PATH','machine')
#---------------------------------------------------------
# setup_env_variables
#---------------------------------------------------------
function setup_env_variables
{
    Write-Host "Line $(CurrentLine)   Entering                     setup_env_variables";

    [Environment]::SetEnvironmentVariable(
        "Path",
        [Environment]::GetEnvironmentVariable("Path", 
		[EnvironmentVariableTarget]::Machine) + 
		";C:\fred;C:\fred2",
        [EnvironmentVariableTarget]::Machine)

    Write-Host "Line $(CurrentLine)   Leaving                      setup_env_variables";
}


#---------------------------------------------------------
# install_desktopOK_x64
#---------------------------------------------------------
function install_desktopOK_x64
{
    Write-Host "Line $(CurrentLine)  Entering                     install_desktopOK_x64";

    if (Test-Path ${BASE_PATH}${SLASH}dload${SLASH}${DESKTOPOK_ZIP})
    {
        Write-Host "Line $(CurrentLine)  desktopOK_x64                Exists ... continuing";
    }
    else
    {
        Write-Host "Line $(CurrentLine)  desktopOK_x64                Does not Exist ... downloading";
        Write-Host "Line $(CurrentLine)  Executing                    Invoke-WebRequest -Uri ${DESKTOPOK_URI} -Outfile ${BASE_PATH}${SLASH}dload${SLASH}${DESKTOPOK_ZIP}";
        Invoke-WebRequest -Uri ${DESKTOPOK_URI} -Outfile ${BASE_PATH}${SLASH}dload${SLASH}${DESKTOPOK_ZIP}	
Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    }
    Write-Host "Line $(CurrentLine)  Executing                    Expand-Archive -Path ${BASE_PATH}${SLASH}dload${SLASH}${DESKTOPOK_ZIP} -DestinationPath "C:\Program Files\DesktopOK_x64" -Force -Verbose";
    Expand-Archive -Path ${BASE_PATH}${SLASH}dload${SLASH}${DESKTOPOK_ZIP} -DestinationPath "C:\Program Files\DesktopOK_x64" -Force -Verbose
    Write-Host "Line $(CurrentLine)  Leaving                      install_desktopOK_x64";
}

#---------------------------------------------------------
# install_zadig
#---------------------------------------------------------
function install_zadig
{
    Write-Host "Line $(CurrentLine)  Entering                     install_zadig";

    if (Test-Path ${BASE_PATH}${SLASH}dload${SLASH}${ZADIG_EXE})
    {
        Write-Host "Line $(CurrentLine)  zadig                        Exists ... continuing";
    }
    else
    {
        Write-Host "Line $(CurrentLine)  zadig                        Does not Exist ... downloading";
        Write-Host "Line $(CurrentLine)  Executing                    Invoke-WebRequest -Uri ${ZADIG_URI} -Outfile ${BASE_PATH}${SLASH}dload${SLASH}${ZADIG_EXE}";
        Invoke-WebRequest -Uri ${ZADIG_URI} -Outfile ${BASE_PATH}${SLASH}dload${SLASH}${ZADIG_EXE}	
    }
    Write-Host "Line $(CurrentLine)  Executing                    Copy-Item -Path ${BASE_PATH}${SLASH}dload${SLASH}${ZADIG_EXE} -Destination ( New-Item -Path "C:\Program Files\Zadig\${ZADIG_EXE}" -Force ) ";
    Copy-Item -Path ${BASE_PATH}${SLASH}dload${SLASH}${ZADIG_EXE} -Destination ( New-Item -Path "C:\Program Files\Zadig\${ZADIG_EXE}" -Force ) 

    Write-Host "Line $(CurrentLine)  Leaving                      install_zadig";
}

#---------------------------------------------------------
# install_cdm_setup
#---------------------------------------------------------
function install_cdm_setup
{
    Write-Host "Line $(CurrentLine)  Entering                     install_cdm_setup";

    if (Test-Path ${BASE_PATH}${SLASH}dload${SLASH}${CDM_SETUP_ZIP})
    {
        Write-Host "Line $(CurrentLine)  cdm_setup                    Exists ... continuing";
    }
    else
    {
        Write-Host "Line $(CurrentLine)  cdm_setup                    Does not Exist ... downloading";
        Write-Host "Line $(CurrentLine)  Executing                    Invoke-WebRequest -Uri ${CDM_SETUP_URI} -Outfile "${BASE_PATH}${SLASH}dload${SLASH}${CDM_SETUP_ZIP}"";
        Invoke-WebRequest -Uri ${CDM_SETUP_URI} -Outfile ${BASE_PATH}${SLASH}dload${SLASH}${CDM_SETUP_ZIP}	
    }
    Write-Host "Line $(CurrentLine)  Executing                    Expand-Archive -Path ${BASE_PATH}${SLASH}dload${SLASH}${CDM_SETUP_ZIP} -DestinationPath "${BASE_PATH}${SLASH}dload${SLASH}FTDI${SLASH}CDM212364_Setup" -Force -Verbose";
    Expand-Archive -Path ${BASE_PATH}${SLASH}dload${SLASH}${CDM_SETUP_ZIP} -DestinationPath "${BASE_PATH}${SLASH}dload${SLASH}FTDI${SLASH}CDM212364_Setup" -Force -Verbose

    Write-Host "Line $(CurrentLine)  Executing                    Copy-Item -Path ${BASE_PATH}${SLASH}dload${SLASH}FTDI${SLASH}CDM212364_Setup${SLASH}CDM212364_Setup.exe -Destination ( New-Item -Path "C:\Program Files${SLASH}FTDI${SLASH}CDM212364_Setup${SLASH}CDM212364_Setup.exe" -Force )";                     
    Copy-Item -Path ${BASE_PATH}${SLASH}dload${SLASH}FTDI${SLASH}CDM212364_Setup${SLASH}CDM212364_Setup.exe -Destination ( New-Item -Path "C:\Program Files${SLASH}FTDI${SLASH}CDM212364_Setup${SLASH}CDM212364_Setup.exe" -Force ) 

    Write-Host "Line $(CurrentLine)  Leaving                      install_cdm_setup";
}

#---------------------------------------------------------
# install_sha256sum
#---------------------------------------------------------
function install_sha256sum
{
    Write-Host "Line $(CurrentLine)  Entering                     install_sha256sum";

    if (Test-Path ${BASE_PATH}${SLASH}dload${SLASH}${SHASUM_EXE})
    {
        Write-Host "Line $(CurrentLine)  sha256sum                    Exists ... continuing";
    }
    else
    {
        Write-Host "Line $(CurrentLine)  sha256sum                    Does not Exist ... downloading";
        Write-Host "Line $(CurrentLine)  Executing                    Invoke-WebRequest -Uri ${SHASUM_URI} -Outfile ${BASE_PATH}${SLASH}dload${SLASH}${SHASUM_EXE}";
        Invoke-WebRequest -Uri ${SHASUM_URI} -Outfile ${BASE_PATH}${SLASH}dload${SLASH}${SHASUM_EXE}	
    }
    Write-Host "Line $(CurrentLine)  Executing                    Copy-Item -Path ${BASE_PATH}${SLASH}dload${SLASH}${SHASUM_EXE} -Destination ( New-Item -Path "C:\Program Files\ShaSUM\${SHASUM_EXE}" -Force )";
    Copy-Item -Path ${BASE_PATH}${SLASH}dload${SLASH}${SHASUM_EXE} -Destination ( New-Item -Path "C:\Program Files\ShaSUM\${SHASUM_EXE}" -Force ) 

    Write-Host "Line $(CurrentLine)  Leaving                      install_sha256sum";
}

#---------------------------------------------------------
# install_PowerShell
#---------------------------------------------------------
function install_PowerShell
{
    Write-Host "Line $(CurrentLine)   Entering                     install_PowerShell";

    if (Test-Path ${BASE_PATH}${SLASH}dload${SLASH}${POWERSHELL_MSI})
    {
        Write-Host "Line $(CurrentLine)  PowerShell                   Exists ... continuing";
    }
    else
    {
        Write-Host "Line $(CurrentLine)  PowerShell                   Does not Exist ... downloading";
        Write-Host "Line $(CurrentLine)  Executing                    Invoke-WebRequest -Uri ${POWERSHELL_URI} -Outfile ${BASE_PATH}${SLASH}dload${SLASH}${POWERSHELL_MSI}";
        Invoke-WebRequest -Uri ${POWERSHELL_URI} -Outfile ${BASE_PATH}${SLASH}dload${SLASH}${POWERSHELL_MSI}	
    }
    Write-Host "Line $(CurrentLine)  Installing                    ${BASE_PATH}${SLASH}dload${SLASH}${POWERSHELL_MSI}";
#    & ${BASE_PATH}${SLASH}dload${SLASH}${POWERSHELL_MSI}
    msiexec /i "${BASE_PATH}${SLASH}dload${SLASH}${POWERSHELL_MSI}"
Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)   Leaving                      install_PowerShell";
}

#---------------------------------------------------------
# install_make
#---------------------------------------------------------
function install_make
{
    Write-Host "Line $(CurrentLine)  Entering                     install_make";

    Write-Host "Line $(CurrentLine)  Installing                   winget install --id=GnuWin32.make -e";
    & winget install --id=GnuWin32.make -e

    Write-Host "Line $(CurrentLine)  Leaving                      install_make";
}

#---------------------------------------------------------
# install_cmake
#---------------------------------------------------------
function install_cmake
{
    Write-Host "Line $(CurrentLine)  Entering                     install_cmake";

    Write-Host "Line $(CurrentLine)  Installing                   winget install cmake";
    & winget install cmake

    Write-Host "Line $(CurrentLine)  Leaving                      install_cmake";
}

#---------------------------------------------------------
# install_notepad++
#---------------------------------------------------------
function install_notepad++
{
    Write-Host "Line $(CurrentLine)  Entering                     install_notepad++";

    Write-Host "Line $(CurrentLine)  Installing                   winget install --id=Notepad++.Notepad++ -e";
    & winget install --id=Notepad++.Notepad++ -e

    Write-Host "Line $(CurrentLine)  Leaving                      install_notepad++";
}


#---------------------------------------------------------
# install_VcXsrv
#---------------------------------------------------------
function install_VcXsrv
{
    Write-Host "Line $(CurrentLine)  Entering                     install_VcXsrv";

    Write-Host "Line $(CurrentLine)  Installing                   winget install vcxsrv";
    & winget install vcxsrv

    Write-Host "Line $(CurrentLine)  Leaving                      install_VcXsrv";
}

#---------------------------------------------------------
# install_windows_terminal
#---------------------------------------------------------
function install_windows_terminal
{
    Write-Host "Line $(CurrentLine)   Entering                     install_windows_terminal";

    Write-Host "Line $(CurrentLine)  Installing                    winget install --id=Microsoft.WindowsTerminal -e";
    & winget install --id=Microsoft.WindowsTerminal -e

    Write-Host "Line $(CurrentLine)   Leaving                      install_windows_terminal";
}

#---------------------------------------------------------
# install_7-Zip
#---------------------------------------------------------
function install_7-Zip
{
    Write-Host "Line $(CurrentLine)  Entering                     install_7-Zip";

    if (Test-Path ${BASE_PATH}${SLASH}dload${SLASH}${7ZIP_EXE})
    {
        Write-Host "Line $(CurrentLine)  7-Zip                        Exists ... continuing";
    }
    else
    {
        Write-Host "Line $(CurrentLine)  7-Zip                        Does not Exist ... downloading";
        Write-Host "Line $(CurrentLine)  Executing                    Invoke-WebRequest -Uri ${7ZIP_URI} -Outfile ( New-Item -Path "${BASE_PATH}${SLASH}dload${SLASH}${7ZIP_EXE}" -Force )";
        Invoke-WebRequest -Uri ${7ZIP_URI} -Outfile ( New-Item -Path "${BASE_PATH}${SLASH}dload${SLASH}${7ZIP_EXE}" -Force )	
    }
    Write-Host "Line $(CurrentLine)  Installing                   ${BASE_PATH}${SLASH}dload${SLASH}${7ZIP_EXE}";
    & ${BASE_PATH}${SLASH}dload${SLASH}${7ZIP_EXE}

    Write-Host "Line $(CurrentLine)  Leaving                      install_7-Zip";
}
##Invoke-WebRequest -uri 'https://google.de/' -OutFile ( New-Item -Path "C:\AMD\out\out.log" -Force )

#---------------------------------------------------------
# create_dload_path
#---------------------------------------------------------
function create_dload_path
{
    Write-Host "Line $(CurrentLine)  Entering                     create_dload_path";
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${DLOAD_PATH})";
    if (Test-Path ${DLOAD_PATH})
    {
        Write-Host "Line $(CurrentLine)  ${DLOAD_PATH}             Exists" ;  
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Creating                     ${DLOAD_PATH}";
        Write-Host "Line $(CurrentLine)  Executing                    md ${DLOAD_PATH}";
        Write-Host "Line $(CurrentLine)   ----------------------------------------------------------------------------------------";
        md ${DLOAD_PATH}
        Write-Host "Line $(CurrentLine)   ----------------------------------------------------------------------------------------";
    }
    cd ..
    Write-Host "Line $(CurrentLine)  DLOAD_PATH                 = ${DLOAD_PATH}";
    Write-Host "Line $(CurrentLine)  Leaving                      create_dload_path";
}

#---------------------------------------------------------
# create_base_path
#---------------------------------------------------------
function create_base_path
{
    Write-Host "Line $(CurrentLine)  Entering                     create_base_path";
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${BASE_PATH})";
    if (Test-Path ${BASE_PATH})
    {
        Write-Host "Line $(CurrentLine)  ${BASE_PATH}                 Exists" ;  
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Creating                     ${BASE_PATH}";
        Write-Host "Line $(CurrentLine)  Executing                    md ${BASE_PATH}";
        Write-Host "Line $(CurrentLine)   ----------------------------------------------------------------------------------------";
        md ${BASE_PATH}
        Write-Host "Line $(CurrentLine)   ----------------------------------------------------------------------------------------";
    }
    cd ${BASE_PATH}
    Write-Host "Line $(CurrentLine)  Current Directory          = $PWD";
    Write-Host "Line $(CurrentLine)  Leaving                      create_${BASE_PATH}";
}

#---------------------------------------------------------
# Echo Args
#---------------------------------------------------------
function echo_args
{
$MyVariable = 1

    Write-Host "Line $(CurrentLine)  Entering                     echo_args";
    Write-Host "Line $(CurrentLine)  Arguments                    as follows";
#    Write-Host "Line $(CurrentLine)  DRIVE                      = ${DRIVE} ";
#    Write-Host "Line $(CurrentLine)  FOLDER                     = ${FOLDER} ";
#    Write-Host "Line $(CurrentLine)  DISTRO                     = ${DISTRO} ";
#    Write-Host "Line $(CurrentLine)  VENDOR                     = ${VENDOR} ";
    Write-Host "Line $(CurrentLine)  End of Arguments             ";

    Write-Host "Line $(CurrentLine)  Operating System           = ${OS}";
    Write-Host "Line $(CurrentLine)  DRIVE                      = ${DRIVE}";
    Write-Host "Line $(CurrentLine)  SLASH                      = ${SLASH}";
    Write-Host "Line $(CurrentLine)  BASE_ROOT                  = ${BASE_ROOT}";
    Write-Host "Line $(CurrentLine)  INSTALL_FOLDER             = ${INSTALL_FOLDER}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_VERSION            = ${ARDUINO_VERSION}";
    Write-Host "Line $(CurrentLine)  USER_NAME                  = ${USER_NAME}";
    Write-Host "Line $(CurrentLine)  BASE_PATH                  = ${BASE_PATH}";            
    Write-Host "Line $(CurrentLine)  DLOAD_PATH                 = ${DLOAD_PATH}";            
    Write-Host "Line $(CurrentLine)  USER                       = ${USER}";            
    Write-Host "Line $(CurrentLine)  Leaving                      echo_args";
}

# Modify PATH variable
function changePath ($action, $addendum) {
    $regLocation = 
    "Registry::HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Session Manager\Environment"
    $path = (Get-ItemProperty -Path $regLocation -Name PATH).path

    # Add an item to PATH
    if ($action -eq "add") {
        $path = "$path;$addendum"
        Set-ItemProperty -Path $regLocation -Name PATH -Value $path
    }

    # Remove an item from PATH
    if ($action -eq "remove") {
        $path = ($path.Split(';') | Where-Object { $_ -ne "$addendum" }) -join ';'
        Set-ItemProperty -Path $regLocation -Name PATH -Value $path
    }
}

##  Add an item to your path
##  changePath "add" "C:\example"

##  Remove an item from your path
##  changePath "remove" "C:\example"


#---------------------------------------------------------
# CurrentLine
#---------------------------------------------------------
function CurrentLine
{
    $MyInvocation.ScriptLineNumber
}


#---------------------------------------------------------
# Main Entry point
#---------------------------------------------------------
Write-Host "Line $(CurrentLine)  Calling                      main()"
main
Write-Host "Line $(CurrentLine)  All Done  To exit Press Continue"
Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"


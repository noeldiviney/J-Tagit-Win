#!C:\PgmFiles\PowerShell\7\pwsh.exe -ExecutionPolicy Bypass
#--------------------------------------------------------------
#    Desktop Shortcut Example
#    C:\PgmFiles\PowerShell\7\pwsh.exe -ExecutionPolicy Bypass -File W:\DinRDuino\PwrShell\arduino_ide.ps1 W DinRDuino 1.8.15 Eicon BluePill F103C8T6 SerLed V1.0.1 stm32 swd
#
#    Launching the Arduino IDE in a fully configured way.
#    This Pwershell Script uses forward declarations as follows.
#
#    #---------------------------------------------------------
#    # Main Function
#    #---------------------------------------------------------
#    function main
#    {
#        function_1        # Calling function_1
#        function_2        # Calling function_2
#    }
#    #---------------------------------------------------------
#    # Function 1
#    #---------------------------------------------------------
#    function function_1
#    {
#        do something
#    }
#    #---------------------------------------------------------
#    # Function 2
#    #---------------------------------------------------------
#    function function_1
#    {
#        do something else
#    }
#    #---------------------------------------------------------
#    # Main      The script entry point
#    #---------------------------------------------------------
#    main                      # Call function_main
#--------------------------------------------------------------

#---------------------------------------------------------
# Global Parameters
#---------------------------------------------------------

param([string]$DRIVE           = "DRIVE",
      [string]$FOLDER          = "FOLDER",
      [string]$DISTRO_MASTER   = "DISTRO_MASTER",
      [string]$USER_NAME       = "USER_NAME",
      [string]$DISTRO_NEW      = "DISTRO_NEW" );


$VENDOR_LC                 =  ${VENDOR}.ToLower()
$VHD_MASTER                = "${DISTRO_MASTER}"
$VHD_NEW                   = "${DISTRO_MASTER}-${DISTRO_NEW}"
$BIN_PATH                  = "${DRIVE}:\bin"
$BASE_PATH                 = "${DRIVE}:\${FOLDER}"
$VHD_NEW_PATH              = "${BASE_PATH}\WSL\${VHD_NEW}"
$VHD_MASTER_TAR            = "${DISTRO_MASTER}.tar"
$USER                      = "$env:UserName"                                 # $env:VAR_NAME="VALUE"
$PROFILE_PATH              = "/home/$env:USER"
$NOTEPAD_EDITOR            = "C:\Program Files\Notepad++\notepad++.exe"
$WSL_CMD_NEW               = "   start %windir%\System32\cmd.exe /K wsl.exe -d ${VHD_NEW} -u ${VENDOR_LC}"
$WSL_CMD_MASTER            = "   start %windir%\System32\cmd.exe /K wsl.exe -d ${VHD_MASTER} -u ${VENDOR_LC}"

#Read-Host -Prompt "Pausing:  Press any key to continue"

#---------------------------------------------------------
# Main Function
#---------------------------------------------------------
function main
{
    Write-Host "Line $(CurrentLine)   Entering                     main";    
    
    Write-Host "Line $(CurrentLine)   Calling                      echo_args";
    echo_args                                                                        # Calling echo_args
Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)   Calling                      create_base_path";
    create_base_path
Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)   Calling                      configure_bashrc";
    configure_bashrc
Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)   Calling                      install_ubuntu";
    install_ubuntu
Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)   Calling                      create_ubuntu_new.bat";
    create_ubuntu_new.bat
Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)   Calling                      create_ubuntu_master.bat";
    create_ubuntu_master.bat
Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    
    Write-Host "Line $(CurrentLine)   ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# create_ubuntu_master.bat
#---------------------------------------------------------
function create_ubuntu_master.bat
{
    Write-Host "Line $(CurrentLine)   Entering                     create_ubuntu_master.bat";
    Write-Host "Line $(CurrentLine)   Executing                    if (Test-Path -Path ${BIN_PATH} ) ";
    if (Test-Path -Path ${BIN_PATH})
    {
        Write-Host "Line $(CurrentLine)   The path                     ${BIN_PATH} exists ... continuing  ";
    }
    else
    {
        Write-Host "Line $(CurrentLine)   Creating             ${BIN_PATH}";
        md ${BIN_PATH}
    }
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path -Path ${BIN_PATH}\${VHD_MASTER}.bat -PathType Leaf)  ";
    if (Test-Path -Path ${BIN_PATH}\${VHD_MASTER}.bat -PathType Leaf)
    {
        Write-Host "Line $(CurrentLine)  Batch file                   ${VHD_MASTER}.bat exists ... deleting  ";
        Remove-Item ${BIN_PATH}\${VHD_MASTER}.bat
        Write-Host "Line $(CurrentLine)  Executing                    New-Item ${BIN_PATH}\${VHD_MASTER}.bat";
        Write-Host "Line $(CurrentLine)  cmd output follows ---------------------------------------------------------------------";
        New-Item ${BIN_PATH}\${VHD_MASTER}.bat
        Write-Host "Line $(CurrentLine)  cmd output ends ------------------------------------------------------------------------";
    }
    else
    {
        Write-Host "Line $(CurrentLine)   Creating                     ${VHD_MASTER}.bat ";
        Write-Host "Line $(CurrentLine)  Executing                    New-Item ${BIN_PATH}\${VHD_MASTER}.bat";
        New-Item ${BIN_PATH}\${VHD_MASTER}.bat
    }
    Write-Host "Line $(CurrentLine)  Adding                       ${VHD_MASTER}.bat Contents";
    Add-Content ${BIN_PATH}\${VHD_MASTER}.bat '@echo off'
    Add-Content ${BIN_PATH}\${VHD_MASTER}.bat 'SET MyProcess=VcXsrv.exe'
    Add-Content ${BIN_PATH}\${VHD_MASTER}.bat 'ECHO "Test if %MyProcess% is running"'
    Add-Content ${BIN_PATH}\${VHD_MASTER}.bat 'TASKLIST | FINDSTR /I "%MyProcess%">nul'
    Add-Content ${BIN_PATH}\${VHD_MASTER}.bat 'if %errorlevel% neq 0 ('
    Add-Content ${BIN_PATH}\${VHD_MASTER}.bat '   ECHO "%MyProcess% is not running"'
    Add-Content ${BIN_PATH}\${VHD_MASTER}.bat '   start C:\"Program Files"\VcXsrv\vcxsrv.exe -ac -terminate -lesspointer -multiwindow -clipboard -wgl'
    Add-Content ${BIN_PATH}\${VHD_MASTER}.bat ${WSL_CMD_MASTER}
    Add-Content ${BIN_PATH}\${VHD_MASTER}.bat '   EXIT /B'
    Add-Content ${BIN_PATH}\${VHD_MASTER}.bat ') ELSE ('
    Add-Content ${BIN_PATH}\${VHD_MASTER}.bat '   ECHO "%MyProcess%" is running'
    Add-Content ${BIN_PATH}\${VHD_MASTER}.bat ${WSL_CMD_MASTER}
    Add-Content ${BIN_PATH}\${VHD_MASTER}.bat '   EXIT /B'
    Add-Content ${BIN_PATH}\${VHD_MASTER}.bat ')'	
    Write-Host "Line $(CurrentLine)  Leaving                      create_ubuntu_master.bat";
}

#---------------------------------------------------------
# create_ubuntu_new.bat
#---------------------------------------------------------
function create_ubuntu_new.bat
{
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------";
    Write-Host "Line $(CurrentLine)  Entering                     create_ubuntu_new.bat";
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path -Path ${BIN_PATH} ";
    if (Test-Path -Path ${BIN_PATH} )
    {
        Write-Host "Line $(CurrentLine)  The path                     ${BIN_PATH}t exists ... continuing  ";
    }
    else
    {
        Write-Host "Line $(CurrentLine)  The path                     ${BIN_PATH} does not exist ... creating  ";
        Write-Host "Line $(CurrentLine)  Creating                     ${BIN_PATH}";
        md ${BIN_PATH}
    }
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path -Path ${BIN_PATH}\${VHD_NEW}.bat -PathType Leaf) ";
    if (Test-Path -Path ${BIN_PATH}\${VHD_NEW}.bat -PathType Leaf)
    {
        Write-Host "Line $(CurrentLine)  Batch file               ${VHD_NEW}.bat exists ... deleting  ";
        Remove-Item ${BIN_PATH}\${VHD_NEW}.bat
        Write-Host "Line $(CurrentLine)  Executing                    New-Item ${BIN_PATH}\${VHD_NEW}.bat";
        Write-Host "Line $(CurrentLine)  cmd output follows ---------------------------------------------------------------------";
        New-Item ${BIN_PATH}\${VHD_NEW}.bat
        Write-Host "Line $(CurrentLine)  cmd output ends ------------------------------------------------------------------------";
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Creating                     ${VHD_NEW}.bat ";
        Write-Host "Line $(CurrentLine)  Executing                    New-Item ${BIN_PATH}\${VHD_NEW}.bat";
        Write-Host "Line $(CurrentLine)  cmd output follows ---------------------------------------------------------------------";
        New-Item ${BIN_PATH}\${VHD_NEW}.bat
        Write-Host "Line $(CurrentLine)  cmd output ends ------------------------------------------------------------------------";
    }
    Write-Host "Line $(CurrentLine)  Adding                       ${VHD_NEW}.bat Contents";
    Add-Content ${BIN_PATH}\${VHD_NEW}.bat '@echo off'
    Add-Content ${BIN_PATH}\${VHD_NEW}.bat 'SET MyProcess=VcXsrv.exe'
    Add-Content ${BIN_PATH}\${VHD_NEW}.bat 'ECHO "Test if %MyProcess% is running"'
    Add-Content ${BIN_PATH}\${VHD_NEW}.bat 'TASKLIST | FINDSTR /I "%MyProcess%">nul'
    Add-Content ${BIN_PATH}\${VHD_NEW}.bat 'if %errorlevel% neq 0 ('
    Add-Content ${BIN_PATH}\${VHD_NEW}.bat '   ECHO "%MyProcess% is not running"'
    Add-Content ${BIN_PATH}\${VHD_NEW}.bat '   start C:\"Program Files"\VcXsrv\vcxsrv.exe -ac -terminate -lesspointer -multiwindow -clipboard -wgl'
    Add-Content ${BIN_PATH}\${VHD_NEW}.bat ${WSL_CMD_NEW}
    Add-Content ${BIN_PATH}\${VHD_NEW}.bat '   EXIT /B'
    Add-Content ${BIN_PATH}\${VHD_NEW}.bat ') ELSE ('
    Add-Content ${BIN_PATH}\${VHD_NEW}.bat '   ECHO "%MyProcess%" is running'
    Add-Content ${BIN_PATH}\${VHD_NEW}.bat ${WSL_CMD_NEW}
    Add-Content ${BIN_PATH}\${VHD_NEW}.bat '   EXIT /B'
    Add-Content ${BIN_PATH}\${VHD_NEW}.bat ')'	
    Write-Host "Line $(CurrentLine)  Leaving                      create_ubuntu_new.bat";
    Write-Host "Line $(CurrentLine)   ----------------------------------------------------------------------------------------";
}
#---------------------------------------------------------
# install_ubuntu
#---------------------------------------------------------
function install_ubuntu
{
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------";
    Write-Host "Line $(CurrentLine)  Entering                     install_ubuntu";
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path -Path ${BASE_PATH}\WSL)  ";
	if (Test-Path -Path ${BASE_PATH}\WSL)
    {
	    Write-Host "Line $(CurrentLine)  WSL folder                   exists ... continuing ";
    }
    else
    {	
	    Write-Host "Line $(CurrentLine)  WSL folder                   does notexist creating ... ";
        Write-Host "Line $(CurrentLine)  Executing                    mkdir -p ${BASE_PATH}\WSL  ";
        mkdir -p ${BASE_PATH}\WSL 
		
    }
Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path -Path ${BASE_PATH}\WSL\${VHD_MASTER_TAR} -PathType Leaf) ";
	if (Test-Path -Path ${BASE_PATH}\WSL\${VHD_MASTER_TAR} -PathType Leaf)
	{
	    Write-Host "Line $(CurrentLine)  The Tar                      ${VHD_MASTER_TAR}  exists ... continuing ";
    }
    else
    {
	    Write-Host "Line $(CurrentLine)  The Tar                      ${VHD_MASTER_TAR}  does not exist ... creating ";
	    Write-Host "Line $(CurrentLine)  Executing                    wsl --export ${DISTRO_MASTER} ${BASE_PATH}\WSL\${VHD_MASTER_TAR} ";
	    wsl --export ${DISTRO_MASTER} ${BASE_PATH}\WSL\${VHD_MASTER_TAR} 
    }
	Write-Host "Line $(CurrentLine)  Creating                     ${VHD_NEW_PATH} ";
	
Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"
	Write-Host "Line $(CurrentLine)  Executing                    wsl --unregister ${VHD_NEW} ";
    Write-Host "Line $(CurrentLine)  cmd output follows ---------------------------------------------------------------------";
    wsl --unregister ${VHD_NEW}			
    Write-Host "Line $(CurrentLine)  cmd output ends ------------------------------------------------------------------------";
Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"
	if (Test-Path -Path ${VHD_NEW_PATH} )
	{
        Write-Host "Line $(CurrentLine)  Distro                       #{VHD_NEW_PATH} Exists ... deleting ";
        Write-Host "Line $(CurrentLine)  Executing                    Remove-Item ${VHD_NEW_PATH} -Recurse -Force ";
        Remove-Item ${VHD_NEW_PATH} -Recurse -Force
	}
Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Executing                    wsl --import ${VHD_NEW} ${VHD_NEW_PATH} ${BASE_PATH}\WSL\${VHD_MASTER_TAR}  ";
	wsl --import ${VHD_NEW} ${VHD_NEW_PATH} ${BASE_PATH}\WSL\${VHD_MASTER_TAR} 
    Write-Host "Line $(CurrentLine)  Leaving                      install_ubuntu";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------";
}

#---------------------------------------------------------
# configure_bashrc
#---------------------------------------------------------
function configure_bashrc 
{
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------";
    Write-Host "Line $(CurrentLine)  Entering                     configure_bashrc";
    Write-Host "Line $(CurrentLine)  Executing                    cp -fr /home/eicon/bin/.bashrc /home/eicon/.bashrc";
##    cp -fr /home/eicon/bin/.bashrc /home/eicon/.bashrc
    Write-Host "Line $(CurrentLine)  Leaving                      configure_bashrc";
}
#---------------------------------------------------------
# create_base_path
#---------------------------------------------------------
function create_base_path
{
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------";
    Write-Host "Line $(CurrentLine)  Entering                     create_base_path";
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${BASE_PATH})";
    if (Test-Path ${BASE_PATH})
    {
        Write-Host "Line $(CurrentLine)  ${BASE_PATH}                 Exists" ;  
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Creating                     ${BASE_PATH}";
        Write-Host "Line $(CurrentLine)  Executing                    md ${BASE_PATH}";
        Write-Host "Line $(CurrentLine)  cmd output follows ---------------------------------------------------------------------";
        md ${BASE_PATH}
        Write-Host "Line $(CurrentLine)  cmd output ends ------------------------------------------------------------------------";
    }
    Write-Host "Line $(CurrentLine)  Executing                    cd ${BASE_PATH}";
    cd ${BASE_PATH}
    Write-Host "Line $(CurrentLine)  Current Directory          = $PWD";
    Write-Host "Line $(CurrentLine)  Leaving                      create_base_path";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------";
}

#---------------------------------------------------------
# Echo Args 
#---------------------------------------------------------
function echo_args
{
$MyVariable = 1

    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------";
    Write-Host "Line $(CurrentLine)  Entering                     echo_args";
    Write-Host "Line $(CurrentLine)  Arguments as follows         ";
    Write-Host "Line $(CurrentLine)  DRIVE                      = ${DRIVE} ";
    Write-Host "Line $(CurrentLine)  FOLDER                     = ${FOLDER} ";
    Write-Host "Line $(CurrentLine)  DISTRO_MASTER              = ${DISTRO_MASTER} ";
    Write-Host "Line $(CurrentLine)  DISTRO_NEW                 = ${DISTRO_NEW} ";
    Write-Host "Line $(CurrentLine)  FOLDER                     = ${VENDOR} ";
    Write-Host "Line $(CurrentLine)  End of Arguments             ";
    Write-Host "";
	Write-Host "Line $(CurrentLine)  VENDOR_LC                  = $VENDOR_LC";
    Write-Host "Line $(CurrentLine)  VHD_MASTER                 = ${VHD_MASTER}";            
    Write-Host "Line $(CurrentLine)  VHD_NEW                    = ${VHD_NEW}";            
    Write-Host "Line $(CurrentLine)  BIN_PATH                   = $BIN_PATH";
    Write-Host "Line $(CurrentLine)  BASE_PATH                  = ${BASE_PATH}";
	Write-Host "Line $(CurrentLine)  VHD_NEW_PATH               = ${VHD_NEW_PATH}";
	Write-Host "Line $(CurrentLine)  VHD_MASTER_TAR             = $VHD_MASTER_TAR";
    Write-Host "Line $(CurrentLine)  USER                       = ${USER}";            
    Write-Host "Line $(CurrentLine)  WSL_CMD_MASTER             = ${WSL_CMD_MASTER}";            
    Write-Host "Line $(CurrentLine)  WSL_CMD_NEW                = ${WSL_CMD_NEW}";            
    Write-Host "Line $(CurrentLine)  Leaving                      echo_args";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------";
}


#---------------------------------------------------------
# CurrentLine
#---------------------------------------------------------
function CurrentLine
{
    $MyInvocation.ScriptLineNumber
}


#---------------------------------------------------------
# Main Entry point
#---------------------------------------------------------
Write-Host "Line $(CurrentLine)  Calling                      main()"
main
Write-Host "Line $(CurrentLine)  All Done  To exit Press Continue"
Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"


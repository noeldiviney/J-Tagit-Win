#!"C:\Program Files\PowerShell\7\pwsh.exe" -ExecutionPolicy Bypass
###   #!/usr/bin/env pwsh
#--------------------------------------------------------------
#    Desktop Shortcut Example
#    W:\ProgramFiles\PowerShell\7\pwsh.exe -ExecutionPolicy Bypass -File W:\DinRDuino\PwrShell\arduino_ide.ps1 W DinRDuino 1.8.15 Eicon BluePill F103C8T6 SerLed V1.0.1 stm32 swd
#
#    Launching the Arduino IDE in a fully configured way.
#    This Pwershell Script uses forward declarations as follows.
#
#    #---------------------------------------------------------
#    # Main Function
#    #---------------------------------------------------------
#    function main
#    {
#        function_1        # Calling function_1
#        function_2        # Calling function_2
#    }
#    #---------------------------------------------------------
#    # Function 1
#    #---------------------------------------------------------
#    function function_1
#    {
#        do something
#    }
#    #---------------------------------------------------------
#    # Function 2
#    #---------------------------------------------------------
#    function function_1
#    {
#        do something else
#    }
#    #---------------------------------------------------------
#    # Main      The script entry point
#    #---------------------------------------------------------
#    main                      # Call function_main
#--------------------------------------------------------------

#---------------------------------------------------------
# Global Parameters
#--------------------------------------------------------

#param([string]$OS              = "OS",
#      [string]$DRIVE           = "DRIVE",
#      [string]$PROJ_NAME       = "PROJ_NAME",
#      [string]$BP_LINE_NO      = "BP_LINE_NO");

#$OS_UC                         = "${OS}"
#$OS_LC                         = ${OS}.ToLower()
#$OS_C                          = ${OS_UC}.SubString(0, 1) + ${OS_LC}.SubString(1, 2)
#OS_C                          = ${OS_UC}.SubString(0, 1) + 

$OS_UC                         = "WIN"
$OS_LC                         = "win"
$OS_C                          = "Win"     
$PROJ_NAME                     = "F103CBT6-BluePill-HelloW"
$BP_LINE_NO                    = "42"
$DRIVE                         = "C"
$SLASH                         = "\"
$INSTALL_FOLDER                = "J-Tagit"
$WORKSPACE_FOLDER              = "CodeLite"
$PROJECT_NAME                  = "${PROJ_NAME}"
$CPU_NAME                      = ${PROJECT_NAME}.SubString(0, 8)
$PROJECT_RELEASE               = "2.2.0"
$CORE_NAME                     = "Core_STM32"
$PLATFORM_NAME                 = "stm32"
$ARDUINO_VERSION               = "1.8.18"
$ARDUINO_VERSION2              = "1.8.18"
$ARDUINO_LIBRARIES_RELEASE     = "10818"
$ARDUINO_CLI_RELEASE           = "0.20.2"
$BOARD_VENDOR_EI               = "Eicon"
$BOARD_VENDOR_EI_LC            = "eicon"
$BOARD_VENDOR_ST               = "STMicroelectronics"
$SKETCHBOOK_RELEASE            = "0.1.0"
$OPENOCD_RELEASE               = "0.11.0-1"
$OPENOCD_CFG_RELEASE           = "0.1.0"
$XPACK_GCC_VERSION             = "10.3.1-2.3"
$ARM_GCC_VERSION               = "10.1.0"
$TEENSY_GCC_VERSION            = "5.4.1"
$CMSIS_VERSION                 = "5.7.0"
$BREAKPOINT_LINE_NO            = "${BP_LINE_NO}"
$TEMPL_PROJ_NAME               = "${PROJ_NAME}"
$PROBE                         = "jtagit-2"
$CPU_TARGET                    = "stm32f1x.cfg"
$BUILD_SYS                     = "cmake"
$PROTOCOL                      = "swd"
$OPENOCD_CFG_FILENAME          = "${PROJECT_NAME}-${BUILD_SYS}-${PROTOCOL}.cfg"

$XPACK_GCC_URI                 = "https://github.com/xpack-dev-tools/arm-none-eabi-gcc-xpack/releases/download/v${XPACK_GCC_VERSION}/xpack-arm-none-eabi-gcc-${XPACK_GCC_VERSION}-win32-x64.zip"
$STM32DUINO_CMSIS_URI          = "https://github.com/stm32duino/ArduinoModule-CMSIS/releases/download/${CMSIS_VERSION}/CMSIS-${CMSIS_VERSION}.tar.bz2"
$PROJECT_URI                   = "https://gitlab.com/noeldiviney/${PROJECT_NAME}-${OS_C}/-/archive/${PROJECT_RELEASE}/${PROJECT_NAME}-${OS_C}-${PROJECT_RELEASE}.zip"
$VARIANTS_URI                  = "https://gitlab.com/noeldiviney/${BOARD_VENDOR_EI_LC}-${OS_LC}-${PLATFORM_NAME}-variants/-/archive/${PROJECT_RELEASE}/${BOARD_VENDOR_EI_LC}-${OS_LC}-${PLATFORM_NAME}-variants-${PROJECT_RELEASE}.zip"
$STM32_CORE_ZIP_URI            = "https://github.com/stm32duino/Arduino_${CORE_NAME}/archive/refs/tags/${PROJECT_RELEASE}.zip"
## https://github.com/stm32duino/Arduino_Core_STM32/archive/refs/tags/2.2.0.zip
## https://gitlab.com/noeldiviney/F103CBT6-BluePill-HelloW-Win/-/archive/2.2.0/F103CBT6-BluePill-HelloW-Win-2.2.0.zip
$XPACK_GCC_ZIP_NAME            = "xpack-arm-none-eabi-gcc-${XPACK_GCC_VERSION}-win32-x64.zip"
$STM32DUINO_CMSIS_ZIP_NAME     = "CMSIS-${CMSIS_VERSION}.tar.bz2"
$STM32DUINO_CMSIS_TAR_NAME     = "CMSIS-${CMSIS_VERSION}.tar"
$PROJECT_ZIP_NAME              = "${PROJECT_NAME}-${OS_C}-${PROJECT_RELEASE}.zip"
$PROJECT_UNZIPPED_NAME         = "${PROJECT_NAME}-${OS_C}-${PROJECT_RELEASE}"
$ARDUINO_CORE_ZIP_NAME         = "Arduino_${CORE_NAME}-${PROJECT_RELEASE}.zip"
$ARDUINO_CORE_NAME             = "Arduino_${CORE_NAME}-${PROJECT_RELEASE}"
$VARIANTS_ZIP_NAME             = "${BOARD_VENDOR_EI_LC}-${OS_LC}-${PLATFORM_NAME}-variants-${PROJECT_RELEASE}.zip"
$VARIANTS_NAME                 = "${BOARD_VENDOR_EI_LC}-${OS_LC}-${PLATFORM_NAME}-variants-${PROJECT_RELEASE}"
$XPACK_GCC_UNZIPPED_NAME       = "xpack-arm-none-eabi-gcc-${XPACK_GCC_VERSION}"

$OPENOCD_TB_TYPE               = "win32-x64.zip"
$ARDUINO_LIBRARIES_PREFIX      = "${BOARD_VENDOR_EI_LC}-${OS_LC}-arduino-libraries"
$SKETCHBOOK_NAME_PREFIX        = "${BOARD_VENDOR_EI_LC}-${OS_LC}-sketchbook"
$XPACK_GCC_NAME                = "xpack-arm-none-eabi-gcc"
$STM32DUINO_CMSIS_NAME         = "CMSIS-${CMSIS_VERSION}"
$EICON_GCC_NAME                = "${BOARD_VENDOR_EI_LC}-arm-none-eabi-gcc"
$ARDUINO_CLI_TB_TYPE           = "Windows_64bit.zip"
$ARDUINO_CLI_TB_NAME           = "arduino-cli_${ARDUINO_CLI_RELEASE}_${ARDUINO_CLI_TB_TYPE}"
$ARDUINO_TB_TYPE               = "windows.zip"
$OPENOCD_CFG_SCRIPT_NAME       = "${BOARD_VENDOR_EI_LC}-${OS_LC}-openocd_cfg"

$BASE_PATH                     = "${DRIVE}:\"
$INSTALL_FOLDER_PATH           = "${BASE_PATH}${INSTALL_FOLDER}"
$DLOAD_PATH                    = "${INSTALL_FOLDER_PATH}\dload"
$TOOLS_PATH                    = "${INSTALL_FOLDER}\tools"
$WORKSPACE_PATH                = "${INSTALL_FOLDER_PATH}\${WORKSPACE_FOLDER}"
$BUILD_PATH                    = "${WORKSPACE_PATH}\${PROJECT_NAME}\build"
$OPENOCD_BUILD_PATH            = "${DRIVE}:/${INSTALL_FOLDER}/${WORKSPACE_FOLDER}/${PROJECT_NAME}/build"
$APP_PATH                      = "${WORKSPACE_PATH}\${PROJECT_NAME}\App"
$ARDUINO_PATH                  = "${INSTALL_FOLDER_PATH}\arduino-${ARDUINO_VERSION}"
$PORTABLE_PATH                 = "${ARDUINO_PATH}\portable"
$HARDWARE_PATH_EI              = "${PORTABLE_PATH}\packages\${BOARD_VENDOR_EI}\hardware"
$HARDWARE_PATH_ST              = "${PORTABLE_PATH}\packages\${BOARD_VENDOR_ST}\hardware"
$TOOLS_PATH_EI                 = "${PORTABLE_PATH}\packages\${BOARD_VENDOR_EI}\tools"
$TOOLS_PATH_ST                 = "${PORTABLE_PATH}\packages\${BOARD_VENDOR_ST}\tools"
$OPENOCD_PATH                  = "${INSTALL_FOLDER_PATH}\openocd"
$OPENOCD_CFG_PATH              = "${OPENOCD_PATH}\scripts\board\${BOARD_VENDOR_EI}\${OS_LC}\jtagit-2"
$PREFS_PATH                    = "${PORTABLE_PATH}"
$SKETCH_PATH                   = "${PORTABLE_PATH}\sketchbook/arduino"
##  https://gitlab.com/noeldiviney/eicon_lin_stm32_variants/-/archive/2.2.0/eicon_lin_stm32_variants-2.2.0.zip
##  https://gitlab.com/noeldiviney/eicon_lin_stm32_variants/-/archive/2.2.0/eicon_lin_stm32_variants-2.2.0.zip
##  https://github.com/xpack-dev-tools/arm-none-eabi-gcc-xpack/releases/download/v10.3.1-2.3/xpack-arm-none-eabi-gcc-10.3.1-2.3-win32-x64.zip
##  https://github.com/stm32duino/ArduinoModule-CMSIS/releases/download/5.7.0/CMSIS-5.7.0.tar.bz2
$ARDUINO_CLI_URI               = "https://downloads.arduino.cc/arduino-cli/arduino-cli_${ARDUINO_CLI_RELEASE}_${ARDUINO_CLI_TB_TYPE}"
$OPENOCD_CFG_SCRIPT_URI        = "https://gitlab.com/noeldiviney/${OPENOCD_CFG_SCRIPT_NAME}/-/archive/${OPENOCD_CFG_RELEASE}/${OPENOCD_CFG_SCRIPT_NAME}-${OPENOCD_CFG_RELEASE}.tar.gz"
$ARDUINO_LIBRARIES_ZIP_URI     = "https://gitlab.com/noeldiviney/${BOARD_VENDOR_EI_LC}_${OS_LC}_arduino_libraries/-/archive/${ARDUINO_LIBRARIES_RELEASE}/${ARDUINO_LIBRARIES_PREFIX}-${ARDUINO_LIBRARIES_RELEASE}.zip"
$ARDUINO_ZIP_URI               = "https://downloads.arduino.cc/arduino-${ARDUINO_VERSION}-${ARDUINO_TB_TYPE}"
$SKETCHBOOK_ZIP_URI            = "https://gitlab.com/noeldiviney/${BOARD_VENDOR_EI_LC}_${OS_LC}_sketchbook/-/archive/${SKETCHBOOK_RELEASE}/${BOARD_VENDOR_EI_LC}-${OS_LC}-sketchbook-${SKETCHBOOK_RELEASE}.zip"
$OPENOCD_TB_NAME               = "xpack-openocd-${OPENOCD_RELEASE}-${OPENOCD_TB_TYPE}"
$OPENOCD_TB_URI                = "https://github.com/xpack-dev-tools/openocd-xpack/releases/download/v${OPENOCD_RELEASE}/xpack-openocd-${OPENOCD_RELEASE}-${OPENOCD_TB_TYPE}"
$STM32_INDEX                   = "        https://gitlab.com/noeldiviney/${BOARD_VENDOR_EI_LC}_${OS_LC}_packages/-/raw/main/package_${BOARD_VENDOR_EI_LC}_stm32_index.json,"
$KINETIS_INDEX                 = "        https://gitlab.com/noeldiviney/${BOARD_VENDOR_EI_LC}_${OS_LC}_packages/-/raw/main/package_${BOARD_VENDOR_EI_LC}_kinetis_index.json,"
$STMicroelectronics_INDEX      = "        https://github.com/stm32duino/BoardManagerFiles/raw/main/package_stmicroelectronics_index.json"

$ARDUINO_ZIP_FILE              = "arduino-${ARDUINO_VERSION}.zip"
$ARDUINO_LIBRARIES_ZIP_FILE    = "${BOARD_VENDOR_EI_LC}-${OS_LC}-arduino-libraries-${ARDUINO_LIBRARIES_RELEASE}.zip"
$SKETCH_NAME                   = "${BOARD_VENDOR}-${BOARD}-${CPU}-${SKETCH}-${SKETCH_VER}"
$USER                          = "$env:UserName"                                 # $env:VAR_NAME="VALUE"
$PROFILE_PATH                  = "/home/$env:USER"
$SCRIPT_PATH                   = "${BASE_ROOT}bin"
$OPENOCD_ZIP_FILE              = "${BOARD_VENDOR_EI_LC}-openocd-${OPENOCD_RELEASE}.zip"
$ARDUINO_CLI_ZIP_FILE          = "arduino_cli.zip"
$SKETCHBOOK_ZIP_FILE           = "sketchbook.zip"
$SourceFileLocation            = "${BASE_ROOT}Program Files/Notepad++/notepad++.exe"

#Read-Host -Prompt "Pausing:  Press any key to continue"

#---------------------------------------------------------
# Echo Args
#---------------------------------------------------------
function echo_args
{
$MyVariable = 1

    Write-Host "Line $(CurrentLine)  Entering                        echo_args";
    Write-Host "Line $(CurrentLine)  OS_UC                         = ${OS_UC}";
    Write-Host "Line $(CurrentLine)  OS_LC                         = ${OS_LC}";
    Write-Host "Line $(CurrentLine)  OS_C                          = ${OS_C}";
    Write-Host "Line $(CurrentLine)  DRIVE                         = ${DRIVE}";
    Write-Host "Line $(CurrentLine)  SLASH                         = ${SLASH}";
    Write-Host "Line $(CurrentLine)  INSTALL_FOLDER                = ${INSTALL_FOLDER}";
    Write-Host "Line $(CurrentLine)  WORKSPACE_FOLDER              = ${WORKSPACE_FOLDER}";
    Write-Host "Line $(CurrentLine)  CPU_NAME                      = ${CPU_NAME}";
    Write-Host "Line $(CurrentLine)  PROJECT_NAME                  = ${PROJECT_NAME}";
    Write-Host "Line $(CurrentLine)  PROJECT_RELEASE               = ${PROJECT_RELEASE}";
    Write-Host "Line $(CurrentLine)  CORE_NAME                     = ${CORE_NAME}";
    Write-Host "Line $(CurrentLine)  ARDUINO_VERSION               = ${ARDUINO_VERSION}";
    Write-Host "Line $(CurrentLine)  ARDUINO_VERSION2              = ${ARDUINO_VERSION2}";
    Write-Host "Line $(CurrentLine)  ARDUINO_LIBRARIES_RELEASE     = ${ARDUINO_LIBRARIES_RELEASE}";
    Write-Host "Line $(CurrentLine)  ARDUINO_CLI_RELEASE           = ${ARDUINO_CLI_RELEASE}";
    Write-Host "Line $(CurrentLine)  BOARD_VENDOR_EI               = ${BOARD_VENDOR_EI}";
    Write-Host "Line $(CurrentLine)  BOARD_VENDOR_EI_LC            = ${BOARD_VENDOR_EI_LC}";
    Write-Host "Line $(CurrentLine)  BOARD_VENDOR_ST               = ${BOARD_VENDOR_ST}";
    Write-Host "Line $(CurrentLine)  SKETCHBOOK_RELEASE            = ${SKETCHBOOK_RELEASE}";
    Write-Host "Line $(CurrentLine)  OPENOCD_RELEASE               = ${OPENOCD_RELEASE}";
    Write-Host "Line $(CurrentLine)  OPENOCD_CFG_RELEASE           = ${OPENOCD_CFG_RELEASE}";
    Write-Host "Line $(CurrentLine)  ARM_GCC_VERSION               = ${ARM_GCC_VERSION}";
    Write-Host "Line $(CurrentLine)  TEENSY_GCC_VERSION            = ${TEENSY_GCC_VERSION}";
    Write-Host "Line $(CurrentLine)  CMSIS_VERSION                 = ${CMSIS_VERSION}";
    Write-Host "Line $(CurrentLine)  BREAKPOINT_LINE_NO            = ${BREAKPOINT_LINE_NO}";
    Write-Host "Line $(CurrentLine)  TEMPL_PROJ_NAME               = ${TEMPL_PROJ_NAME}";
    Write-Host "Line $(CurrentLine)  PROBE                         = ${PROBE}";
    Write-Host "Line $(CurrentLine)  CPU_TARGET                    = ${CPU_TARGET}";
    Write-Host "Line $(CurrentLine)  BUILD_SYS                     = ${BUILD_SYS}";
    Write-Host "Line $(CurrentLine)  PROTOCOL                      = ${PROTOCOL}";
    Write-Host "Line $(CurrentLine)  OPENOCD_CFG_FILENAME          = ${OPENOCD_CFG_FILENAME}";

    Write-Host "Line $(CurrentLine)  PROJECT_FOLDER                = ${PROJECT_NAME}";	
    Write-Host "Line $(CurrentLine)  PROJECT_NAME                  = ${PROJECT_NAME}";	
    Write-Host "Line $(CurrentLine)  PROJECT_RELEASE               = ${PROJECT_RELEASE}";	
    Write-Host "Line $(CurrentLine)  ARDUINO_VERSION               = ${ARDUINO_VERSION}";
    Write-Host "Line $(CurrentLine)  ARDUINO_LIBRARIES_RELEASE     = $ARDUINO_LIBRARIES_RELEASE";	
    Write-Host "Line $(CurrentLine)  ARDUINO_CLI_RELEASE           = ${ARDUINO_CLI_RELEASE}";            
    Write-Host "Line $(CurrentLine)  OPENOCD_CFG_SCRIPT_NAME       = ${OPENOCD_CFG_SCRIPT_NAME}";            
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Write-Host "Line $(CurrentLine)";
    Write-Host "Line $(CurrentLine)  BASE_PATH                     = ${BASE_PATH}";
    Write-Host "Line $(CurrentLine)  INSTALL_FOLDER_PATH           = ${INSTALL_FOLDER_PATH}";	
    Write-Host "Line $(CurrentLine)  DLOAD_PATH                    = ${DLOAD_PATH}";	
    Write-Host "Line $(CurrentLine)  TOOLS_PATH                    = ${TOOLS_PATH}";	
    Write-Host "Line $(CurrentLine)  WORKSPACE_PATH                = ${WORKSPACE_PATH}";	
    Write-Host "Line $(CurrentLine)  APP_PATH                      = ${APP_PATH}";	
    Write-Host "Line $(CurrentLine)  BUILD_PATH                    = ${BUILD_PATH}";	
    Write-Host "Line $(CurrentLine)  OPENOCD_BUILD_PATH            = ${OPENOCD_BUILD_PATH}";	
    Write-Host "Line $(CurrentLine)  ARDUINO_PATH                  = ${ARDUINO_PATH}";            
    Write-Host "Line $(CurrentLine)  PORTABLE_PATH                 = ${PORTABLE_PATH}";            
    Write-Host "Line $(CurrentLine)  HARDWARE_PATH_EI              = ${HARDWARE_PATH_EI}";            
    Write-Host "Line $(CurrentLine)  HARDWARE_PATH_ST              = ${HARDWARE_PATH_ST}";            
    Write-Host "Line $(CurrentLine)  TOOLS_PATH_EI                 = ${TOOLS_PATH_EI}";            
    Write-Host "Line $(CurrentLine)  TOOLS_PATH_ST                 = ${TOOLS_PATH_ST}";            
    Write-Host "Line $(CurrentLine)  OPENOCD_PATH                  = ${OPENOCD_PATH}";            
    Write-Host "Line $(CurrentLine)  OPENOCD_CFG_PATH              = ${OPENOCD_CFG_PATH}";            
    Write-Host "Line $(CurrentLine)  PREFS_PATH                    = ${PREFS_PATH}";            
    Write-Host "Line $(CurrentLine)  SKETCH_PATH                   = ${SKETCH_PATH}";            
    Write-Host "Line $(CurrentLine)";

	Write-Host "Line $(CurrentLine)  XPACK_GCC_URI                 = ${XPACK_GCC_URI}";
	Write-Host "Line $(CurrentLine)  STM32DUINO_CMSIS_URI          = ${STM32DUINO_CMSIS_URI}";
	Write-Host "Line $(CurrentLine)  PROJECT_URI                   = ${PROJECT_URI}";
	Write-Host "Line $(CurrentLine)  VARIANTS_URI                  = ${VARIANTS_URI}";
    Write-Host "Line $(CurrentLine)  STM32_CORE_ZIP_URI            = ${STM32_CORE_ZIP_URI}";            
    Write-Host "Line $(CurrentLine)  XPACK_GCC_ZIP_NAME            = ${XPACK_GCC_ZIP_NAME}";
    Write-Host "Line $(CurrentLine)  XPACK_GCC_NAME                = ${XPACK_GCC_NAME}";            
    Write-Host "Line $(CurrentLine)  STM32DUINO_CMSIS_ZIP_NAME     = ${STM32DUINO_CMSIS_ZIP_NAME}";
    Write-Host "Line $(CurrentLine)  STM32DUINO_CMSIS_NAME         = ${STM32DUINO_CMSIS_NAME}";
    Write-Host "Line $(CurrentLine)  PROJECT_ZIP_NAME              = ${PROJECT_ZIP_NAME}";
    Write-Host "Line $(CurrentLine)  PROJECT_UNZIPPED_NAME         = ${PROJECT_UNZIPPED_NAME}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_CORE_ZIP_NAME         = ${ARDUINO_CORE_ZIP_NAME}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_CORE_NAME             = ${ARDUINO_CORE_NAME}";            
	Write-Host "Line $(CurrentLine)  VARIANTS_ZIP_NAME             = ${VARIANTS_ZIP_NAME}";
	Write-Host "Line $(CurrentLine)  VARIANTS_NAME                 = ${VARIANTS_NAME}";
    Write-Host "Line $(CurrentLine)";	
    Write-Host "Line $(CurrentLine)  OPENOCD_TB_TYPE               = ${OPENOCD_TB_TYPE}";            
    Write-Host "Line $(CurrentLine)  XPACK_GCC_NAME                = ${XPACK_GCC_NAME}";            
    Write-Host "Line $(CurrentLine)  EICON_GCC_NAME                = ${EICON_GCC_NAME}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_CLI_TB_TYPE           = ${ARDUINO_CLI_TB_TYPE}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_CLI_TB_NAME           = ${ARDUINO_CLI_TB_NAME}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_TB_TYPE               = ${ARDUINO_TB_TYPE}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_ZIP_FILE              = ${ARDUINO_ZIP_FILE}";            
    Write-Host "Line $(CurrentLine)  OPENOCD_TB_NANE               = ${OPENOCD_TB_NAME}";                    
    Write-Host "Line $(CurrentLine)  OPENOCD_CFG_SCRIPT_NAME       = ${OPENOCD_CFG_SCRIPT_NAME}";
    Write-Host "Line $(CurrentLine)  SKETCHBOOK_ZIP_FILE           = ${SKETCHBOOK_ZIP_FILE}";            
    Write-Host "Line $(CurrentLine)  USER                          = ${USER}";            
    Write-Host "Line $(CurrentLine)  Leaving                         echo_args";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# Main Function
#---------------------------------------------------------
function main
{
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------";
    Write-Host "Line $(CurrentLine)  Entering                     main"; 

    Write-Host "Line $(CurrentLine)  Calling                      echo_args";
    echo_args                                                                        # Calling echo_args
Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      check_environment";
    check_environment
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      install_arduino_cmake_project";
   install_arduino_cmake_project
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      install_arduino_stm32_core";
    install_arduino_stm32_core
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

###    Write-Host "Line $(CurrentLine)  Calling                      install_xpack_arm_gcc";
###    install_xpack_arm_gcc
###Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

###    Write-Host "Line $(CurrentLine)  Calling                      install_stm32duino_cmsis";
###    install_stm32duino_cmsis
###Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    
    Write-Host "Line $(CurrentLine)  Calling                      install_eicon_win_stm32_variants";
    install_eicon_win_stm32_variants
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      install_codelite_config";
    install_codelite_config
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      rename_files";
    rename_files
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      create_openocd_config";
    create_openocd_config
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      build_project";
    build_project
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
}

#---------------------------------------------------------
# build_project
#---------------------------------------------------------
function build_project
{
    Write-Host "Line $(CurrentLine)  Entering                     build_project";
    Write-Host "Line $(CurrentLine)  Executing                    cd ${BUILD_PATH}";
    cd ${BUILD_PATH}
	mkdir tmp
    Write-Host "Line $(CurrentLine)  PWD                        = ${PWD}";
#    Write-Host "Line $(CurrentLine)  Executing                    rm -rf *";
#    rm -rf *
    Write-Host "Line $(CurrentLine)  Executing                    cmake .. -G "CodeLite - Unix Makefiles"";
    cmake .. -G "CodeLite - Unix Makefiles"
    Write-Host "Line $(CurrentLine)  Executing                    make";
    make        
    Write-Host "Line $(CurrentLine)  Leaving                      build_project";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# create_openocd_config
#---------------------------------------------------------
function create_openocd_config
{
    Write-Host "Line $(CurrentLine)  Entering                     create_openocd_config";
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${OPENOCD_CFG_PATH}\${OPENOCD_CFG_FILENAME})";
    if (Test-Path ${OPENOCD_CFG_PATH}\${OPENOCD_CFG_FILENAME})    
    {
        Write-Host "Line $(CurrentLine)  OpenOCD CFG File             exists  ...  deleting" ; 
        Remove-Item -Recurse -Force ${OPENOCD_CFG_PATH}\${OPENOCD_CFG_FILENAME}
    }
	else
    {
        Write-Host "Line $(CurrentLine)  OpenOCD Cfg File             does not exist  ...  continuing" ;  
    }
    Write-Host "Line $(CurrentLine)  Adding                       ${OPENOCD_CFG_PATH}\${OPENOCD_CFG_FILENAME} Contents";
    Add-Content ${OPENOCD_CFG_PATH}\${OPENOCD_CFG_FILENAME} "# ${BOARD_VENDOR_EI} ${PROJECT_NAME}"
    Add-Content ${OPENOCD_CFG_PATH}\${OPENOCD_CFG_FILENAME} ""
    Add-Content ${OPENOCD_CFG_PATH}\${OPENOCD_CFG_FILENAME} "source [find interface/${BOARD_VENDOR_EI}/${PROBE}-${PROTOCOL}.cfg]"
    Add-Content ${OPENOCD_CFG_PATH}\${OPENOCD_CFG_FILENAME} ""
    Add-Content ${OPENOCD_CFG_PATH}\${OPENOCD_CFG_FILENAME} "source [find target/${BOARD_VENDOR_EI}/${CPU_TARGET}]"
    Add-Content ${OPENOCD_CFG_PATH}\${OPENOCD_CFG_FILENAME} ""
    Add-Content ${OPENOCD_CFG_PATH}\${OPENOCD_CFG_FILENAME} "init"
    Add-Content ${OPENOCD_CFG_PATH}\${OPENOCD_CFG_FILENAME} ""
    Add-Content ${OPENOCD_CFG_PATH}\${OPENOCD_CFG_FILENAME} "program ${OPENOCD_BUILD_PATH}/${PROJECT_NAME}.hex verify reset"
    Write-Host "Line $(CurrentLine)  Editing                      ${OPENOCD_CFG_PATH}\${OPENOCD_CFG_FILENAME} Completed";
    Write-Host "Line $(CurrentLine)  Leaving                      create_openocd_config";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# rename_files
#---------------------------------------------------------
function rename_files
{
    Write-Host "Line $(CurrentLine)  Entering                     rename_files";
    Write-Host "Line $(CurrentLine)  Executing                    Rename-Item  ${APP_PATH}\${TEMPL_PROJ_NAME}.cpp  ${APP_PATH}\${PROJECT_NAME}.cpp";
    Rename-Item  ${APP_PATH}\${TEMPL_PROJ_NAME}.cpp  ${APP_PATH}\${PROJECT_NAME}.cpp

    Write-Host "Line $(CurrentLine)  Leaving                      rename_files";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# install_codelite_config
#---------------------------------------------------------
function install_codelite_config
{
    Write-Host "Line $(CurrentLine)  Entering                     install_codelite_config";
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${INSTALL_FOLDER_PATH}\.gdbinit)";
    if (Test-Path ${INSTALL_FOLDER_PATH}\.gdbinit)    
    {
        Write-Host "Line $(CurrentLine)  .gdbinit                     exists  ...  deleting" ; 
        Remove-Item -Recurse -Force ${INSTALL_FOLDER_PATH}\.gdbinit
    }
	else
    {
        Write-Host "Line $(CurrentLine)  .gdbinit                     does not exist  ...  continuing" ;  
    }
    Write-Host "Line $(CurrentLine)  Adding                       ${INSTALL_FOLDER_PATH}\.gdbinit Contents";
    Add-Content ${INSTALL_FOLDER_PATH}\.gdbinit "set auto-load safe-path ${BUILD_PATH}\.gdbinit"
    Write-Host "Line $(CurrentLine)  Adding                       ${BUILD_PATH}\.gdbinit Contents";

    Add-Content ${BUILD_PATH}\.gdbinit "set history save on"
    Add-Content ${BUILD_PATH}\.gdbinit "set pagination off"
    Add-Content ${BUILD_PATH}\.gdbinit "set print pretty on"
    Add-Content ${BUILD_PATH}\.gdbinit "set confirm off"
    Add-Content ${BUILD_PATH}\.gdbinit "target extended-remote:3333"
    Add-Content ${BUILD_PATH}\.gdbinit "b ${APP_PATH}\${PROJECT_NAME}.cpp:${BREAKPOINT_LINE_NO}"

    Write-Host "Line $(CurrentLine)  Editing                      install_codelite_config Completed";
    Write-Host "Line $(CurrentLine)  Leaving                      install_codelite_config";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}


#---------------------------------------------------------
# install_eicon_win_stm32_variants
#---------------------------------------------------------
function install_eicon_win_stm32_variants
{
    Write-Host "Line $(CurrentLine)  Entering                     install_eicon_win_stm32_variants";
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${INSTALL_FOLDER_PATH}\${CORE_NAME}\variants)";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${INSTALL_FOLDER_PATH}\${CORE_NAME}\variants)
    {
        Write-Host "Line $(CurrentLine)  variants                     exist  ...  deleting" ;  
        Write-Host "Line $(CurrentLine)  Executing                    Remove-Item -Recurse -Force ${INSTALL_FOLDER_PATH}\${CORE_NAME}\variants" ;  
        Remove-Item -Recurse -Force ${INSTALL_FOLDER_PATH}\${CORE_NAME}\variants
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    }
	else
    {
        Write-Host "Line $(CurrentLine)  variants                     do not exist  ...  continuing" ;  
    }


#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"


    if (Test-Path ${INSTALL_FOLDER_PATH}\${CORE_NAME}\boards.txt)
    {
        Write-Host "Line $(CurrentLine)  boards.txt                   exist  ...  deleting" ;  
        Write-Host "Line $(CurrentLine)  Executing                    Remove-Item -Recurse -Force ${INSTALL_FOLDER_PATH}\${CORE_NAME}\boards.txt" ;  
        Remove-Item -Recurse -Force ${INSTALL_FOLDER_PATH}\${CORE_NAME}\boards.txt
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    }
	else
    {
        Write-Host "Line $(CurrentLine)  boards.txt                   does not exist  ...  continuing" ;  
    }

    if (Test-Path ${INSTALL_FOLDER_PATH}\${CORE_NAME}\platform.txt)
    {
        Write-Host "Line $(CurrentLine)  platform.txt                 exist  ...  deleting" ;  
        Write-Host "Line $(CurrentLine)  Executing                    Remove-Item -Recurse -Force ${INSTALL_FOLDER_PATH}\${CORE_NAME}\platform.txt" ;  
        Remove-Item -Recurse -Force ${INSTALL_FOLDER_PATH}\${CORE_NAME}\platform.txt
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    }
	else
    {
        Write-Host "Line $(CurrentLine)  platform.txt                 does not exist  ...  continuing" ;  
    }

    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${DLOAD_PATH}\${VARIANTS_ZIP_NAME})";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${DLOAD_PATH}\${VARIANTS_ZIP_NAME})
    {
        Write-Host "Line $(CurrentLine)  variants zip                 exist  ...  deleting" ;  
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
        Write-Host "Line $(CurrentLine)  Executing                    Remove-Item -Recurse -Force ${DLOAD_PATH}\${VARIANTS_ZIP+NAME}" ;  
        Remove-Item -Recurse -Force ${DLOAD_PATH}\${VARIANTS_ZIP_NAME}        
    }

    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${DLOAD_PATH}\${VARIANTS_NAME})";
    if (Test-Path ${DLOAD_PATH}\${VARIANTS_NAME})
    {
        Write-Host "Line $(CurrentLine)  variants unzipped            exists  ...  deleting" ;  
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
        Write-Host "Line $(CurrentLine)  Executing                    Remove-Item -Recurse -Force ${DLOAD_PATH}\${VARIANTS_NAME}" ;  
        Remove-Item -Recurse -Force ${DLOAD_PATH}\${VARIANTS_NAME}        
    }
		
    Write-Host "Line $(CurrentLine)  Downloading                  ${DLOAD_PATH}\${VARIANTS_ZIP_NAME}";
    Write-Host "Line $(CurrentLine)  Executing                    Invoke-WebRequest -Uri ${VARIANTS_URI} -Outfile ${DLOAD_PATH}\${VARIANTS_ZIP_NAME}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Invoke-WebRequest -Uri ${VARIANTS_URI} -Outfile ${DLOAD_PATH}\${VARIANTS_ZIP_NAME}

    Write-Host "Line $(CurrentLine)  Executing                    Expand-Archive -Path ${DLOAD_PATH}\${VARIANTS_ZIP_NAME} -DestinationPath ${DLOAD_PATH}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Expand-Archive -Path ${DLOAD_PATH}\${VARIANTS_ZIP_NAME} -DestinationPath ${DLOAD_PATH}
	
    Write-Host "Line $(CurrentLine)  Executing                    Copy-Item -Recurse -Force -path: "${DLOAD_PATH}\${VARIANTS_NAME}\*" -destination: ${INSTALL_FOLDER_PATH}\${CORE_NAME}";
    Copy-Item -Recurse -Force -path: "${DLOAD_PATH}\${VARIANTS_NAME}\*" -destination: ${INSTALL_FOLDER_PATH}\${CORE_NAME}
	
    Write-Host "Line $(CurrentLine)  Leaving                      install_eicon_lin_stm32_variants";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# install_stm32duino_cmsis
#---------------------------------------------------------
function install_stm32duino_cmsis
{
    Write-Host "Line $(CurrentLine)  Entering                     install_stm32duino_cmsis";

    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${DLOAD_PATH}\${STM32DUINO_CMSIS_ZIP_NAME})";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${DLOAD_PATH}\${STM32DUINO_CMSIS_ZIP_NAME})
    {
        Write-Host "Line $(CurrentLine)  CMSIS                        Exists ... continuing" ;  
    }
    else
    {
        Write-Host "Line $(CurrentLine)  CMSIS                        does not exist  ...   downloading" ;  
        Write-Host "Line $(CurrentLine)  Downloading                  ${DLOAD_PATH}\${STM32DUINO_CMSIS_ZIP_NAME}";
        Write-Host "Line $(CurrentLine)  Executing                    Invoke-WebRequest -Uri ${STM32DUINO_CMSIS_URI} -Outfile ${DLOAD_PATH}\${STM32DUINO_CMSIS_ZIP_NAME}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
        Invoke-WebRequest -Uri ${STM32DUINO_CMSIS_URI} -Outfile ${DLOAD_PATH}\${STM32DUINO_CMSIS_ZIP_NAME}
    }

    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${INSTALL_FOLDER_PATH}\tools\CMSIS)";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${INSTALL_FOLDER_PATH}\tools\CMSIS)
    {
        Write-Host "Line $(CurrentLine)  CMSIS                        is installed  ...  deleting" ;  
        Write-Host "Line $(CurrentLine)  Executing                    Remove-Item -Recurse -Force ${INSTALL_FOLDER_PATH}\tools\CMSIS" ;  
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
        Remove-Item -Recurse -Force ${INSTALL_FOLDER_PATH}\tools\CMSIS
    }
    else
    {
        Write-Host "Line $(CurrentLine)  CMSIS                        is not installed ...  continuing" ;  
    }
#    Write-Host "Line $(CurrentLine)  Executing                    md ${DLOAD_PATH}\fred3";
#    md ${DLOAD_PATH}\fred
#    Write-Host "Line $(CurrentLine)  Executing                    md ${DLOAD_PATH}\fred4";
#	md ${DLOAD_PATH}\fred2
    Write-Host "Line $(CurrentLine)  Executing                    &tar -xf.\CMSIS-5.7.0.tar.bz2";
#    &tar -C${INSTALL_FOLDER_PATH}\tools -xf.\CMSIS-5.7.0.tar.bz2
    & tar -xf CMSIS-5.7.0.tar.bz2


#    & 7z x ${DLOAD_PATH}\${STM32DUINO_CMSIS_ZIP_NAME}
Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Executing                    7z x ${DLOAD_PATH}\${STM32DUINO_CMSIS_TAR_NAME} -o${INSTALL_FOLDER_PATH}\tools";
Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    7z x ${DLOAD_PATH}\${STM32DUINO_CMSIS_TAR_NAME} -o${INSTALL_FOLDER_PATH}\tools

Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Renaming                     ${INSTALL_FOLDER_PATH}\tools\${XPACK_GCC_UNZIPPED_NAME}";
    Write-Host "Line $(CurrentLine)  To                           ${INSTALL_FOLDER_PATH}\tools\${XPACK_GCC_NAME}";
Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Rename-Item  ${INSTALL_FOLDER_PATH}\tools\${XPACK_GCC_UNZIPPED_NAME}  ${INSTALL_FOLDER_PATH}\tools\${XPACK_GCC_NAME}

    Write-Host "Line $(CurrentLine)  Leaving                      install_stm32duino_cmsis";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# install_xpack_arm_gcc
#---------------------------------------------------------
function install_xpack_arm_gcc
{
    Write-Host "Line $(CurrentLine)  Entering                     install_xpack_arm_gcc";
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${DLOAD_PATH}\${XPACK_GCC_ZIP_NAME})";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${DLOAD_PATH}\${XPACK_GCC_ZIP_NAME})
    {
        Write-Host "Line $(CurrentLine)  Xpack GCC                    Exists ... continuing" ;  
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Xpack GCC                    does not exist  ...   downloading" ;  
        Write-Host "Line $(CurrentLine)  Downloading                  ${DLOAD_PATH}\${XPACK_GCC_ZIP_NAME}";
        Write-Host "Line $(CurrentLine)  Executing                    Invoke-WebRequest -Uri ${XPACK_GCC_URI} -Outfile ${DLOAD_PATH}\${XPACK_GCC_ZIP_NAME}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
        Invoke-WebRequest -Uri ${XPACK_GCC_URI} -Outfile ${DLOAD_PATH}\${XPACK_GCC_ZIP_NAME}
    }

    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${INSTALL_FOLDER_PATH}\${XPACK_GCC_NAME})";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${INSTALL_FOLDER_PATH}\${XPACK_GCC_NAME})
    {
        Write-Host "Line $(CurrentLine)  Xpack GCC                    is installed  ...  deleting" ;  
        Write-Host "Line $(CurrentLine)  Executing                    Remove-Item -Recurse -Force ${INSTALL_FOLDER_PATH}\${XPACK_GCC_NAME}" ;  
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
        Remove-Item -Recurse -Force ${INSTALL_FOLDER_PATH}\${XPACK_GCC_NAME}
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Xpack GCC                    is not installed ...  continuing" ;  
    }

    Write-Host "Line $(CurrentLine)  Executing                    Expand-Archive -Path ${DLOAD_PATH}\${XPACK_GCC_ZIP_NAME} -DestinationPath ${INSTALL_FOLDER_PATH}\tools";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Expand-Archive -Path ${DLOAD_PATH}\${XPACK_GCC_ZIP_NAME} -DestinationPath ${INSTALL_FOLDER_PATH}\tools

#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Renaming                     ${INSTALL_FOLDER_PATH}\tools\${XPACK_GCC_UNZIPPED_NAME}";
    Write-Host "Line $(CurrentLine)  To                           ${INSTALL_FOLDER_PATH}\tools\${XPACK_GCC_NAME}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Rename-Item  ${INSTALL_FOLDER_PATH}\tools\${XPACK_GCC_UNZIPPED_NAME}  ${INSTALL_FOLDER_PATH}\tools\${XPACK_GCC_NAME}

    Write-Host "Line $(CurrentLine)  Leaving                      install_xpack_arm_gcc";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# install_arduino_stm32_core
#---------------------------------------------------------
function install_arduino_stm32_core
{
    Write-Host "Line $(CurrentLine)  Entering                     install_arduino_stm32_core";


    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${DLOAD_PATH}\${ARDUINO_CORE_ZIP_NAME})";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${DLOAD_PATH}\${ARDUINO_CORE_ZIP_NAME})
    {
        Write-Host "Line $(CurrentLine)  Arduino STM32 Core           Exists" ;  
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Arduino STM32 Core           does not exist  ...   downloading" ;  
        Write-Host "Line $(CurrentLine)  Downloading                  ${DLOAD_PATH}\${ARDUINO_CORE_ZIP_NAME}";
        Write-Host "Line $(CurrentLine)  Executing                    Invoke-WebRequest -Uri ${STM32_CORE_ZIP_URI} -Outfile ${DLOAD_PATH}\${ARDUINO_CORE_ZIP_NAME}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
        Invoke-WebRequest -Uri ${STM32_CORE_ZIP_URI} -Outfile ${DLOAD_PATH}\${ARDUINO_CORE_ZIP_NAME}
    }
	
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${INSTALL_FOLDER_PATH}\${CORE_NAME})";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${INSTALL_FOLDER_PATH}\${CORE_NAME})
    {
        Write-Host "Line $(CurrentLine)  Arduino STM32 Core           is installed  ...  deleting" ;  
        Write-Host "Line $(CurrentLine)  Executing                    Remove-Item -Recurse -Force ${INSTALL_FOLDER_PATH}\${CORE_NAME}" ;  
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
        Remove-Item -Recurse -Force ${INSTALL_FOLDER_PATH}\${CORE_NAME}
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Arduino STM32 Core           is not installed ...  continuing" ;  
    }

    Write-Host "Line $(CurrentLine)  Executing                    Expand-Archive -Path ${DLOAD_PATH}\${ARDUINO_CORE_ZIP_NAME} -DestinationPath ${INSTALL_FOLDER_PATH}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Expand-Archive -Path ${DLOAD_PATH}\${ARDUINO_CORE_ZIP_NAME} -DestinationPath ${INSTALL_FOLDER_PATH}

#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Renaming                     ${INSTALL_FOLDER_PATH}\${ARDUINO_CORE_NAME}";
    Write-Host "Line $(CurrentLine)  To                           ${INSTALL_FOLDER_PATH}\${CORE_NAME}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Rename-Item  ${INSTALL_FOLDER_PATH}\${ARDUINO_CORE_NAME}  ${INSTALL_FOLDER_PATH}\${CORE_NAME}
		
    Write-Host "Line $(CurrentLine)  Leaving                      install_arduino_stm32_core";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}


#---------------------------------------------------------
# install_arduino_cmake_project
#---------------------------------------------------------
function install_arduino_cmake_project
{
    Write-Host "Line $(CurrentLine)  Entering                     install_arduino_cmake_project";

    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${WORKSPACE_PATH}\${PROJECT_NAME})";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${WORKSPACE_PATH}\${PROJECT_NAME})
    {
        Write-Host "Line $(CurrentLine)  Project                      Exists ... deleting" ;  
        Write-Host "Line $(CurrentLine)  Executing                    Remove-Item -Recurse -Force ${WORKSPACE_PATH}\${PROJECT_NAME}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
        Remove-Item -Recurse -Force ${WORKSPACE_PATH}\${PROJECT_NAME}
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Project                      does not exist ... continuing" ;  
    }
    Write-Host "Line $(CurrentLine)  Installing                   ${WORKSPACE_PATH}\${PROJECT_NAME}";
    Write-Host "Line $(CurrentLine)  Executing                    Invoke-WebRequest -Uri ${PROJECT_URI} -Outfile ${DLOAD_PATH}\${PROJECT_ZIP_NAME}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Invoke-WebRequest -Uri ${PROJECT_URI} -Outfile ${DLOAD_PATH}\${PROJECT_ZIP_NAME}
    Write-Host "Line $(CurrentLine)  Executing                    Expand-Archive -Force -Path ${DLOAD_PATH}\${PROJECT_ZIP_NAME} -DestinationPath ${WORKSPACE_PATH}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Expand-Archive -Force -Path ${DLOAD_PATH}\${PROJECT_ZIP_NAME} -DestinationPath ${WORKSPACE_PATH}

    Write-Host "Line $(CurrentLine)  Renaming                     ${WORKSPACE_PATH}\${PROJECT_UNZIPPED_NAME}";
    Write-Host "Line $(CurrentLine)  To                           ${WORKSPACE_PATH}\${PROJECT_NAME}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Rename-Item  ${WORKSPACE_PATH}\${PROJECT_UNZIPPED_NAME}  ${WORKSPACE_PATH}\${PROJECT_NAME}

    Write-Host "Line $(CurrentLine)  Leaving                      install_arduino_cmake_project";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}


#---------------------------------------------------------
# check_environment
#---------------------------------------------------------
function check_environment
{
    Write-Host "Line $(CurrentLine)  Entering                     check_environment";

    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${INSTALL_FOLDER_PATH})";
    if (Test-Path ${INSTALL_FOLDER_PATH})
    {
        Write-Host "Line $(CurrentLine)  INSTALL Folder Path          Exists ... continuing" ;  
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Creating                     ${INSTALL_FOLDER_PATH}";
        Write-Host "Line $(CurrentLine)  Executing                    md ${INSTALL_FOLDER_PATH}";
        md ${INSTALL_FOLDER_PATH}
    }

    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${DLOAD_PATH})";
    if (Test-Path ${DLOAD_PATH})
    {
        Write-Host "Line $(CurrentLine)  dload folser                 Exists ... continuing" ;  
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Creating                     ${DLOAD_PATH}";
        Write-Host "Line $(CurrentLine)  Executing                    md $DLOAD_PATH}";
        md ${DLOAD_PATH}
    }
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${TOOLS_PATH})";
    if (Test-Path ${TOOLS_PATH})
    {
        Write-Host "Line $(CurrentLine)  tools folser                 Exists ... continuing" ;  
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Creating                     ${TOOLS_PATH}";
        Write-Host "Line $(CurrentLine)  Executing                    md $TOOLS_PATH}";
        md ${TOOLS_PATH}
    }

    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${WORKSPACE_PATH})";
    if (Test-Path ${WORKSPACE_PATH})
    {
        Write-Host "Line $(CurrentLine)  Projects Folder              Exists ... continuing" ;  
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Creating                     ${WORKSPACE_PATH}";
        Write-Host "Line $(CurrentLine)  Executing                    md ${WORKSPACE_PATH}";
        md ${WORKSPACE_PATH}
    }

    Write-Host "Line $(CurrentLine)  Leaving                      check_environment";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}


#---------------------------------------------------------
# Setup OS Stuff
#---------------------------------------------------------
function setup_os_stuff
{
    Write-Host "Line $(CurrentLine)  Entering                     setup_os_stuff"
	if(${OS} -eq "WIN")
    { 
        Write-Host "Line $(CurrentLine)  OS                         = Windows";            
        Write-Host "Line $(CurrentLine)  SLASH                      = \";
        Write-Host "Line $(CurrentLine)  ARDUINO_VERSION            = ${ARDUINO_VERSION}";
        Write-Host "Line $(CurrentLine)  SLASH                      = \";
        \ = "\";
        Write-Host "Line $(CurrentLine)  SLASH                      = \";
        ${BASE_ROOT} = "${DRIVE}:\"	
        Write-Host "Line $(CurrentLine)  BASE_ROOT                  = ${BASE_ROOT}";
    }
    elseIf(${OS} -eq "LIN")
    {
        Write-Host "Line $(CurrentLine)  OS                         = Linux";            
    }
    ElseIf(${OS} -eq "WSL" )
    {
        Write-Host "Line $(CurrentLine)  OS                         = WSL";            
    }
    Else
    {
        Write-Host "Line $(CurrentLine)  OS                         = Not Defined";            
    }
    Write-Host "Line $(CurrentLine)  Leaving                      setup_os_stuff"
}

#---------------------------------------------------------
# Launch_Arduino_IDE
#---------------------------------------------------------
function Launch_Arduino_IDE
{
    Write-Host "Line $(CurrentLine)  Entering                  Launch_Arduino_IDE"

    Write-Host "Line $(CurrentLine)  Executing                 $ARDUINO_PATH\arduino "
#Read-Host -Prompt "Pausing:  Press any key to continue"
    & $ARDUINO_PATH\arduino
    Write-Host "Line $(CurrentLine)  Leaving                   Launch_Arduino_IDE"
}

#---------------------------------------------------------
# CurrentLine
#---------------------------------------------------------
function CurrentLine
{
    $MyInvocation.ScriptLineNumber
}


#---------------------------------------------------------
# Main Entry point
#---------------------------------------------------------
Write-Host "Line $(CurrentLine)  Calling                      main()"
main
Write-Host "Line $(CurrentLine)  All Done  To exit Press Continue"
Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"



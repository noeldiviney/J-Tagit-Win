#!"C:\Program Files\PowerShell\7\pwsh.exe" -ExecutionPolicy Bypass
#--------------------------------------------------------------
#    Desktop Shortcut Example
#    W:\ProgramFiles\PowerShell\7\pwsh.exe -ExecutionPolicy Bypass -File W:\DinRDuino\PwrShell\arduino_ide.ps1 W DinRDuino 1.8.15 Eicon BluePill F103C8T6 SerLed V1.0.1 stm32 swd
#
#    Launching the Arduino IDE in a fully configured way.
#    This Pwershell Script uses forward declarations as follows.
#
#    #---------------------------------------------------------
#    # Main Function
#    #---------------------------------------------------------
#    function main
#    {
#        function_1        # Calling function_1
#        function_2        # Calling function_2
#    }
#    #---------------------------------------------------------
#    # Function 1
#    #---------------------------------------------------------
#    function function_1
#    {
#        do something
#    }
#    #---------------------------------------------------------
#    # Function 2
#    #---------------------------------------------------------
#    function function_1
#    {
#        do something else
#    }
#    #---------------------------------------------------------
#    # Main      The script entry point
#    #---------------------------------------------------------
#    main                      # Call function_main
#--------------------------------------------------------------

#---------------------------------------------------------
# Global Parameters
#---------------------------------------------------------

$OS_UC                         = "WIN"
$OS_LC                         = "win"
$OS_C                          = "Win"
$DRIVE                         = "C"
$SLASH                         = "\"
$INSTALL_FOLDER                = "J-Tagit"
$PROJECT_FOLDER                = "Projects"
$PROJECT_NAME                  = "F103CBT6-BluePill-HelloW"
$PROJECT_RELEASE               = "2.2.0"
$STM32_CORE_NAME               = "Core_STM32"
$ARDUINO_VERSION               = "1.8.18"
$ARDUINO_LIBRARIES_RELEASE     = "1.0.1"
$ARDUINO_CLI_RELEASE           = "0.20.2"
$BOARD_VENDOR_EI               = "Eicon"
$BOARD_VENDOR_EI_LC            = "eicon"
$BOARD_VENDOR_ST               = "STMicroelectronics"
$SKETCHBOOK_RELEASE            = "0.1.0"
$OPENOCD_RELEASE               = "0.11.0-1"
$OPENOCD_CFG_RELEASE           = "0.1.0"
$ARM_GCC_VERSION               = "10.1.0"
$EICON_GCC_VERSION             = "5.4.1"
$CMSIS_VERSION                 = "5.7.0"
$EICON_STM32_VARIANTS_NAME     = "eicon_win_stm32_variants"

$PROJECT_URI                   = "https://gitlab.com/noeldiviney/${PROJECT_NAME}-${OS_C}/-/archive/${PROJECT_RELEASE}/${PROJECT_NAME}-${OS_C}-${PROJECT_RELEASE}.zip"
$VARIANTS_URI                  = "https://gitlab.com/noeldiviney/${BOARD_VENDOR_EI_LC}_${OS_LC}_stm32_variants/-/archive/${PROJECT_RELEASE}/${BOARD_VENDOR_EI_LC}_${OS_LC}_stm32_variants-${PROJECT_RELEASE}.zip"
$STM32_CORE_ZIP_URI            = "https://github.com/stm32duino/Arduino_${STM32_CORE_NAME}/archive/refs/tags/${PROJECT_RELEASE}.zip"
## https://github.com/stm32duino/Arduino_Core_STM32/archive/refs/tags/2.2.0.zip
$PROJECT_ZIP_NAME              = "${PROJECT_NAME}-${OS_C}-${PROJECT_RELEASE}.zip"
$PROJECT_UNZIPPED_NAME         = "${PROJECT_NAME}-${OS_C}-${PROJECT_RELEASE}"
$ARDUINO_STM32_CORE_ZIP_NAME   = "Arduino_${STM32_CORE_NAME}-${PROJECT_RELEASE}.zip"
$ARDUINO_STM32_CORE_NAME       = "Arduino_${STM32_CORE_NAME}-${PROJECT_RELEASE}"
$VARIANTS_ZIP_NAME             = "${BOARD_VENDOR_EI_LC}_${OS_LC}_stm32_variants-${PROJECT_RELEASE}.zip"
$VARIANTS_NAME                 = "${BOARD_VENDOR_EI_LC}_${OS_LC}_stm32_variants-${PROJECT_RELEASE}"

$OPENOCD_TB_TYPE               = "win32-x64.zip"
$ARDUINO_LIBRARIES_PREFIX      = "${BOARD_VENDOR_EI_LC}_${OS_LC}_arduino_libraries"
$SKETCHBOOK_NAME_PREFIX        = "${BOARD_VENDOR_EI_LC}_${OS_LC}_sketchbook"
$XPACK_GCC_NAME                = "xpack-arm-none-eabi-gcc"
$EICON_GCC_NAME                = "${BOARD_VENDOR_EI_LC}-arm-none-eabi-gcc"
$ARDUINO_CLI_TB_TYPE           = "Windows_64bit.zip"
$ARDUINO_CLI_TB_NAME           = "arduino-cli_${ARDUINO_CLI_RELEASE}_${ARDUINO_CLI_TB_TYPE}"
$ARDUINO_TB_TYPE               = "windows.zip"
$OPENOCD_CFG_SCRIPT_NAME       = "${BOARD_VENDOR_EI_LC}_${OS_LC}_openocd_cfg"

$BASE_PATH                     = "${DRIVE}:"
$INSTALL_PATH                  = "${BASE_PATH}\${INSTALL_FOLDER}"
$DLOAD_PATH                    = "${INSTALL_PATH}\dload" 
$PROJECT_PATH                  = "${BASE_PATH}\${INSTALL_FOLDER}\Projects"
$ARDUINO_PATH                  = "${INSTALL_PATH}\arduino-${ARDUINO_VERSION}"
$PORTABLE_PATH                 = "${ARDUINO_PATH}\portable"
$HARDWARE_PATH_EI              = "${PORTABLE_PATH}\packages\${BOARD_VENDOR_EI}\hardware"
$HARDWARE_PATH_ST              = "${PORTABLE_PATH}\packages\${BOARD_VENDOR_ST}\hardware"
$TOOLS_PATH_EI                 = "${PORTABLE_PATH}\packages\${BOARD_VENDOR_EI}\tools"
$TOOLS_PATH_ST                 = "${PORTABLE_PATH}\packages\${BOARD_VENDOR_ST}\tools"
$OPENOCD_PATH                  = "${INSTALL_PATH}\Openocd"
$PREFS_PATH                    = "${PORTABLE_PATH}"
$SKETCH_PATH                   = "${PORTABLE_PATH}\sketchbook\arduino"
##  https://gitlab.com/noeldiviney/eicon_lin_stm32_variants/-/archive/2.2.0/eicon_lin_stm32_variants-2.2.0.zip
##  https://gitlab.com/noeldiviney/F103CBT6-HelloW-Win/-/archive/2.2.0/F103CBT6-HelloW-Win-2.2.0.zip
$ARDUINO_CLI_URI               = "https://downloads.arduino.cc/arduino-cli/arduino-cli_${ARDUINO_CLI_RELEASE}_${ARDUINO_CLI_TB_TYPE}"
$OPENOCD_CFG_SCRIPT_URI        = "https://gitlab.com/noeldiviney/${OPENOCD_CFG_SCRIPT_NAME}/-/archive/${OPENOCD_CFG_RELEASE}/${OPENOCD_CFG_SCRIPT_NAME}-${OPENOCD_CFG_RELEASE}.tar.gz"
$ARDUINO_LIBRARIES_ZIP_URI     = "https://gitlab.com/noeldiviney/eicon_win_arduino_libraries/-/archive/${ARDUINO_LIBRARIES_RELEASE}/${ARDUINO_LIBRARIES_PREFIX}-${ARDUINO_LIBRARIES_RELEASE}.zip"
$ARDUINO_ZIP_URI               = "https://downloads.arduino.cc/arduino-${ARDUINO_VERSION}-${ARDUINO_TB_TYPE}"
$SKETCHBOOK_ZIP_URI            = "https://gitlab.com/noeldiviney/eicon_win_sketchbook/-/archive/${SKETCHBOOK_RELEASE}/eicon_win_sketchbook-${SKETCHBOOK_RELEASE}.zip"
$OPENOCD_TB_NAME               = "xpack-openocd-${OPENOCD_RELEASE}-${OPENOCD_TB_TYPE}"
$OPENOCD_TB_URI                = "https://github.com/xpack-dev-tools/openocd-xpack/releases/download/v${OPENOCD_RELEASE}/xpack-openocd-${OPENOCD_RELEASE}-${OPENOCD_TB_TYPE}"
$STM32_INDEX                   = "        https://gitlab.com/noeldiviney/eicon_win_packages/-/raw/main/package_eicon_stm32_index.json,"
$KINETIS_INDEX                 = "        https://gitlab.com/noeldiviney/eicon_win_packages/-/raw/main/package_eicon_kinetis_index.json,"
$STMicroelectronics_INDEX      = "        https://github.com/stm32duino/BoardManagerFiles/raw/main/package_stmicroelectronics_index.json"

$ARDUINO_ZIP_FILE              = "arduino-${ARDUINO_VERSION}.zip"
$ARDUINO_LIBRARIES_ZIP_FILE    = "eicon_win_arduino_libraries-${ARDUINO_LIBRARIES_RELEASE}.zip"
$SKETCH_NAME                   = "${BOARD_VENDOR}-${BOARD}-${CPU}-${SKETCH}-${SKETCH_VER}"
$USER                          = "$env:UserName"                                 # $env:VAR_NAME="VALUE"
$PROFILE_PATH                  = "/home/$env:USER"
$SCRIPT_PATH                   = "${BASE_ROOT}bin"
$OPENOCD_ZIP_FILE              = "eicon-openocd-${OPENOCD_RELEASE}.zip"
$ARDUINO_CLI_ZIP_FILE          = "arduino_cli.zip"
$SKETCHBOOK_ZIP_FILE           = "sketchbook.zip"
$SourceFileLocation            = "${BASE_ROOT}Program Files\Notepad++\notepad++.exe"

#Read-Host -Prompt "Pausing:  Press any key to continue"

#---------------------------------------------------------
# Main Function
#---------------------------------------------------------
function main
{
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------";
    Write-Host "Line $(CurrentLine)  Entering                     main"; 

    Write-Host "Line $(CurrentLine)  Calling                      echo_args";
    echo_args                                                                        # Calling echo_args
Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      check_environment";
    check_environment
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      install_arduino_cmake_project";
    install_arduino_cmake_project
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      install_arduino_stm32_core";
    install_arduino_stm32_core
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    
    Write-Host "Line $(CurrentLine)  Calling                      install_eicon_win_stm32_variants";
    install_eicon_win_stm32_variants
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"


#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
}

#---------------------------------------------------------
# install_DesktopShortcut
#---------------------------------------------------------
function install_DesktopShortcut
{
#$WScriptShell = New-Object -ComObject WScript.Shell
    Write-Host "Line $(CurrentLine)  Entering                     install_DesktopShortcut";
#    $SourceFileLocation = "C:\Program Files\PowerShell\7\pwsh.exe"
#    $ShortcutLocation = "C:\Users\$USER\Desktop\pwsh.exe.lnk"
#    $WScriptShell = New-Object -ComObject WScript.Shell
#    $Shortcut = "$WScriptShell.CreateShortcut($ShortcutLocation)" 
#    $Shortcut.TargetPath = $SourceFileLocation "C:\Program Files\PwrShell\arduino-install.ps1" "W Dinrduino5 1.8.15 Eicon"
#    $Shortcut.Save()
#    Write-Host "Line $(CurrentLine)  Executing                 $ShortcutLocation = "C:\Users\$USER\Desktop\Notepad++.lnk";
#    $ShortcutLocation = "C:\Users\$USER\Desktop\Notepad++.lnk"
#    Write-Host "Line $(CurrentLine)  Executing                 $WScriptShell = New-Object -ComObject WScript.Shell";                 
#    $WScriptShell = New-Object -ComObject WScript.Shell
#    Write-Host "Line $(CurrentLine)  Executing                 '$Shortcut = $WScriptShell.CreateShortcut($ShortcutLocation)'";
#    $Shortcut = $WScriptShell.CreateShortcut($ShortcutLocation)
#    Write-Host "Line $(CurrentLine)  Executing                 $Shortcut.TargetPath = $SourceFileLocation";
#    $Shortcut.TargetPath = $SourceFileLocation
#    Write-Host "Line $(CurrentLine)  Executing                 $Shortcut.Save()";
#    $Shortcut.Save()
    Write-Host "Line $(CurrentLine)  Leaving                      install_DesktopShortcut";
}

#---------------------------------------------------------
# install_eicon_win_stm32_variants
#---------------------------------------------------------
function install_eicon_win_stm32_variants
{
    Write-Host "Line $(CurrentLine)  Entering                     install_eicon_win_stm32_variants";
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${PROJECT_PATH}\${PROJECT_NAME}\${STM32_CORE_NAME}\variants)";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${PROJECT_PATH}\${PROJECT_NAME}\${STM32_CORE_NAME}\variants)
    {
        Write-Host "Line $(CurrentLine)  variants                     exist  ...  deleting" ;  
        Write-Host "Line $(CurrentLine)  Executing                    Remove-Item -Recurse -Force ${PROJECT_PATH}\${PROJECT_NAME}\${STM32_CORE_NAME}\variants" ;  
        Remove-Item -Recurse -Force ${PROJECT_PATH}\${PROJECT_NAME}\${STM32_CORE_NAME}\variants
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    }
	else
    {
        Write-Host "Line $(CurrentLine)  variants                     do not exist  ...  continuing" ;  
    }


#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"


    if (Test-Path ${PROJECT_PATH}\${PROJECT_NAME}\${STM32_CORE_NAME}\boards.txt)
    {
        Write-Host "Line $(CurrentLine)  boards.txt                   exist  ...  deleting" ;  
        Write-Host "Line $(CurrentLine)  Executing                    Remove-Item -Recurse -Force ${PROJECT_PATH}\${PROJECT_NAME}\${STM32_CORE_NAME}\boards.txt" ;  
        Remove-Item -Recurse -Force ${PROJECT_PATH}\${PROJECT_NAME}\${STM32_CORE_NAME}\boards.txt
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    }
	else
    {
        Write-Host "Line $(CurrentLine)  boards.txt                   does not exist  ...  continuing" ;  
    }

    if (Test-Path ${PROJECT_PATH}\${PROJECT_NAME}\${STM32_CORE_NAME}\platform.txt)
    {
        Write-Host "Line $(CurrentLine)  platform.txt                 exist  ...  deleting" ;  
        Write-Host "Line $(CurrentLine)  Executing                    Remove-Item -Recurse -Force ${PROJECT_PATH}\${PROJECT_NAME}\${STM32_CORE_NAME}\platform.txt" ;  
        Remove-Item -Recurse -Force ${PROJECT_PATH}\${PROJECT_NAME}\${STM32_CORE_NAME}\platform.txt
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    }
	else
    {
        Write-Host "Line $(CurrentLine)  platform.txt                 does not exist  ...  continuing" ;  
    }

    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${DLOAD_PATH}\${VARIANTS_ZIP_NAME})";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${DLOAD_PATH}\${VARIANTS_ZIP_NAME})
    {
        Write-Host "Line $(CurrentLine)  variants zip                 exist  ...  deleting" ;  
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
        Write-Host "Line $(CurrentLine)  Executing                    Remove-Item -Recurse -Force ${DLOAD_PATH}\${EICON_WIN_STM32_VARIANTS_ZIP}" ;  
        Remove-Item -Recurse -Force ${DLOAD_PATH}\${EICON_WIN_STM32_VARIANT_ZIP}        
    }		
    Write-Host "Line $(CurrentLine)  Downloading                  ${DLOAD_PATH}\${VARIANTS_ZIP_NAME}";
    Write-Host "Line $(CurrentLine)  Executing                    Invoke-WebRequest -Uri ${VARIANTS_URI} -Outfile ${DLOAD_PATH}\${VARIANTS_ZIP_NAME}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Invoke-WebRequest -Uri ${VARIANTS_URI} -Outfile ${DLOAD_PATH}\${VARIANTS_ZIP_NAME}
	
    Write-Host "Line $(CurrentLine)  Executing                    tar -xf ${DLOAD_PATH}\${VARIANTS_ZIP_NAME} -C ${DLOAD_PATH}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    tar -xf ${DLOAD_PATH}\${VARIANTS_ZIP_NAME} -C ${DLOAD_PATH}

    Write-Host "Line $(CurrentLine)  Executing                    Copy-Item -Recurse -Force -path: "${DLOAD_PATH}\${VARIANTS_NAME}\*" -destination: ${PROJECT_PATH}\${PROJECT_NAME}\${STM32_CORE_NAME}";
    Copy-Item -Recurse -Force -path: "${DLOAD_PATH}\${VARIANTS_NAME}\*" -destination: ${PROJECT_PATH}\${PROJECT_NAME}\${STM32_CORE_NAME}
	
    Write-Host "Line $(CurrentLine)  Leaving                      install_eicon_win_stm32_variants";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# install_arduino_stm32_core
#---------------------------------------------------------
function install_arduino_stm32_core
{
    Write-Host "Line $(CurrentLine)  Entering                     install_arduino_stm32_core";


    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${DLOAD_PATH}\${ARDUINO_STM32_CORE_ZIP_NAME})";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${DLOAD_PATH}\${ARDUINO_STM32_CORE_ZIP_NAME})
    {
        Write-Host "Line $(CurrentLine)  Arduino STM32 Core           Exists" ;  
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Arduino STM32 Core           does not exist  ...   downloading" ;  
        Write-Host "Line $(CurrentLine)  Downloading                  ${DLOAD_PATH}\${ARDUINO_STM32_CORE_ZIP_NAME}";
        Write-Host "Line $(CurrentLine)  Executing                    Invoke-WebRequest -Uri ${STM32_CORE_ZIP_URI} -Outfile ${DLOAD_PATH}\${ARDUINO_STM32_CORE_ZIP_NAME}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
        Invoke-WebRequest -Uri ${STM32_CORE_ZIP_URI} -Outfile ${DLOAD_PATH}\${ARDUINO_STM32_CORE_ZIP_NAME}
    }
	
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${PROJECT_PATH}\${PROJECT_NAME}\${STM32_CORE_NAME})";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${PROJECT_PATH}\${PROJECT_NAME}\${STM32_CORE_NAME})
    {
        Write-Host "Line $(CurrentLine)  Arduino STM32 Core           is installed  ...  deleting" ;  
        Write-Host "Line $(CurrentLine)  Executing                    Remove-Item -Recurse -Force ${PROJECT_PATH}\${PROJECT_NAME}\${STM32_CORE_NAME}" ;  
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
        Remove-Item -Recurse -Force ${PROJECT_PATH}\${PROJECT_NAME}\${STM32_CORE_NAME}
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Arduino STM32 Core                       is not installed ...  continuing" ;  
    }
		
    Write-Host "Line $(CurrentLine)  Executing                    tar -xf ${DLOAD_PATH}\${ARDUINO_STM32_CORE_ZIP_NAME} -C ${PROJECT_PATH}\${PROJECT_NAME}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    tar -xf ${DLOAD_PATH}\${ARDUINO_STM32_CORE_ZIP_NAME} -C ${PROJECT_PATH}\${PROJECT_NAME}

    Write-Host "Line $(CurrentLine)  Renaming                     ${PROJECT_PATH}\${PROJECT_NAME}\${ARDUINO_STM32_CORE_NAME}";
    Write-Host "Line $(CurrentLine)  To                           ${PROJECT_PATH}\${PROJECT_NAME}\${STM32_CORE_NAME}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Rename-Item  ${PROJECT_PATH}\${PROJECT_NAME}\${ARDUINO_STM32_CORE_NAME}  ${PROJECT_PATH}\${PROJECT_NAME}\${STM32_CORE_NAME}
		
    Write-Host "Line $(CurrentLine)  Leaving                      install_arduino_stm32_core";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# install_arduino_cmake_project
#---------------------------------------------------------
function install_arduino_cmake_project
{
    Write-Host "Line $(CurrentLine)  Entering                     install_arduino_cmake_project";

    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${PROJECT_PATH}\${PROJECT_NAME})";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    if (Test-Path ${PROJECT_PATH}\${PROJECT_NAME})
    {
        Write-Host "Line $(CurrentLine)  Project                      Exists ... deleting" ;  
        Write-Host "Line $(CurrentLine)  Executing                    Remove-Item -Recurse -Force ${PROJECT_PATH}\${PROJECT_NAME}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
        Remove-Item -Recurse -Force ${PROJECT_PATH}\${PROJECT_NAME}
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Project                      does not exist ... continuing" ;  
    }
    Write-Host "Line $(CurrentLine)  Installing                   ${PROJECT_PATH}\${PROJECT_NAME}";
    Write-Host "Line $(CurrentLine)  Executing                    Invoke-WebRequest -Uri ${PROJECT_URI} -Outfile ${DLOAD_PATH}\${PROJECT_ZIP_NAME}";
    Invoke-WebRequest -Uri ${PROJECT_URI} -Outfile ${DLOAD_PATH}\${PROJECT_ZIP_NAME}
    Write-Host "Line $(CurrentLine)  Executing                    Expand-Archive -Force -Path ${DLOAD_PATH}\${PROJECT_ZIP_NAME} -DestinationPath ${PROJECT_PATH}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Expand-Archive -Force -Path ${DLOAD_PATH}\${PROJECT_ZIP_NAME} -DestinationPath ${PROJECT_PATH}

    Write-Host "Line $(CurrentLine)  Renaming                     ${PROJECT_PATH}\${PROJECT_UNZIPPED_NAME}";
    Write-Host "Line $(CurrentLine)  To                           ${PROJECT_PATH}\${PROJECT_NAME}";
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Rename-Item  ${PROJECT_PATH}\${PROJECT_UNZIPPED_NAME}  ${PROJECT_PATH}\${PROJECT_NAME}

    Write-Host "Line $(CurrentLine)  Leaving                      install_arduino_cmake_project";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}


#---------------------------------------------------------
# check_environment
#---------------------------------------------------------
function check_environment
{
    Write-Host "Line $(CurrentLine)  Entering                     check_environment";

    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${INSTALL_PATH})";
    if (Test-Path ${INSTALL_PATH})
    {
        Write-Host "Line $(CurrentLine)  INSTALL Path                 Exists ... continuing" ;  
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Creating                     ${INSTALL_PATH}";
        Write-Host "Line $(CurrentLine)  Executing                    md ${INSTALL_PATH}";
        md ${INSTALL_PATH}
    }

    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${DLOAD_PATH})";
    if (Test-Path ${DLOAD_PATH})
    {
        Write-Host "Line $(CurrentLine)  dload folser                 Exists ... continuing" ;  
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Creating                     ${DLOAD_PATH}";
        Write-Host "Line $(CurrentLine)  Executing                    md $DLOAD_PATH}";
        md ${DLOAD_PATH}
    }

    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${PROJECT_PATH})";
    if (Test-Path ${PROJECT_PATH})
    {
        Write-Host "Line $(CurrentLine)  Projects Folder              Exists ... continuing" ;  
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Creating                     ${PROJECT_PATH}";
        Write-Host "Line $(CurrentLine)  Executing                    md ${PROJECT_PATH}";
        md ${PROJECT_PATH}
    }

    Write-Host "Line $(CurrentLine)  Leaving                      check_environment";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# Echo Args
#---------------------------------------------------------
function echo_args
{
$MyVariable = 1

    Write-Host "Line $(CurrentLine)  Entering                        echo_args";
    Write-Host "Line $(CurrentLine)  OS                            = ${OS}";
    Write-Host "Line $(CurrentLine)  OS_C                          = ${OS_C}";
    Write-Host "Line $(CurrentLine)  OS_LC                         = ${OS_LC}";
    Write-Host "Line $(CurrentLine)  DRIVE                         = ${DRIVE}";
    Write-Host "Line $(CurrentLine)  SLASH                         = ${SLASH}";
    Write-Host "Line $(CurrentLine)  BASE_PATH                     = ${BASE_PATH}";
    Write-Host "Line $(CurrentLine)  INSTALL_FOLDER                = ${INSTALL_FOLDER}";	
    Write-Host "Line $(CurrentLine)  PROJECT_FOLDER                = ${PROJECT_FOLDER}";	
    Write-Host "Line $(CurrentLine)  PROJECT_NAME                  = ${PROJECT_NAME}";	
    Write-Host "Line $(CurrentLine)  PROJECT_RELEASE               = ${PROJECT_RELEASE}";	
    Write-Host "Line $(CurrentLine)  ARDUINO_VERSION               = ${ARDUINO_VERSION}";
    Write-Host "Line $(CurrentLine)  ARDUINO_LIBRARIES_RELEASE     = $ARDUINO_LIBRARIES_RELEASE";	
    Write-Host "Line $(CurrentLine)  ARDUINO_CLI_RELEASE           = ${ARDUINO_CLI_RELEASE}";            
    Write-Host "Line $(CurrentLine)  BOARD_VENDOR_EI               = ${BOARD_VENDOR_EI}";            
    Write-Host "Line $(CurrentLine)  BOARD_VENDOR_EI_LC            = ${BOARD_VENDOR_EI_LC}";            
    Write-Host "Line $(CurrentLine)  BOARD_VENDOR_ST               = ${BOARD_VENDOR_ST}";            
    Write-Host "Line $(CurrentLine)  SKETCHBOOK_RELEASE            = ${SKETCHBOOK_RELEASE}";            
    Write-Host "Line $(CurrentLine)  OPENOCD_RELEASE               = ${OPENOCD_RELEASE}";            
    Write-Host "Line $(CurrentLine)  OPENOCD_CFG_SCRIPT_NAME       = ${OPENOCD_CFG_SCRIPT_NAME}";            
    Write-Host "Line $(CurrentLine)  OPENOCD_CFG_RELEASE           = ${OPENOCD_CFG_RELEASE}";            
    Write-Host "Line $(CurrentLine)  ARM_GCC_VERSION               = ${ARM_GCC_VERSION}";            
    Write-Host "Line $(CurrentLine)  EICON_GCC_VERSION             = ${EICON_GCC_VERSION}";            
    Write-Host "Line $(CurrentLine)  CMSIS_VERSION                 = ${CMSIS_VERSION}";            
    Write-Host "Line $(CurrentLine)  EICON_STM32_VARIANTS_NAME     = ${EICON_STM32_VARIANTS_NAME}";            
    Write-Host "Line $(CurrentLine)";
    Write-Host "Line $(CurrentLine)  PROJECT_URI                   = ${PROJECT_URI}";
    Write-Host "Line $(CurrentLine)  VARIANTS_URI                  = ${VARIANTS_URI}";
	Write-Host "Line $(CurrentLine)  STM32_CORE_ZIP_URI            = ${STM32_CORE_ZIP_URI}";
    Write-Host "Line $(CurrentLine)  PROJECT_ZIP_NAME              = ${PROJECT_ZIP_NAME}";
    Write-Host "Line $(CurrentLine)  PROJECT_UNZIPPED_NAME         = ${PROJECT_UNZIPPED_NAME}";
    Write-Host "Line $(CurrentLine)  ARDUINO_STM32_CORE_ZIP_NAME   = ${ARDUINO_STM32_CORE_ZIP_NAME}";
    Write-Host "Line $(CurrentLine)  ARDUINO_STM32_CORE_NAME       = ${ARDUINO_STM32_CORE_NAME}";
    Write-Host "Line $(CurrentLine)  VARIANTS_ZIP_NAME             = ${VARIANTS_ZIP_NAME}";
    Write-Host "Line $(CurrentLine)  VARIANTS_NAME                 = ${VARIANTS_NAME}";
    Write-Host "Line $(CurrentLine)";
    Write-Host "Line $(CurrentLine)  OPENOCD_TB_TYPE               = ${OPENOCD_TB_TYPE}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_LIBRARIES_PREFIX      = ${ARDUINO_LIBRARIES_PREFIX}";            
    Write-Host "Line $(CurrentLine)  SKETCHBOOK_NAME_PREFIX        = ${SKETCHBOOK_NAME_PREFIX}";            
    Write-Host "Line $(CurrentLine)  XPACK_GCC_NAME                = ${XPACK_GCC_NAME}";            
    Write-Host "Line $(CurrentLine)  EICON_GCC_NAME                = ${EICON_GCC_NAME}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_CLI_TB_TYPE           = ${ARDUINO_CLI_TB_TYPE}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_CLI_TB_NAME           = ${ARDUINO_CLI_TB_NAME}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_TB_TYPE               = ${ARDUINO_TB_TYPE}";            
    Write-Host "Line $(CurrentLine)  OPENOCD_CFG_SCRIPT_NAME       = ${OPENOCD_CFG_SCRIPT_NAME}";
    Write-Host "Line $(CurrentLine)";
    Write-Host "Line $(CurrentLine)  BASE_PATH                     = ${BASE_PATH}";
    Write-Host "Line $(CurrentLine)  INSTALL_PATH                  = ${INSTALL_PATH}";
    Write-Host "Line $(CurrentLine)  DLOAD_PATH                    = ${DLOAD_PATH}";	
    Write-Host "Line $(CurrentLine)  PROJECT_PATH                  = ${PROJECT_PATH}";	
    Write-Host "Line $(CurrentLine)  ARDUINO_PATH                  = ${ARDUINO_PATH}";            
    Write-Host "Line $(CurrentLine)  PORTABLE_PATH                 = ${PORTABLE_PATH}";            
    Write-Host "Line $(CurrentLine)  HARDWARE_PATH_EI              = ${HARDWARE_PATH_EI}";            
    Write-Host "Line $(CurrentLine)  HARDWARE_PATH_ST              = ${HARDWARE_PATH_ST}";            
    Write-Host "Line $(CurrentLine)  TOOLS_PATH_EI                 = ${TOOLS_PATH_EI}";            
    Write-Host "Line $(CurrentLine)  TOOLS_PATH_ST                 = ${TOOLS_PATH_ST}";            
    Write-Host "Line $(CurrentLine)  PREFS_PATH                    = ${PREFS_PATH}";            
    Write-Host "Line $(CurrentLine)  SKETCH_PATH                   = ${SKETCH_PATH}";            
    Write-Host "Line $(CurrentLine)";
    Write-Host "Line $(CurrentLine)  ARDUINO_CLI_URI               = ${ARDUINO_CLI_URI}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_ZIP_URI               = ${ARDUINO_ZIP_URI}";            
    Write-Host "Line $(CurrentLine)  OPENOCD_TB_URI                = ${OPENOCD_TB_URI}";
    Write-Host "Line $(CurrentLine)  OPENOCD_CFG_SCRIPT_URI        = ${OPENOCD_CFG_SCRIPT_URI}";            
    Write-Host "Line $(CurrentLine)  SKETCHBOOK_ZIP_URI            = ${SKETCHBOOK_ZIP_URI}";            
    Write-Host "Line $(CurrentLine)";	
    Write-Host "Line $(CurrentLine)  ARDUINO_ZIP_FILE              = ${ARDUINO_ZIP_FILE}";            
    Write-Host "Line $(CurrentLine)  OPENOCD_TB_NANE               = ${OPENOCD_TB_NAME}";                    
    Write-Host "Line $(CurrentLine)  SKETCHBOOK_ZIP_FILE           = ${SKETCHBOOK_ZIP_FILE}";            
    Write-Host "Line $(CurrentLine)  USER                          = ${USER}";            
    Write-Host "Line $(CurrentLine)  Leaving                         echo_args";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# Launch_Arduino_IDE
#---------------------------------------------------------
function Launch_Arduino_IDE
{
    Write-Host "Line $(CurrentLine)  Entering                  Launch_Arduino_IDE"

    Write-Host "Line $(CurrentLine)  Executing                 ${ARDUINO_PATH}\arduino "
#Read-Host -Prompt "Pausing:  Press any key to continue"
    & ${ARDUINO_PATH}\arduino
    Write-Host "Line $(CurrentLine)  Leaving                   Launch_Arduino_IDE"
}

#---------------------------------------------------------
# CurrentLine
#---------------------------------------------------------
function CurrentLine
{
    $MyInvocation.ScriptLineNumber
}


#---------------------------------------------------------
# Main Entry point
#---------------------------------------------------------
Write-Host "Line $(CurrentLine)  Calling                      main()"
main
Write-Host "Line $(CurrentLine)  All Done  To exit Press Continue"
Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"

#---------------------------------------------------------
# install_sketchbook
#---------------------------------------------------------
function install_sketchbook
{
    Write-Host "Line $(CurrentLine)  Entering                     install_sketchbook";
    Write-Host "Line $(CurrentLine)  Downloading                  ${BASE_PATH}${SLASH}dload${SLASH}${SKETCHBOOK_ZIP_FILE}";
    Write-Host "Line $(CurrentLine)  Executing                    Invoke-WebRequest -Uri ${SKETCHBOOK_ZIP_URI} -Outfile ${BASE_PATH}${SLASH}dload${SLASH}${SKETCHBOOK_ZIP_FILE}";
    Invoke-WebRequest -Uri ${SKETCHBOOK_ZIP_URI} -Outfile ${BASE_PATH}${SLASH}dload${SLASH}${SKETCHBOOK_ZIP_FILE}
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${PORTABLE_PATH}${SLASH}sketchbook)";
    if (Test-Path ${PORTABLE_PATH}${SLASH}sketchbook)
    {
        Write-Host "Line $(CurrentLine)  sketchbook folder            Exists , Deleting";
        Remove-Item -Recurse -Force ${PORTABLE_PATH}${SLASH}sketchbook
    }	
    Write-Host "Line $(CurrentLine)  Executing                    Expand-Archive -PATH ${BASE_PATH}${SLASH}dload${SLASH}${SKETCHBOOK_ZIP_FILE} -Destination ${PORTABLE_PATH}${SLASH}";
    Expand-Archive -Force -PATH ${BASE_PATH}${SLASH}dload${SLASH}${SKETCHBOOK_ZIP_FILE} -Destination ${PORTABLE_PATH}${SLASH} 
    Write-Host "Line $(CurrentLine)  Renaming                     ${PORTABLE_PATH}${SLASH}${SKETCHBOOK_NAME_PREFIX}*";
    Write-Host "Line $(CurrentLine)  To                           ${PORTABLE_PATH}${SLASH}sketchbook";
    Rename-Item ${PORTABLE_PATH}${SLASH}${SKETCHBOOK_NAME_PREFIX}* ${PORTABLE_PATH}${SLASH}sketchbook
    Write-Host "Line $(CurrentLine)  Leaving                      install_sketchbook";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
#  reconfig_paths
#---------------------------------------------------------
function reconfig_paths
{
    Write-Host "Line $(CurrentLine)  Entering                     reconfig_paths";
    Write-Host "Line $(CurrentLine)  Exexcuting                   Get-ChildItem -Path ${TOOLS_PATH_EI}${SLASH}CMSIS${SLASH}*${SLASH} -Recurse | Move-Item  -Destination ${TOOLS_PATH_EI}${SLASH}CMSIS${SLASH} ";
    Get-ChildItem -Path ${TOOLS_PATH_EI}${SLASH}CMSIS${SLASH}*${SLASH} -Recurse | Move-Item -Force -Destination ${TOOLS_PATH_EI}${SLASH}CMSIS${SLASH}

    Write-Host "Line $(CurrentLine)  Exexcuting                   Get-ChildItem -Path ${TOOLS_PATH_EI}${SLASH}${XPACK_GCC_NAME}${SLASH}*${SLASH} -Recurse | Move-Item  -Destination ${TOOLS_PATH_EI}${SLASH}${XPACK_GCC_NAME}${SLASH} ";
    Get-ChildItem -Path ${TOOLS_PATH_EI}${SLASH}${XPACK_GCC_NAME}${SLASH}*${SLASH} -Recurse | Move-Item  -Force -Destination ${TOOLS_PATH_EI}${SLASH}${XPACK_GCC_NAME}${SLASH}

    Write-Host "Line $(CurrentLine)  Exexcuting                   Get-ChildItem -Path ${TOOLS_PATH_EI}${SLASH}${EICON_GCC_NAME}${SLASH}*${SLASH} -Recurse | Move-Item  -Destination ${TOOLS_PATH_EI}${SLASH}${EICON_GCC_NAME}${SLASH} ";
    Get-ChildItem -Path ${TOOLS_PATH_EI}${SLASH}${EICON_GCC_NAME}${SLASH}*${SLASH} -Recurse | Move-Item -Force -Destination ${TOOLS_PATH_EI}${SLASH}${EICON_GCC_NAME}${SLASH}
    Write-Host "Line $(CurrentLine)  Leaving                      reconfig_paths";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

function core_install_stmicro
{
    Write-Host "Line $(CurrentLine)  Entering                     core_install_stmicro";
    if (Test-Path ${ST_HARDWARE_PATH}${SLASH}stm32)
    {
        Write-Host "Line $(CurrentLine)  stmicro_core                 Exists , Deleting";
        Remove-Item -Recurse -Force ${ST_HARDWARE_PATH}${SLASH}stm32
        Remove-Item -Recurse -Force ${TOOLS_PATH}${SLASH}${EICON_GCC_NAME}
    }
    Write-Host "Line $(CurrentLine)  Executing                    arduino-cli core install ${BOARD_VENDOR_ST}:stm32";
    & $PORTABLE_PATH${SLASH}arduino-cli core install ${BOARD_VENDOR_ST}:stm32    
    Write-Host "Line $(CurrentLine)  Leaving                      core_install_micro";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}
#---------------------------------------------------------
# install_openocd_cfg_scripts
#---------------------------------------------------------
function install_openocd_cfg_scripts
{
    Write-Host "Line $(CurrentLine)  Entering                     install_openocd_cfg_scripts";
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${BASE_PATH}${SLASH}dload${SLASH}${OPENOCD_CFG_SCRIPT_NAME})";
    if (Test-Path ${BASE_PATH}${SLASH}dload${SLASH}${OPENOCD_CFG_SCRIPT_NAME})
    {
        Write-Host "Line $(CurrentLine)  openocd_cfg_scripts          Exists";   
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Downloading                  ${BASE_PATH}${SLASH}dload${SLASH}${OPENOCD_CFG_SCRIPT_TB_NAME}";
        Write-Host "Line $(CurrentLine)  Executing                    'Invoke-WebRequest -Uri ${OPENOCD_CFG_SCRIPT_URI} -Outfile ${BASE_PATH}${SLASH}dload${SLASH}${OPENOCD_CFG_SCRIPT_NAME}'";
        Invoke-WebRequest -Uri ${OPENOCD_CFG_SCRIPT_URI} -Outfile ${BASE_PATH}${SLASH}dload${SLASH}${OPENOCD_CFG_SCRIPT_NAME}
    }

    Write-Host "Line $(CurrentLine)  Executing                    tar -xf ${BASE_PATH}${SLASH}dload${SLASH}${OPENOCD_CFG_SCRIPT_NAME} -C ${BASE_PATH}${SLASH}dload${SLASH}";
    tar -xf ${BASE_PATH}${SLASH}dload${SLASH}${OPENOCD_CFG_SCRIPT_NAME} -C ${BASE_PATH}${SLASH}dload${SLASH}
    Write-Host "Line $(CurrentLine)  Executing                    Copy-Item -Recurse -Force -path: "${BASE_PATH}${SLASH}dload${SLASH}${OPENOCD_CFG_SCRIPT_NAME}-${OPENOCD_CFG_RELEASE}${SLASH}*" -destination: ${BASE_PATH}${SLASH}openocd${SLASH}";
    Copy-Item -Recurse -Force -path: "${BASE_PATH}${SLASH}dload${SLASH}${OPENOCD_CFG_SCRIPT_NAME}-${OPENOCD_CFG_RELEASE}${SLASH}*" -destination: ${BASE_PATH}${SLASH}openocd${SLASH}


    Write-Host "Line $(CurrentLine)  Leaving                      install_openocd_cfg_scripts";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# install_openocd-${OPENOCD_RELEASE}
#---------------------------------------------------------
function install_openocd-${OPENOCD_RELEASE}
{
    Write-Host "Line $(CurrentLine)  Entering                     install_openocd-${OPENOCD_RELEASE}";
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${BASE_PATH}${SLASH}dload${SLASH}${OPENOCD_TB_NAME})";
    if (Test-Path ${BASE_PATH}${SLASH}dload${SLASH}${OPENOCD_TB_NAME})
    {
        Write-Host "Line $(CurrentLine)  xpack-openocd                Exists";   
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Downloading                  ${BASE_PATH}${SLASH}dload${SLASH}${OPENOCD_TB_NAME}";
        Write-Host "Line $(CurrentLine)  Executing                    'Invoke-WebRequest -Uri ${OPENOCD_TB_URI} -Outfile ${BASE_PATH}${SLASH}dload${SLASH}${OPENOCD_TB_NAME}'";
        Invoke-WebRequest -Uri ${OPENOCD_TB_URI} -Outfile ${BASE_PATH}${SLASH}dload${SLASH}${OPENOCD_TB_NAME}
    }
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${BASE_PATH}${SLASH}openocd)";
    if (Test-Path ${BASE_PATH}${SLASH}openocd)
    {
        Write-Host "Line $(CurrentLine)  openocd                      is installed  removing it";
        Write-Host "Line $(CurrentLine)  Executing                    Remove-item -Recurse -Force ${BASE_PATH}${SLASH}openocd ";
        Remove-Item -Recurse -Force ${BASE_PATH}${SLASH}openocd
    }

    Write-Host "Line $(CurrentLine)  Executing                    tar -xf ${BASE_PATH}${SLASH}dload${SLASH}${OPENOCD_TB_NAME} -C ${BASE_PATH}${SLASH}dload";
    tar -xf ${BASE_PATH}${SLASH}dload${SLASH}${OPENOCD_TB_NAME} -C ${BASE_PATH}
    Write-Host "Line $(CurrentLine)  Renaming                     ${BASE_PATH}${SLASH}xpack-openocd-${OPENOCD_RELEASE}";
    Write-Host "Line $(CurrentLine)  To                           ${BASE_PATH}${SLASH}openocd";
    Rename-Item  ${BASE_PATH}${SLASH}xpack-openocd-${OPENOCD_RELEASE} ${BASE_PATH}${SLASH}openocd

    Write-Host "Line $(CurrentLine)  Leaving                      install_openocd-${OPENOCD_RELEASE}";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# install_arduino-libraries
#---------------------------------------------------------
function install_arduino-libraries
{
    Write-Host "Line $(CurrentLine)  Entering                     install_arduino-libraries";

    Write-Host "Line $(CurrentLine)  Downloading                  ${BASE_PATH}${SLASH}dload${SLASH}ARDUINO_LIBRARIES";
    Write-Host "Line $(CurrentLine)  Executing                    Invoke-WebRequest -Uri ${ARDUINO_LIBRARIES_ZIP_URI} -Outfile ${BASE_PATH}${SLASH}dload${SLASH}${ARDUINO_LIBRARIES_ZIP_FILE}";
    Invoke-WebRequest -Uri ${ARDUINO_LIBRARIES_ZIP_URI} -Outfile ${BASE_PATH}${SLASH}dload${SLASH}${ARDUINO_LIBRARIES_ZIP_FILE}
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${$ARDUINO_PATH}${SLASH}libraries)";
    if (Test-Path ${ARDUINO_PATH}${SLASH}libraries)
    {
        Write-Host "Line $(CurrentLine)  libraries folder            Exists , Deleting";
        Remove-Item -Recurse -Force ${ARDUINO_PATH}${SLASH}libraries
    }	
    Write-Host "Line $(CurrentLine)  Executing                    Expand-Archive -PATH ${BASE_PATH}${SLASH}dload${SLASH}${ARDUINO_LIBRARIES_ZIP_FILE} -Destination ${ARDUINO_PATH}${SLASH}";
    Expand-Archive -Force -PATH ${BASE_PATH}${SLASH}dload${SLASH}${ARDUINO_LIBRARIES_ZIP_FILE} -Destination ${ARDUINO_PATH}${SLASH} 
    Write-Host "Line $(CurrentLine)  Renaming                     ${ARDUINO_PATH}${SLASH}${ARDUINO_LIBRARIES_PREFIX}-${ARDUINO_LIBRARIES_RELEASE}";
    Write-Host "Line $(CurrentLine)  To                           ${PORTABLE_PATH}${SLASH}libraries";
    Rename-Item ${ARDUINO_PATH}${SLASH}${ARDUINO_LIBRARIES_PREFIX}-${ARDUINO_LIBRARIES_RELEASE} ${ARDUINO_PATH}${SLASH}libraries
 
    Write-Host "Line $(CurrentLine)  Leaving                      install_arduino-libraries";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

function core_install_kinetis
{
    Write-Host "Line $(CurrentLine)  Entering                     core_install_kinetis";
    if (Test-Path ${EI_HARDWARE_PATH}${SLASH}kinetis)
    {
        Write-Host "Line $(CurrentLine)  kinetis_core                 Exists , Deleting";
        Remove-Item -Recurse -Force ${EI_HARDWARE_PATH}${SLASH}kinetis
        Remove-Item -Recurse -Force ${EI_TOOLS_PATH}${SLASH}${EICON_GCC_NAME}
    }
    Write-Host "Line $(CurrentLine)  Executing                    arduino-cli core install ${BOARD_VENDOR_EI}:kinetis";
    & $PORTABLE_PATH${SLASH}arduino-cli core install ${BOARD_VENDOR_EI}:kinetis    
    Write-Host "Line $(CurrentLine)  Leaving                      core_install_kinetis";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

function core_install_stm32
{
    Write-Host "Line $(CurrentLine)  Entering                     core_install_stm32";
    if (Test-Path ${EI_HARDWARE_PATH}${SLASH}stm32)
    {
        Write-Host "Line $(CurrentLine)  stm32_core                   Exists , Deleting";
        Remove-Item -Recurse -Force ${EI_HARDWARE_PATH}${SLASH}stm32
        Remove-Item -Recurse -Force ${EI_TOOLS_PATH}${SLASH}CMSIS
        Remove-Item -Recurse -Force ${EI_TOOLS_PATH}${SLASH}${XPACK_GCC_NAME}
    }
    Write-Host "Line $(CurrentLine)  Executing                    arduino-cli core install ${BOARD_VENDOR_EI}:stm32";
    & $PORTABLE_PATH${SLASH}arduino-cli core install ${BOARD_VENDOR_EI}:stm32    
    Write-Host "Line $(CurrentLine)  Leaving                      core_install_stm32";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# core_update-index
#---------------------------------------------------------
function core_update-index
{
    Write-Host "Line $(CurrentLine)  Entering                     core_update-index";
    Write-Host "Line $(CurrentLine)  Executing                    cd ${PORTABLE_PATH}";
    cd ${PORTABLE_PATH}
    Write-Host "Line $(CurrentLine)  Current Working Dir        = $PWD";
    Write-Host "Line $(CurrentLine)  Executing                    arduino-cli core update-index";
    & $PORTABLE_PATH${SLASH}arduino-cli core update-index    
    Write-Host "Line $(CurrentLine)  Leaving                      core_update-index";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}
#---------------------------------------------------------
# install_arduino_cli_config
#---------------------------------------------------------
function install_arduino_cli_config
{
    Write-Host "Line $(CurrentLine)  Entering                     install_arduino_cli_config";

    if (Test-Path ${PORTABLE_PATH}${SLASH}arduino-cli.yaml)
    {
        Write-Host "Line $(CurrentLine)  arduino-cli.yaml             Exists , Deleting";
        Remove-Item ${PORTABLE_PATH}${SLASH}arduino-cli.yaml
        Write-Host "Line $(CurrentLine)  Executing                    New-Item ${PORTABLE_PATH}${SLASH}arduino-cli.yaml";
        New-Item ${PORTABLE_PATH}${SLASH}arduino-cli.yaml
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Executing                    New-Item ${PORTABLE_PATH}${SLASH}arduino-cli.yaml";
        New-Item ${PORTABLE_PATH}${SLASH}arduino-cli.yaml
    }    
    Write-Host "Line $(CurrentLine)  Adding                       arduino-cli.yaml Contents";
    Add-Content ${PORTABLE_PATH}${SLASH}arduino-cli.yaml 'board_manager:'
    Add-Content ${PORTABLE_PATH}${SLASH}arduino-cli.yaml '    additional_urls: ['
    Add-Content ${PORTABLE_PATH}${SLASH}arduino-cli.yaml          ${STM32_INDEX}
    Add-Content ${PORTABLE_PATH}${SLASH}arduino-cli.yaml          ${KINETIS_INDEX}
    Add-Content ${PORTABLE_PATH}${SLASH}arduino-cli.yaml          ${STMicroelectronics_INDEX}
    Add-Content ${PORTABLE_PATH}${SLASH}arduino-cli.yaml '    ]'
    Add-Content ${PORTABLE_PATH}${SLASH}arduino-cli.yaml 'daemon:'
    Add-Content ${PORTABLE_PATH}${SLASH}arduino-cli.yaml '    port: "50051"'
    Add-Content ${PORTABLE_PATH}${SLASH}arduino-cli.yaml 'directories:'
    Add-Content ${PORTABLE_PATH}${SLASH}arduino-cli.yaml '    data: ' -NoNewLine
    Add-Content ${PORTABLE_PATH}${SLASH}arduino-cli.yaml             ${PORTABLE_PATH}
    Add-Content ${PORTABLE_PATH}${SLASH}arduino-cli.yaml '    downloads: ' -NoNewLine
    Add-Content ${PORTABLE_PATH}${SLASH}arduino-cli.yaml             ${PORTABLE_PATH}${SLASH}staging
    Add-Content ${PORTABLE_PATH}${SLASH}arduino-cli.yaml '    user: ' -NoNewLine
    Add-Content ${PORTABLE_PATH}${SLASH}arduino-cli.yaml             ${PORTABLE_PATH}${SLASH}Workspace
    Add-Content ${PORTABLE_PATH}${SLASH}arduino-cli.yaml 'logging:'
    Add-Content ${PORTABLE_PATH}${SLASH}arduino-cli.yaml '    file: ""'
    Add-Content ${PORTABLE_PATH}${SLASH}arduino-cli.yaml '    format: text'
    Add-Content ${PORTABLE_PATH}${SLASH}arduino-cli.yaml '    level: info'
    Write-Host "Line $(CurrentLine)  Editing                      arduino-cli.yaml Completed";
    Write-Host "Line $(CurrentLine)  Leaving                      install_arduino_cli_config";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}
#---------------------------------------------------------
# install_arduino_cli
#---------------------------------------------------------
function install_arduino_cli
{
    Write-Host "Line $(CurrentLine)  Entering                     install_arduino_cli";
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${PORTABLE_PATH})";
    if (Test-Path ${PORTABLE_PATH})
    {
        Write-Host "Line $(CurrentLine)  portable folder              Exists";   
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Executing                    md ${PORTABLE_PATH}";
        md ${PORTABLE_PATH}
    }
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${BASE_PATH}${SLASH}dload${SLASH}${ARDUINO_CLI_TB_NAME})";
    if (Test-Path ${BASE_PATH}${SLASH}dload${SLASH}${ARDUINO_CLI_TB_NAME})
    {
        Write-Host "Line $(CurrentLine)  arduino-cli                  Exists";   
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Downloading                  ${BASE_PATH}${SLASH}dload${SLASH}${ARDUINO_CLI_TB_NAME})";
        Write-Host "Line $(CurrentLine)  Executing                    Invoke-WebRequest -Uri ${ARDUINO_CLI_URI} -Outfile ${BASE_PATH}${SLASH}dload${SLASH}${ARDUINO_CLI_TB_NAME}";
        Invoke-WebRequest -Uri ${ARDUINO_CLI_URI} -Outfile ${BASE_PATH}${SLASH}dload${SLASH}${ARDUINO_CLI_TB_NAME}
    }
    Write-Host "Line $(CurrentLine)  Executing                    tar -xf ${BASE_PATH}${SLASH}dload${SLASH}${ARDUINO_CLI_TB_NAME} -C ${PORTABLE_PATH}";
    tar -xf ${BASE_PATH}${SLASH}dload${SLASH}${ARDUINO_CLI_TB_NAME} -C ${PORTABLE_PATH}

    Write-Host "Line $(CurrentLine)  Leaving                      install_arduino_cli";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

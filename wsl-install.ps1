#!C:\PgmFiles\PowerShell\7\pwsh.exe -ExecutionPolicy Bypass
#--------------------------------------------------------------
#    Desktop Shortcut Example
#    C:\PgmFiles\PowerShell\7\pwsh.exe -ExecutionPolicy Bypass -File W:\DinRDuino\PwrShell\arduino_ide.ps1 W DinRDuino 1.8.15 Eicon BluePill F103C8T6 SerLed V1.0.1 stm32 swd
#
#    Launching the Arduino IDE in a fully configured way.
#    This Pwershell Script uses forward declarations as follows.
#
#    #---------------------------------------------------------
#    # Main Function
#    #---------------------------------------------------------
#    function main
#    {
#        function_1        # Calling function_1
#        function_2        # Calling function_2
#    }
#    #---------------------------------------------------------
#    # Function 1
#    #---------------------------------------------------------
#    function function_1
#    {
#        do something
#    }
#    #---------------------------------------------------------
#    # Function 2
#    #---------------------------------------------------------
#    function function_1
#    {
#        do something else
#    }
#    #---------------------------------------------------------
#    # Main      The script entry point
#    #---------------------------------------------------------
#    main                      # Call function_main
#--------------------------------------------------------------

#---------------------------------------------------------
# Global Parameters
#---------------------------------------------------------

param([string]$DRIVE  = "DRIVE", 
      [string]$FOLDER = "FOLDER",
      [string]$DISTRO = "DISTRO",
      [string]$VENDOR = "VENDOR");

#Read-Host -Prompt "Pausing:  Press any key to continue"

$BASE_PATH                 = "${DRIVE}:\$FOLDER}"
$USER                      = "$env:UserName"                                 # $env:VAR_NAME="VALUE"
$PROFILE_PATH              = "/home/$env:USER"
$NOTEPAD_EDITOR            = "C:\Program Files\Notepad++\notepad++.exe"
#Read-Host -Prompt "Pausing:  Press any key to continue"

#---------------------------------------------------------
# Main Function
#---------------------------------------------------------
function main
{
    Write-Host "Line $(CurrentLine)   Entering                     main";    

    Write-Host "Line $(CurrentLine)   Calling                      echo_args";
    echo_args                                                                        # Calling echo_args
Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)   Calling                      install_WSL";
    install_WSL
Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    
    Write-Host "Line $(CurrentLine)   ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# install_WSL
#---------------------------------------------------------
function install_WSL
{
    Write-Host "Line $(CurrentLine)   Entering                     install_WSL";

    Write-Host "Line $(CurrentLine)   Executing                    wsl --install -d ${DISTRO} ";                    ### Instal Ubuntu distro
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    wsl --install -d ${DISTRO}	

    Write-Host "Line $(CurrentLine)   Executing                    Restart-Computer ";
    Restart-Computer
    Write-Host "Line $(CurrentLine)   Leaving                      install_WSL";
}
#---------------------------------------------------------
# create_base_path
#---------------------------------------------------------
function create_base_path
{
    Write-Host "Line $(CurrentLine)  Entering                     create_base_path";
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${BASE_PATH})";
    if (Test-Path ${BASE_PATH})
    {
        Write-Host "Line $(CurrentLine)  ${BASE_PATH}                 Exists" ;  
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Creating                     ${BASE_PATH}";
        Write-Host "Line $(CurrentLine)  Executing                    md ${BASE_PATH}";
        Write-Host "Line $(CurrentLine)   ----------------------------------------------------------------------------------------";
        md ${BASE_PATH}
        Write-Host "Line $(CurrentLine)   ----------------------------------------------------------------------------------------";
    }
    cd ${BASE_PATH}
    Write-Host "Line $(CurrentLine)  Current Directory          = $PWD";
    Write-Host "Line $(CurrentLine)  Leaving                      create_${BASE_PATH}";
}

#---------------------------------------------------------
# Echo Args
#---------------------------------------------------------
function echo_args
{
$MyVariable = 1

    Write-Host "Line $(CurrentLine)  Entering                     echo_args";
    Write-Host "Line $(CurrentLine)  Arguments                    as follows";
    Write-Host "";
    Write-Host "Line $(CurrentLine)  DRIVE                      = ${DRIVE} ";
    Write-Host "Line $(CurrentLine)  FOLDER                     = ${FOLDER} ";
    Write-Host "Line $(CurrentLine)  DISTRO                     = ${DISTRO} ";
    Write-Host "Line $(CurrentLine)  VENDOR                     = ${VENDOR} ";
    Write-Host "";
    Write-Host "Line $(CurrentLine)  End of Arguments             ";
    Write-Host "Line $(CurrentLine)  BASE_PATH                  = ${BASE_PATH}";            
    Write-Host "Line $(CurrentLine)  USER                       = ${USER}";            
    Write-Host "Line $(CurrentLine)  Leaving                      echo_args";
}


#---------------------------------------------------------
# CurrentLine
#---------------------------------------------------------
function CurrentLine
{
    $MyInvocation.ScriptLineNumber
}


#---------------------------------------------------------
# Main Entry point
#---------------------------------------------------------
Write-Host "Line $(CurrentLine)  Calling                      main()"
main
Write-Host "Line $(CurrentLine)  All Done  To exit Press Continue"
Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"

